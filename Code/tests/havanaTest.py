import sys, os
import unittest
testdir = os.path.dirname(__file__)
srcdir = '../src'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from game import GameElement

class GameTestCase(unittest.TestCase):

    def setUp(self) -> None:
        sys.argv.append("src/game_elements-Havana.xml")
        sys.argv.append("red = Basic")
        sys.argv.append("green")
        self.game_element = GameElement()

    def test_filter_material_resource(self):
        # Test that it returns none
        self.assertEqual(self.game_element.material_resource("inexistant"), None)
        # Test that it returns an existant material
        self.assertEqual(self.game_element.material_resource("sandstone").name, "sandstone")


if __name__ == '__main__':
    unittest.main()
