from tkinter import Tk, ttk

from game import Game
from translation import TranslationModel, TranslationFrame, TranslationConsole, TranslationController


class Gui:
    """
    Graphical user interface of the game.
    """

    ROOT_WIDTH = 1310  # Width of the root window.
    ROOT_HEIGHT = 710  # Height of the root window.

    def __init__(self, the_game: Game):
        """
        Initialization of the graphical user interface.
        :param the_game: Game.
        """
        self.game = the_game  # Game.
        self.game.gui = self  # Graphical user interface. Remark: we have 1 GUI <---> 1 game.
        self.root = Tk()  # Root window of the GUI.
        self.root.title(self.game.game_element.game_name)
        self.root.resizable(width=False, height=False)
        self.root.minsize(Gui.ROOT_WIDTH, Gui.ROOT_HEIGHT)
        self.root.focus_set()
        self.game.init_gui()
        self.game.play()  # The GUI launches the game.
        self.root.mainloop()

    def init_frame_translate(self):
        # create the model
        model: TranslationModel = TranslationModel(self.game)
        # create view in frame
        frame_view: TranslationFrame = TranslationFrame()
        # create view in console
        console_view: TranslationConsole = TranslationConsole()
        # create the controller
        controller: TranslationController = TranslationController(model, frame_view, console_view)


