#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on March 21 14:15:16 2019. Ended without Graphical User Interface on April 5 10:09:08 2019.
Board game Havana.
@author: Olivier
"""

# TODO? abc for classes Player, AIPlayer, Resource, Building, Action.
# TODO? replace :type Class of parameter p by p: Class; see Resource.move()

# Remark: We use isinstance(resource, <type>Resource) instead of resource.type() == ResourceType.<type>
#         in order to avoid warning check when we inspect the code.
#         So we also use isinstance in some other cases.

# Design error: the stock must be rely to the game instead of the game element.
import datetime

from game import GameElement, Game
from gui import Gui
from utils import *

if __name__ == "__main__":
    printing(0, 'Start at ' + datetime.datetime.now().isoformat(sep=' ', timespec='seconds') + '.')
    game_element: GameElement = GameElement()
    game: Game = Game(game_element)
    Gui(game)
    printing(0, 'End at ' + datetime.datetime.now().isoformat(sep=' ', timespec='seconds') + '.')
