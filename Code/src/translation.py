from tkinter import LabelFrame, ttk


from game import Game
from label_frame_observable import WidgetObservable, Observer


class TranslationModel:
    LABEL_FR = {"widget_n_victory_pts_to_reach": "Total des PV",
                "frame_translate": "Langue",
                "widget_players": "Player",
                "frame_resources": "Ressources",
                "widget_n_buildings" : "Nombre de bâtiments"
                }

    LABEL_EN = {"widget_n_victory_pts_to_reach": "total  of HP",
                "frame_translate": "Language",
                "widget_players": "joueurs",
                "frame_resources": "Resources",
                "widget_n_buildings": "Number of buildings"
                }

    def __init__(self, game: Game):
        self.game = game
        self.widget_n_victory_pts_to_reach = game.widget_n_victory_pts_to_reach
        self.widget_players = game.widget_players
        self.frame_resources = game.game_element.frame_resources
        self.frame_translate: LabelFrame = ttk.LabelFrame(master=self.widget_players, text=self.LABEL_FR['frame_translate'])
        self.frame_translate.grid(row=0, column=3)
        self.widget_players_observable: WidgetObservable = WidgetObservable(self.widget_players)


class TranslationFrame:
    controller: 'TranslationController' = None

    def set_controler(self, controller: 'TranslationController'):
        self.controller = controller

    def create_frame(self):
        # Button for french translation
        ttk.Button(master=self.controller.model.frame_translate, text='Français',
                   command=lambda: self.controller.translation('FR')).pack()
        # Button for english translation
        ttk.Button(master=self.controller.model.frame_translate, text='English',
                   command=lambda: self.controller.translation('EN')).pack()


class TranslationConsole(Observer):
    controller: 'TranslationController' = None

    def set_controler(self, controller: 'TranslationController'):
        self.controller = controller

    def update(self):
        self.display()

    def display(self):
        print('Translation : ', self.controller.model.widget_players['text'])


class TranslationController:
    def __init__(self, model: TranslationModel, frame_view: TranslationFrame, console_view: TranslationConsole):
        self.model = model
        self.frame_view = frame_view
        self.frame_view.set_controler(self)
        self.frame_view.create_frame()
        self.console_view = console_view
        self.console_view.set_controler(self)
        self.console_view.display()
        # adding console_view to observer of frame_translate
        self.model.widget_players_observable.attach_observer(self.console_view)

    def translation(self, language: str):
        if language == 'EN':
            for player in self.model.game.players:
                player.widget_n_tot_victory_pts['text'] = self.model.LABEL_EN['widget_n_victory_pts_to_reach']
                player.frame_resources['text'] = self.model.LABEL_EN['frame_resources']
                player.widget_n_buildings['text'] = self.model.LABEL_EN['widget_n_buildings']
            self.model.widget_players_observable.set_text(self.model.LABEL_EN['widget_players'])
        else:
            for player in self.model.game.players:
                player.widget_n_tot_victory_pts['text'] = self.model.LABEL_FR['widget_n_victory_pts_to_reach']
                player.frame_resources['text'] = self.model.LABEL_FR['frame_resources']
                player.widget_n_buildings['text'] = self.model.LABEL_FR['widget_n_buildings']
            self.model.widget_players_observable.set_text(self.model.LABEL_FR['widget_players'])
