from tkinter import LabelFrame, ttk
from typing import List, Dict, Tuple

from building import Building, BuildingLocation
from ressources import T_Resources_int, Resource
from utils import n2str_txt_label_widget, COLOR_REMOVED, Singleton


class Table(metaclass=Singleton):
    """
    Table i.e. elements in the center of the table.
    """

    def __init__(self, buildings: List[List[Building]], resources: T_Resources_int):
        """
        Initialization of the table.
        :param buildings: Buildings revealed and placed face-up in the center of the table.
        :param resources: Number of all resources in the center of the table.
        """
        # Attributes.
        # Buildings revealed and placed face-up in the center of the table.
        self.buildings: List[List[Building]] = buildings  # Buildings revealed in the center of the table.
        self.frame_buildings_table: LabelFrame = None  # Frame for the buildings revealed in the center of the table.
        self.widget_n_buildings_in_pile: LabelFrame = None  # Widget for the number of buildings in the pile.
        self.widget_n_buildings_removed: LabelFrame = None  # Widget for the number of buildings removed.
        # Number of all resources in the center of the table.
        self.resources: T_Resources_int = resources  # Number of all resources in the center of the table.
        self.frame_resources: LabelFrame = None  # Frame for all resources in the center of the table.

    def init_frame_table(self: 'Game', the_game: 'Game') -> None:
        """
        Initialization of the frame for the table of the graphical user interface.
        """
        # Frame for the table.
        frame_table: LabelFrame = ttk.LabelFrame(master=the_game.gui.root, text='Table')  # Frame for the table.
        frame_table.grid(row=0, column=0)
        # Frame for the buildings.
        frame_buildings: LabelFrame = ttk.LabelFrame(master=frame_table, text='Bâtiments')  # Frame for the buildings.
        frame_buildings.grid(row=0, column=0)
        # Frame for the buildings in the center of the table.
        self.frame_buildings_table = \
            ttk.LabelFrame(master=frame_buildings,
                           text='Bâtiments sur la table (' + str(len(self.buildings)) + ' lignes'
                                # + ' de ' + '+'.join([str(len(row)) for row in self.buildings])
                                + ')')
        rowspan: int = the_game.game_element.n_rows_revealed_buildings \
                       * (2 + len(the_game.game_element.resources))  # Rowspan.
        self.frame_buildings_table.grid(row=0, column=0, rowspan=rowspan, columnspan=2)
        self.create_widgets_frame_buildings_table(the_game.game_element)
        # Frame for the number of buildings in the pile.
        self.widget_n_buildings_in_pile = ttk.LabelFrame(master=frame_buildings, text='Bâtiments dans la pile')
        self.widget_n_buildings_in_pile.grid(row=rowspan + 1, column=0)
        self.create_widget_n_buildings_in_pile(the_game.game_element)
        # Frame for the number of buildings removed.
        self.widget_n_buildings_removed = ttk.LabelFrame(frame_buildings, text='Bâtiments supprimés')
        self.widget_n_buildings_removed.grid(row=rowspan + 1, column=1)
        self.create_widget_n_buildings_removed(the_game.game_element)
        # Frame for the resources.
        self.frame_resources = ttk.LabelFrame(master=frame_table, text='Ressources')
        self.frame_resources.grid(row=1, column=0, rowspan=3, columnspan=len(self.resources))
        self.create_widgets_frame_resources()

    def create_widgets_frame_buildings_table(self: 'GameElement', the_game_element: 'GameElement') -> None:
        """
        [Re]Create all the widgets of the frame for the buildings in the center of the table.
        """
        # Destroy all previous widgets.
        for widget_building in self.frame_buildings_table.winfo_children():
            widget_building.destroy()
        # Create new widgets.
        row_offset: int = 2 + len(the_game_element.resources)  # Offset of the rows.
        for i_row in range(the_game_element.n_rows_revealed_buildings):
            for i_column, building in enumerate(self.buildings[i_row]):
                widget_building: LabelFrame = \
                    building.new_widget(self.frame_buildings_table)  # Widget for the building.
                widget_building.grid(row=row_offset * i_row, column=i_column)
        pass

    def create_widget_n_buildings_in_pile(self, the_game_element: 'GameElement') -> None:
        """
        Create a widget for the number of buildings in the pile.
        :param the_game_element: Game element.
        """
        ttk.Label(master=self.widget_n_buildings_in_pile,
                  text=n2str_txt_label_widget(the_game_element.n_buildings_in_location(BuildingLocation.PILE))
                  ).grid(row=0, column=0)

    def create_widget_n_buildings_removed(self, the_game_element: 'GameElement') -> None:
        """
        Create a widget for the number of buildings removed.
        :param the_game_element: Game element.
        """
        ttk.Label(master=self.widget_n_buildings_removed,
                  text=n2str_txt_label_widget(the_game_element.n_buildings_in_location(BuildingLocation.REMOVED)),
                  foreground=COLOR_REMOVED).grid(row=0, column=0)

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources, self.resources, False)

    def buildings_extremities_rows_and_coord(self) -> Dict[Building, Tuple[int, int]]:
        """
        Buildings (revealed and placed face-up) with their coordinates at the extremities (beginning and end)
        of all rows in the center of the table.
        :return Buildings (revealed and placed face-up) with their coordinates at the extremities (beginning and end)
                of all rows in the center of the table.
        """
        buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
            dict()  # Buildings with their coordinates at the extremities of all rows of the table.
        for i_row, buildings_row in enumerate(self.buildings):
            n_buildings_row: int = len(buildings_row)  # Number of buildings in the row.
            if n_buildings_row >= 1:
                buildings_extremities_rows_and_coord[buildings_row[0]] = (i_row, 0)
            if n_buildings_row >= 2:
                buildings_extremities_rows_and_coord[buildings_row[-1]] = (i_row, n_buildings_row - 1)
        return buildings_extremities_rows_and_coord

    def buildings_extremities_rows(self) -> List[Building]:
        """
        Buildings (revealed and placed face-up) at the extremities (beginning and end) of all rows
        in the center of the table.
        :return Buildings (revealed and placed face-up) at the extremities (beginning and end) of all rows
                in the center of the table.
        """
        return list(self.buildings_extremities_rows_and_coord().keys())