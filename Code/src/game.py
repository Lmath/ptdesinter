import random
import sys
from os import path
from tkinter import LabelFrame, ttk, messagebox
from typing import List, Dict, Tuple, Set, Union
from xml.etree import ElementTree as ElemTree

from actions import Action, SiestaAction, RefreshmentAction, ProtectionAction, DebrisAction, ConservationAction, \
    TaxCollectorAction, WorkerAction, ArchitectAction, PesosThiefAction, MaterialsThiefAction, BlackMarketAction, \
    PesosAction, MamaAction, ActionLocation
from building import Building, BuildingLocation

from players import PlayerColor, Player, AIPlayer, HumanPlayer, BasicAIPlayer, AdvancedAIPlayer
from ressources import T_Resources_int, Resource, WorkerResource, CoinResource, MaterialResource
from table import Table
from utils import n2str_txt_label_widget, usage, printing, first, n_random_elements_from_frequencies, \
    n_random_elements_from_sequence, LINE_SEPARATOR, messagebox_showinfo


class GameElementResource:
    """
    Numbers of some resource for the elements of the game.
    """

    def __init__(self, n_preparation_table: int, n_preparation_player: int, n_supplies: int):
        """
        Initialization of numbers of some resource for the elements of the game.
        :param n_preparation_table: Number of a resource to place in the center of the table during the preparation.
        :param n_preparation_player: Number of a resource to place in the center of the table during the preparation.
        :param n_supplies: Number of a resource to place in the center of the table during the 2nd phase of supplies.
        """
        # Number of a resource to place in the center of the table during the preparation.
        self.n_preparation_table: int = n_preparation_table
        # Number of a resource for each player during the preparation.
        self.n_preparation_player: int = n_preparation_player
        # Number of a resource to place in the center of the table during the 2nd phase of supplies.
        self.n_supplies: int = n_supplies

    def __str__(self) -> str:
        """
        Text for the numbers of some resource for the elements of the game.
        :return: Text for the numbers of some resource for the elements of the game.
        """
        return str(self.n_preparation_table) + ' ressource(s) à placer au centre de la table lors de la préparation, ' \
               + str(self.n_preparation_player) + ' ressource(s) à donner à chaque joueur lors de la préparation, ' \
               + str(self.n_supplies) \
               + ' ressource(s) à ajouter au centre de la table lors de la phase d\'approvisionnements'


class GameElement:
    """
    Elements of the game read from the XML file.
    """

    def __init__(self):
        """
        Initialization of the elements of the game read from the XML file.
        """
        # Attributes (begin).
        # Name of the game.
        self.game_name: str = None
        # Game. Remark: Like a public attribute only for friend class Game.
        self.game: Game = None
        # Minimal number of players.
        self.n_min_players: int = None
        # Maximal number of players.
        self.n_max_players: int = None
        # The number of victory points to reach for a player to win (values) according to the number of players (keys).
        self.n_victory_pts_to_reach_for_players: Dict[int, int] = None
        # Number of actions i.e. number of cards placed face-down in front of each player.
        self.n_actions: int = None
        # Number of rows of buildings revealed and placed face-up in the center of the table.
        self.n_rows_revealed_buildings: int = None
        # Number of buildings by row revealed and placed face-up in the center of the table.
        self.n_revealed_buildings_by_row: int = None
        # Number of material resources to draw from the bag to place in the center of the table during the preparation.
        self.n_draw_materials_preparation_table: int = None
        # Number of material resources to draw from the bag for each player during the preparation.
        self.n_draw_materials_preparation_player: int = None
        # Number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies.
        self.n_draw_materials_supplies: int = None
        # Number of new actions i.e. number of cards each player takes from his hand and places it face-down on top of
        # any of the action cards laid-out in front of him during the 3rd phase of new action card.
        self.n_new_actions: int = None
        # Minimal number of actions i.e. each player has no more than this number of action cards in his hand
        # at the end of the 3rd phase of new action card.
        self.n_min_actions_in_hand: int = None
        # Player colors.
        self.player_colors: Set[PlayerColor] = None
        # Resources and numbers of some resource for the elements of the game.
        self.resources: Dict[Resource, GameElementResource] = None
        self.frame_resources: LabelFrame = None  # Frame for all resources in the stock.
        # Buildings.
        self.buildings: Set[Building] = None
        # Actions.
        self.actions: Set[Action] = None
        # Resources swap: number of resources obtained for 1 resource given
        # where key is (resource given, resource obtained).
        self.resources_swap: Dict[Tuple[Resource, Resource], int] = None
        # Attributes (end).
        # Check if there is enough arguments, at least the XML file.
        n_args: int = len(sys.argv)  # Number of arguments of the command.
        if n_args < 2:
            usage('Le nombre d\'arguments ' + str(n_args)
                  + ' doit être au moins égal à 2 (au moins un fichier XML est requis).')
        # Check if the XML file exists.
        if not path.isfile(sys.argv[1]):
            usage('Le fichier XML attendu ' + sys.argv[1] + ' n\'existe pas.')
        # Prepare the tree of the XML file.
        xml_tree: ElemTree.ElementTree = ElemTree.parse(sys.argv[1])  # The XML tree.
        xml_tree_root: ElemTree.Element = xml_tree.getroot()  # Root of the XML tree.
        # Read the name of the game.
        self.game_name = xml_tree_root.find('game_name').text
        # Initialization of the minimum and maximum number of players of the game read from the XML file.
        self.init_min_max_players(xml_tree_root, n_args)
        # Initialization of the number of victory points a player have to reach to win the game read from the XML file.
        self.init_n_victory_pts_to_reach_for_players(xml_tree_root)
        # Initialization of the players colors of the game read from the XML file.
        self.init_player_colors(xml_tree_root)
        # Initialization of the resources (and related attributes) read from the XML file.
        self.init_resources(xml_tree_root)
        # Initialization of the buildings (and related attributes) read from the XML file.
        self.init_buildings(xml_tree_root)
        # Initialization of the actions (and related attributes) read from the XML file.
        self.init_actions(xml_tree_root)
        # End of the initialization of the elements of the game.
        self.display()
        pass

    def init_min_max_players(self, xml_tree_root: ElemTree.Element, n_args: int) -> None:
        """
        Initialization of the minimum and maximum number of players
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        :param n_args: Number of arguments of the command.
        """
        # Read the minimum number of players.
        self.n_min_players = int(xml_tree_root.find('n_min_players').text)
        # Check if the minimum number of players is positive and is not zero.
        if self.n_min_players < 1:
            usage('Le nombre minimum de joueurs ' + str(self.n_min_players) + ' doit être au moins égal à 1.')
        # Read the maximum number of players.
        self.n_max_players = int(xml_tree_root.find('n_max_players').text)
        # Check if the minimum number of players is smaller than or equals to the maximum number of players.
        if self.n_min_players > self.n_max_players:
            usage('Le nombre minimum de joueurs '
                  + str(self.n_min_players) + ' doit être au plus égal au nombre maximum de joueurs '
                  + str(self.n_max_players) + '.')
        # Check if the number of arguments according to the number of players.
        if not (self.n_min_players <= n_args - 2 <= self.n_max_players):
            usage('Le nombre d\'arguments ' + str(n_args) + ' doit être compris entre '
                  + str(2 + self.n_min_players) + ' et ' + str(2 + self.n_max_players) + '.')
        pass

    def init_n_victory_pts_to_reach_for_players(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the number of victory points a player have to reach to win
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the number of victory points a player have to reach to win.
        # Check if numbers of victory points a player have to reach to win are positive and nonzero.
        self.n_victory_pts_to_reach_for_players = dict()
        for n_vp_to_reach_for_players_tag in xml_tree_root.findall(
                'n_victory_pts_to_reach_for_players/n_victory_pts_to_reach_for_player'):
            n_vp_to_reach_for_players: int = int(n_vp_to_reach_for_players_tag.find(
                'n_victory_pts_to_reach').text)  # Number of VPs to reach for players.
            if n_vp_to_reach_for_players < 1:
                usage('Le nombre de points de victoire à atteindre pour gagner la partie '
                      + '(récupéré à partir du fichier XML) doit être au moins égal à 1.')
            self.n_victory_pts_to_reach_for_players[int(n_vp_to_reach_for_players_tag.find('n_players').text)] = \
                n_vp_to_reach_for_players
        # Check if all numbers of players are set for the number of victory points a player have to reach to win.
        if not (set(range(self.n_min_players, self.n_max_players + 1)) <=
                self.n_victory_pts_to_reach_for_players.keys()):
            usage('Il faut préciser le nombre de points de victoire à atteindre pour gagner la partie '
                  + '(récupéré à partir du fichier XML) pour tous les nombres de joueurs possibles (entre '
                  + str(self.n_min_players) + ' et ' + str(self.n_max_players) + ').')
        pass

    def init_player_colors(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the players colors
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the player colors. Check their names are unique.
        self.player_colors = set()
        player_color_names_xml_file: Set[str] = set()  # Names of the player colors.
        for player_color_name_tag in xml_tree_root.findall('player_colors/player_color'):
            player_color_name_xml_file: str = player_color_name_tag.text  # Name of the player color.
            if player_color_name_xml_file in player_color_names_xml_file:
                usage('Il y a des doublons, notamment ' + player_color_name_xml_file
                      + ', parmi les couleurs des joueurs (récupérées à partir du fichier XML).')
            else:
                player_color_names_xml_file.add(player_color_name_xml_file)
                self.player_colors.add(PlayerColor(player_color_name_xml_file))
        # Check if the number of player colors is equals to or greater than the maximal number of players.
        if len(self.player_colors) < self.n_max_players:
            usage('Le nombre de couleurs des joueurs ' + str(len(self.player_colors))
                  + ' doit être au moins égal au nombre maximum de joueurs ' + str(self.n_max_players) + '.')
        pass

    def init_resources(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the resources
        and “5 to 1“-swap resources during building purchase
        and the number of resources
            placed in the center of the table during the preparation,
            received by each player during the preparation,
            added in the center of the table during the 2nd phase of supplies
        and the number of material resources to draw from the bag
            to place in the center of the table during the preparation,
            for each player during the preparation,
            to place in the center of the table during the 2nd phase of supplies
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Prepare the resources.
        self.resources = dict()
        # Read the worker resource.
        self.resources[WorkerResource(
            xml_tree_root.find('resources/worker/name').text,
            int(xml_tree_root.find('resources/worker/number').text),
            xml_tree_root.find(
                'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_worker_removed').text ==
            'True')] = GameElementResource(0, 0, 0)
        # Read the coin resource.
        self.resources[CoinResource(
            xml_tree_root.find('resources/coin/name').text,
            int(xml_tree_root.find('resources/coin/number').text),
            xml_tree_root.find(
                'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_coin_removed').text ==
            'True')] = GameElementResource(0, 0, 0)
        # Read the material resources.
        # Get once a player has purchased a building, is the material removed from the game?
        # (otherwise is returned to the box)
        is_material_removed: bool = xml_tree_root.find(
            'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_material_removed').text == 'True'
        for material_tag in xml_tree_root.findall('resources/materials/material'):
            self.resources[MaterialResource(material_tag.find('name').text, int(material_tag.find('number').text),
                                            is_material_removed, material_tag.find('is_colored').text == 'True',
                                            material_tag.find('color').text)] = GameElementResource(0, 0, 0)
        # Check if the names of the resources read are unique.
        if len(set([resource.name for resource in self.resources])) < len(self.resources):
            usage('Il y a au moins un doublon parmi les noms des ressources (récupérées à partir du fichier XML).')
        # Check if the numbers of the resources read are all non negative.
        if [resource for resource in self.resources if resource.number < 0]:
            usage('Il y a au moins un nombre de ressources '
                  + '(récupérées à partir du fichier XML) qui est strictement négatif.')
        # Check if the material resource colors read are unique.
        if len(set([material.color for material in self.material_resources()])) < len(self.material_resources()):
            usage('Il y a au moins un doublon parmi les couleurs des matériaux (récupérées à partir du fichier XML).')
        # ~~~~~
        # Prepare the “5 to 1“-swap resources during building purchase.
        self.resources_swap = dict()
        # Read the number of coins instead of a worker when a player purchases a building.
        n_coins_for_1_worker: int = int(
            xml_tree_root.find('course/phase_actions_and_purchasing/swap_during_purchasing/n_coins_for_1_worker').text)
        # Check if the number of coins instead of a worker when a player purchases a building is positive and nonzero.
        if n_coins_for_1_worker < 1:
            usage('Le nombre de pièces de monnaie pour remplacer un ouvrier pendant l\'achat de bâtiments '
                  + str(n_coins_for_1_worker) + ' doit être au moins égal à 1.')
        # Set the number of coins instead of a worker when a player purchases a building.
        self.resources_swap[(self.coin_resource(), self.worker_resource())] = n_coins_for_1_worker
        # Read the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building.
        n_material_debris_for_1_colored_material: int = int(
            xml_tree_root.find('course/phase_actions_and_purchasing/swap_during_purchasing/n_coins_for_1_worker').text)
        # Check if the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building is positive and nonzero.
        if n_material_debris_for_1_colored_material < 1:
            usage('Le nombre de débris pour remplacer un autre matériau pendant l\'achat de bâtiments '
                  + str(n_material_debris_for_1_colored_material) + ' doit être au moins égal à 1.')
        # Set the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building.
        colored_materials: Set[MaterialResource] = set([material for material in self.material_resources()
                                                        if material.is_colored])  # Material resources colored.
        for non_colored_material in set(
                [material for material in self.material_resources() if not material.is_colored]):
            for colored_material in colored_materials:
                self.resources_swap[(non_colored_material, colored_material)] = n_material_debris_for_1_colored_material
        # ~~~~~
        # Read the number of coins placed in the center of the table during the preparation.
        n_coins_preparation_table: int = int(xml_tree_root.find('preparation/center_table/n_coins').text)
        # Check if the number of coins placed in the center of the table during the preparation is nonnegative.
        if n_coins_preparation_table < 0:
            usage('Le nombre de pièces de monnaie à placer au centre de la table lors de la préparation '
                  + str(n_coins_preparation_table) + ' doit être positif ou nul.')
        # Set the number of coins placed in the center of the table during the preparation.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_preparation_table = n_coins_preparation_table
        self.resources[self.coin_resource()] = game_element_resource
        # Read the number of workers received by each player during the preparation.
        n_workers_preparation_player: int = int(xml_tree_root.find('preparation/player/n_workers').text)
        # Check if the number of workers received by each player during the preparation is nonnegative.
        if n_workers_preparation_player < 0:
            usage('Le nombre d\'ouvriers à donner à chaque joueur lors de la préparation '
                  + str(n_workers_preparation_player) + ' doit être positif ou nul.')
        # Set the number of workers received by each player during the preparation.
        game_element_resource: GameElementResource = self.resources[self.worker_resource()]
        game_element_resource.n_preparation_player = n_workers_preparation_player
        self.resources[self.worker_resource()] = game_element_resource
        # Read the number of coins received by each player during the preparation.
        n_coins_preparation_player: int = int(xml_tree_root.find('preparation/player/n_coins').text)
        # Check if the number of coins received by each player during the preparation is nonnegative.
        if n_coins_preparation_player < 0:
            usage('Le nombre de pièces de monnaie à donner à chaque joueur lors de la préparation '
                  + str(n_coins_preparation_player) + ' doit être positif ou nul.')
        # Set the number of coins received by each player during the preparation.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_preparation_player = n_coins_preparation_player
        self.resources[self.coin_resource()] = game_element_resource
        # Read the number of all materials received by each player during the preparation.
        for material_resource in self.material_resources():
            material_resource_name: str = material_resource.name  # Name of the material.
            # Read the number of materials received by each player during the preparation.
            n_materials_preparation_player: int = \
                int(xml_tree_root.find('preparation/player/n_material_' + material_resource_name).text)
            # Check if the number of materials received by each player during the preparation is nonnegative.
            if n_materials_preparation_player < 0:
                usage('Le nombre de matériaux ' + material_resource_name
                      + ' à donner à chaque joueur lors de la préparation '
                      + str(n_materials_preparation_player) + ' doit être positif ou nul.')
            # Set the number of materials received by each player during the preparation.
            game_element_resource: GameElementResource = self.resources[material_resource]
            game_element_resource.n_preparation_player = n_materials_preparation_player
            self.resources[material_resource] = game_element_resource
        # Read the number of coins added in the center of the table during the 2nd phase of supplies.
        n_coins_supplies: int = int(xml_tree_root.find('course/phase_supplies/n_coins').text)
        # Check if the number of coins added in the center of the table during the 2nd phase of supplies is nonnegative.
        if n_coins_supplies < 0:
            usage('Le nombre de pièces de monnaie à ajouter au centre de la table '
                  + 'lors de la phase d\'approvisionnements ' + str(n_coins_supplies) + ' doit être positif ou nul.')
        # Set the number of coins added in the center of the table during the 2nd phase of supplies.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_supplies = n_coins_supplies
        self.resources[self.coin_resource()] = game_element_resource
        # ~~~~~
        # Read the number of material resources to draw from the bag to place in the center of the table
        # during the preparation.
        self.n_draw_materials_preparation_table = \
            int(xml_tree_root.find('preparation/center_table/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag to place in the center of the table
        # during the preparation is nonnegative.
        if self.n_draw_materials_preparation_table < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à placer au centre de la table '
                  + 'lors de la préparation ' + str(self.n_draw_materials_preparation_table)
                  + ' doit être positif ou nul.')
        # Read the number of material resources to draw from the bag for each player during the preparation.
        self.n_draw_materials_preparation_player = int(xml_tree_root.find('preparation/player/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag for each player
        # during the preparation is nonnegative.
        if self.n_draw_materials_preparation_player < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à donner à chaque joueur lors de la préparation'
                  + str(self.n_draw_materials_preparation_player) + ' doit être positif ou nul.')
        # Read the number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies.
        self.n_draw_materials_supplies = int(xml_tree_root.find('course/phase_supplies/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies is nonnegative.
        if self.n_draw_materials_supplies < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à ajouter au centre de la table '
                  + 'lors de la phase d\'approvisionnements ' + str(self.n_draw_materials_supplies)
                  + ' doit être positif ou nul.')
        # ~~~~~
        # Check if the number of each material resource is enough to place in the table and
        # to give to the maximum number of players during the preparation.
        for resource, game_element_resource in sorted(self.resources.items()):
            if game_element_resource.n_preparation_table \
                    + (game_element_resource.n_preparation_player * self.n_max_players) > resource.number:
                usage('Le nombre de matériaux ' + resource.name
                      + ' doit être suffisant pour être placés au centre de la table '
                      + 'et donnés au nombre maximum de joueurs lors de la préparation.')
        # ~~~~~
        # Check if the total number of material resources is enough to place in the table and
        # to give to the maximum number of players during the preparation.
        if sum([game_element_resource.n_preparation_table
                for resource, game_element_resource in self.resources.items()
                if isinstance(resource, MaterialResource)]) \
                + self.n_draw_materials_preparation_table \
                + (sum([game_element_resource.n_preparation_player
                        for resource, game_element_resource in self.resources.items()
                        if isinstance(resource, MaterialResource)])
                   + self.n_draw_materials_preparation_player) * self.n_max_players \
                > sum([material.number for material in self.material_resources()]):
            usage('Le nombre total de matériaux doit être suffisant pour être placés au centre de la table et '
                  + 'donnés au nombre maximum de joueurs lors de la préparation.')
        pass

    def init_buildings(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the buildings
        and number of rows of buildings and number of buildings by row revealed face-up in the center of the table
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Prepare the buildings.
        self.buildings = set()
        exists_building_with_1or2or3_vp: bool = False  # Exists a building with n_victory_pts <= 3?
        # Read the buildings.
        # Check if their cost numbers are nonpositive.
        # Check if their numbers of victory points are positive and nonzero.
        for building_tag in xml_tree_root.findall('buildings/building'):
            building_costs: T_Resources_int = dict()  # Cost for each resource of the building.
            building_costs[self.worker_resource()] = int(building_tag.find('cost/n_workers').text)
            building_costs[self.coin_resource()] = int(building_tag.find('cost/n_coins').text)
            for material_resource in self.material_resources():
                material_resource_name: str = material_resource.name  # Name of the material.
                building_costs[material_resource] = \
                    int(building_tag.find('cost/n_material_' + material_resource_name).text)
            for n_building_cost in building_costs.values():
                if n_building_cost > 0:
                    usage('Le coût d\'une resource d\'un bâtiment (récupéré à partir du fichier XML) '
                          + 'doit être négatif ou nul.')
            n_victory_pts: int = \
                int(building_tag.find('n_victory_pts').text)  # Number of victory points of the building.
            if n_victory_pts < 1:
                usage('Le nombre de points de victoire d\'un bâtiment (récupéré à partir du fichier XML) '
                      + 'doit être au moins égal à 1.')
            if n_victory_pts <= 3:
                exists_building_with_1or2or3_vp = True
            self.buildings.add(
                Building(building_tag.find('is_architect_required').text == 'True', building_costs, n_victory_pts))
        # Check if exists a building.
        if not len(self.buildings):  # Check if self.buildings is equal to {} (and also is equal to None).
            usage('Aucun bâtiment n\'a été récupéré à partir du fichier XML.')
        # Check if exists a building with a number of victory points less than or equals to 3.
        if not exists_building_with_1or2or3_vp:
            usage('Aucun des bâtiments (récupérés à partir du fichier XML) n\'a au plus 3 points de victoire.')
        # Read the number of rows of buildings revealed face-up in the center of the table.
        self.n_rows_revealed_buildings = int(
            xml_tree_root.find('preparation/center_table/revealed_buildings/n_rows_revealed_buildings').text)
        # Check if the number of rows of buildings revealed face-up in the center of the table is positive and nonzero.
        if self.n_rows_revealed_buildings < 1:
            usage('Le nombre de lignes de bâtiments à acheter ' + str(self.n_rows_revealed_buildings)
                  + ' doit être au moins égal à 1.')
        # Read the number of buildings by row revealed face-up in the center of the table.
        self.n_revealed_buildings_by_row = int(
            xml_tree_root.find('preparation/center_table/revealed_buildings/n_revealed_buildings_by_row').text)
        # Check if the number of buildings by row revealed face-up in the center of the table
        # is greater than or equals to 2.
        if self.n_revealed_buildings_by_row < 2:
            usage('Le nombre de bâtiments par ligne à acheter ' + str(self.n_revealed_buildings_by_row)
                  + ' doit être au moins égal à 2 (car une ligne a deux extrémités).')
        # Check if there are enough buildings to reveal face-up in the center of the table.
        if self.n_rows_revealed_buildings * self.n_revealed_buildings_by_row > len(self.buildings):
            usage('Il n\'y a pas assez de bâtiments ' + str(len(self.buildings))
                  + ' qui peuvent être présentés pour être achetés ('
                  + str(self.n_rows_revealed_buildings) + ' lignes de '
                  + str(self.n_revealed_buildings_by_row) + ').')
        # Check if there are sufficient total number of victory points in all buildings to lead a player to win.
        if sum(building.n_victory_pts for building in self.buildings) <= \
                max([(self.n_victory_pts_to_reach_for_players[n_player] - 1) * n_player
                     for n_player in range(self.n_min_players, self.n_max_players + 1)]):
            usage('Il peut ne pas y avoir assez de points de victoire des bâtiments '
                  + 'pour permettre à un joueur de gagner.')
        pass

    def init_actions(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the actions
        and the number of actions, the number of new actions, the minimal number of actions
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the actions. Check if their names are unique. Check if their values are between 0 and 9.
        self.actions = set()
        action_names_xml_file: Set[str] = set()  # Names of the actions read from the XML file.
        for action_tag in xml_tree_root.findall('actions/action'):
            action_name_xml_file: str = action_tag.find('name').text  # Name of the action read from the XML file.
            if action_name_xml_file in action_names_xml_file:
                usage('Il y a des doublons, notamment ' + action_name_xml_file
                      + ', parmi les actions (récupérées à partir du fichier XML).')
            else:
                action_names_xml_file.add(action_name_xml_file)
                action_value_xml_file: int = \
                    int(action_tag.find('value').text)  # Value of the action read from the XML file.
                if not (0 <= action_value_xml_file <= 9):
                    usage('La valeur d\'une action ' + str(action_value_xml_file)
                          + ' (récupérées à partir du fichier XML) doit être compris entre 0 et 9.')
                else:
                    action_names_xml_file.add(action_name_xml_file)
                    action_text_xml_file: str = \
                        action_tag.find('text').text  # Text of the action read from the XML file.
                    if not action_text_xml_file:
                        action_text_xml_file = ''
                    action_can_be_skipped_xml_file: bool = action_tag.find(
                        'can_be_skipped').text == 'True'  # Is the action read from the XML file can be skipped?
                    if action_name_xml_file == 'Siesta':
                        self.actions.add(
                            SiestaAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Refreshment':
                        self.actions.add(
                            RefreshmentAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                              action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Protection':
                        self.actions.add(
                            ProtectionAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                             action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Debris':
                        self.actions.add(
                            DebrisAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Conservation':
                        self.actions.add(
                            ConservationAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                               action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Tax Collector':
                        self.actions.add(
                            TaxCollectorAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                               action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Worker':
                        self.actions.add(
                            WorkerAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Architect':
                        self.actions.add(
                            ArchitectAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                            action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Pesos Thief':
                        self.actions.add(
                            PesosThiefAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                             action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Materials Thief':
                        self.actions.add(
                            MaterialsThiefAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                 action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Black Market':
                        self.actions.add(
                            BlackMarketAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                              action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Pesos':
                        self.actions.add(PesosAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                     action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Mama':
                        self.actions.add(MamaAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                    action_can_be_skipped_xml_file))
                    else:
                        usage('Le nom de l\'action ' + action_name_xml_file
                              + ' (récupéré à partir du fichier XML) est inconnu.')
        # Check if exists an action.
        if not len(self.actions):  # Check if self.actions is equal to {} (and also is equal to None).
            usage('Aucune action n\'a été récupérée à partir du fichier XML.')
        # Read the number of actions.
        self.n_actions = int(xml_tree_root.find('preparation/n_actions').text)
        # Check if the number of actions is between 1 and the total number of possible actions.
        if not (1 <= self.n_actions <= len(self.actions)):
            usage('Le nombre d\'action(s) (par manche) ' + str(self.n_actions)
                  + ' doit être compris entre 1 et le nombre total d\'actions possibles ' + str(len(self.actions))
                  + '.')
        # Read the number of new actions.
        self.n_new_actions = int(xml_tree_root.find('course/phase_new_action_card/n_new_actions').text)
        # Check if the number of new actions is between 1 and the number of actions.
        if not (1 <= self.n_new_actions <= self.n_actions):
            usage('Le nombre de nouvelle(s) action(s) (à chaque manche) ' + str(self.n_new_actions)
                  + ' doit être compris entre 1 et le nombre d\'action(s) (par manche) ' + str(self.n_actions)
                  + '.')
        # Read the minimal number of actions.
        self.n_min_actions_in_hand = int(xml_tree_root.find('course/phase_new_action_card/n_min_actions_in_hand').text)
        # Check if the minimal number of actions is between 1 and the total number of possible actions.
        if not (self.n_new_actions <= self.n_min_actions_in_hand <= len(self.actions)):
            usage('Le nombre d\'action(s) minimum (en main) ' + str(self.n_min_actions_in_hand)
                  + ' doit être compris entre le nombre de nouvelle(s) action(s) (à chaque manche) '
                  + str(self.n_new_actions) + ' et le nombre total d\'actions possibles ' + str(len(self.actions))
                  + '.')
        pass

    def init_frame_stock(self) -> None:
        """
        Initialization of the frame for the stock of the graphical user interface.
        """
        # Frame for the stock.
        frame_stock: LabelFrame = ttk.LabelFrame(master=self.game.gui.root, text='Réserve')  # Frame for the stock.
        frame_stock.grid(row=1, column=0)
        # Frame for the resources in the stock.
        self.frame_resources = ttk.LabelFrame(master=frame_stock, text='Ressources')
        self.frame_resources.grid(row=0, column=0, rowspan=4, columnspan=len(self.resources))
        self.create_widgets_frame_resources()

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources,
                                                {resource: resource.number for resource in self.resources}, True)

    def display(self) -> None:
        """
        Display all the elements of the game read from the XML file.
        """
        printing(0, self.game_name
                 + ', ' + 'de ' + str(self.n_min_players) + ' à ' + str(self.n_max_players) + ' joueurs'
                 + ', ' + str(len(self.player_colors)) + ' couleurs de joueurs'
                 + ', ' + str(len(self.resources)) + ' ressources'
                 + ', ' + str(len(self.buildings)) + ' bâtiments'
                 + ', ' + str(len(self.actions)) + ' actions'
                 + '.')
        printing(1, 'Couleurs de joueurs :')
        for player_color in sorted(self.player_colors):
            printing(2, str(player_color))
        printing(1, 'Ressources :')
        for resource, game_element_resource in sorted(self.resources.items()):
            printing(2, resource.name)
            printing(3, str(resource))
            printing(3, str(game_element_resource))
        printing(1, 'Nombre de matériaux à piocher dans le sac : '
                 + str(self.n_draw_materials_preparation_table)
                 + ' à placer au centre de la table lors de la préparation, '
                 + str(self.n_draw_materials_preparation_player)
                 + ' à donner à chaque joueur lors de la préparation, '
                 + str(self.n_draw_materials_supplies)
                 + ' à ajouter au centre de la table lors de la phase d\'approvisionnements')
        printing(1, 'Échanges entre ressources :')
        for (resource_given, resource_obtained), number_for_1 in sorted(self.resources_swap.items()):
            printing(2, str(number_for_1) + ' ' + resource_given.name + ' for 1 ' + resource_obtained.name)
        printing(1, 'Actions :')
        for action in sorted(self.actions):
            printing(2, str(action))
        printing(1, str(self.n_actions) + ' action(s) (par manche), '
                 + str(self.n_new_actions) + ' nouvelle(s) action(s) (à chaque manche), '
                 + str(self.n_min_actions_in_hand) + ' action(s) minimum (en main)')
        printing(1, 'Bâtiments :')
        for building in sorted(self.buildings):
            printing(2, str(building))
        printing(1, 'Les bâtiments à acheter forment '
                 + str(self.n_rows_revealed_buildings) + ' lignes de ' + str(self.n_revealed_buildings_by_row)
                 + ' au centre de la table')
        printing(1, 'Points de victoire à atteindre pour gagner la partie selon le nombre de joueurs :')
        for n_players, n_victory_pts_to_reach in sorted(self.n_victory_pts_to_reach_for_players.items()):
            printing(2, str(n_victory_pts_to_reach) + ' points de victoire à ' + str(n_players) + ' joueurs')

    def worker_resource(self) -> WorkerResource:
        """
        Worker resource.
        :return: Worker resource.
        """
        workers_resources: List[WorkerResource] = [resource for resource in self.resources
                                                   if isinstance(resource, WorkerResource)]  # Workers as resources.
        return first(workers_resources) if workers_resources else None  # Remark: We consider exists 1! worker resource.

    def coin_resource(self) -> CoinResource:
        """
        Coin resource.
        :return: Coin resource.
        """
        coins_resources: List[CoinResource] = [resource for resource in self.resources
                                               if isinstance(resource, CoinResource)]  # Coins as resources.
        return first(coins_resources) if coins_resources else None  # Remark: We consider exists 1! coin resource.

    def material_resources(self) -> Set[MaterialResource]:
        """
        Material resources.
        :return: Material resources.
        """
        return set([resource for resource in self.resources.keys() if isinstance(resource, MaterialResource)])

    def material_resource(self, material_name: str) -> MaterialResource:
        """
        Material resource for which its name is equals to the name given in parameter.
        :param material_name: Name of the material resource.
        :return: Material resource for which its name is equals to the name given in parameter.
        """
        # Get materials such that their name is equals to the name given in parameter.
        return next(filter(lambda material: material.name == material_name, self.material_resources()),None)

    def resource(self, resource_name: str) -> Resource:
        """
        Resource for which its name is equals to the name given in parameter.
        :param resource_name: Name of the resource.
        :return: Resource for which its name is equals to the name given in parameter.
        """
        # Get resources such that their name is equals to the name given in parameter.
        resources_correct_name: List[Resource] = [resource for resource in self.resources
                                                  if resource.name == resource_name]
        return first(resources_correct_name) if resources_correct_name else None

    def draw_materials(self, n_draw_materials: int) -> List[MaterialResource]:
        """
        Draw material resources ("without looking, draw material resources (cubes) from the bag").
        :param n_draw_materials: Number (maximum) of materials to draw (from the bag).
        :return: Material resources.
        """
        return n_random_elements_from_frequencies(n_draw_materials, {material_resource: material_resource.number
                                                                     for material_resource in self.material_resources()
                                                                     if material_resource.number > 0})

    def draw_buildings(self, n_draw_buildings: int, only_in_pile: bool) -> List[Building]:
        """
        Draw buildings. For example,
        "Reveal 12 building and place them face-up in two rows of 6 cards each in the center of the table.".
        :param n_draw_buildings: Number (maximum) of buildings to draw (from the bag).
        :param only_in_pile: Should we consider buildings only in the pile
                             (e.g. of building as a face-down draw pile at the table edge) that is
                             neither purchased by any player nor in the center of the table?
                             (otherwise, consider all buildings)
        :return: Buildings.
        """
        return n_random_elements_from_sequence(n_draw_buildings,
                                               self.buildings_in_pile() if only_in_pile else
                                               [building for building in self.buildings])

    def buildings_in_pile(self) -> List[Building]:
        """
        Buildings in the pile (e.g. of building as a face-down draw pile at the table edge) that is
        neither removed nor purchased by any player nor in the center of the table.
        :return: Buildings in the pile (e.g. of building as a face-down draw pile at the table edge).
        """
        return [building for building in self.buildings
                if not building.is_removed
                and building not in [building for player in self.game.players for building in player.buildings]
                and building not in [building for building_row in self.game.table.buildings
                                     for building in building_row]]

    def n_buildings_in_location(self, building_location: BuildingLocation) -> int:
        """
        Number of buildings in some location.
        :param building_location
        :return: Number of buildings in some location.
        """
        if building_location == BuildingLocation.TABLE:
            return len([None for building_row in self.game.table.buildings for _ in building_row])
        elif building_location == BuildingLocation.PURCHASED:
            return len([None for player in self.game.players for _ in player.buildings])
        elif building_location == BuildingLocation.PILE:
            return len(self.buildings_in_pile())
        elif building_location == BuildingLocation.REMOVED:
            return len(list(filter(lambda building: building.is_removed, self.buildings)))
        else:
            raise ValueError('Building location ' + str(building_location) + ' is unknown.')


class Game:
    """
    Game.
    """

    def __init__(self, the_game_element: GameElement):
        """
        Initialization (preparation) of the game.
        Remark: We rename "game_element" by "the_game_element" in order to avoid (weak) warning
                "shadowing names defined in outer scopes" during the code inspection.
        ~~~~~
        - Each player receives an identical set of 13 action cards and takes them in his hand.
        - Place the Pesos, workers, and the bag (containing all 80 building material cubes) as a stock
          at the table edge.
          Shuffle the 36 buildings.
          Reveal 12 building and place them face-up in two rows of 6 cards each in the center of the table.
          Note: there should be at least one building with a value of 1, 2, or 3 on either end of at least
                one of the rows.
          Place the remaining building as a face-down draw pile at the table edge.
        - Each player draws 1 building material cube from the bag and places it in front of himself.
          Additionally, each player receives 1 Peso, which he also places in front of himself.
        - Without looking, draw 3 cubes from the bag and place them below the 2 rows of face-up building cards
          in the center of the table.
          Also place 4 Pesos in the center of the table.
        - Each player chooses any 2 of his 13 action cards and places them face-down for the time being
          in front of himself.
          Once all players have done that, the 2 actions cards are revealed and arranged with the lower numbered card
          on the left.
        :param the_game_element: Elements of the game read from the XML file.
        """
        # Attributes (begin).
        # Graphical user interface. Remark: Like a public attribute only for friend class Gui.
        from gui import Gui
        self.gui: Gui = None
        # Elements of the game read from the XML file.
        self.game_element: GameElement = the_game_element
        # Players.
        self.players: Set[Player] = None
        # Table.
        self.table: Table = None
        # Attributes used when the game is played.
        # Turn.
        self.turn: int = None  # Turn.
        self.widget_turn: LabelFrame = None  # Widget for the turn.
        # Is the action Conservation already played during the turn?
        # Remark: Like a public attribute only for friend class ConservationAction.
        self.is_conservation_played: bool = None
        # Is the game over?
        # Remark: Like a public attribute only for friend class ConservationAction.
        self.is_game_over: bool = None
        # Set the game for the game element: we have 1 game_element <---> 1 game.
        self.game_element.game = self
        # Attributes (end).
        # Initialization (preparation) of the table.
        self.init_table()
        # Initialization (preparation) of the (ordered) players.
        self.init_players()
        for player in sorted(self.players):
            player.choose_actions_preparation(self.game_element.n_actions)
        # End of the initialization of the game.
        # self.display()
        pass

    def init_table(self) -> None:
        """
        Initialization (preparation) of the table.
        """
        # Prepare the buildings.
        # Check if exists at least one building with number of victory points <= 3 on
        # either end of at least one of the rows.
        exists_building_3_or_less_vp: bool = False  # Exists building with VPs <= 3 at the end of some row?
        buildings: List[List[Building]] = None  # Initial buildings of the table.
        while not exists_building_3_or_less_vp:
            buildings = []
            # Draw all buildings.
            n_draw_buildings: int = self.game_element.n_rows_revealed_buildings * \
                                    self.game_element.n_revealed_buildings_by_row  # Number of buildings to draw.
            draw_buildings = self.game_element.draw_buildings(n_draw_buildings=n_draw_buildings, only_in_pile=False)
            # Split all buildings (in several rows of same length).
            for _ in range(self.game_element.n_rows_revealed_buildings):
                row_buildings: List[Building] = \
                    draw_buildings[:self.game_element.n_revealed_buildings_by_row]  # One row of buildings.
                if row_buildings[0].n_victory_pts <= 3 or row_buildings[-1].n_victory_pts <= 3:
                    exists_building_3_or_less_vp = True
                buildings.append(row_buildings)
                draw_buildings = draw_buildings[self.game_element.n_revealed_buildings_by_row:]
            if draw_buildings:
                raise ValueError('The draw buildings ' + str(draw_buildings)
                                 + ' during the initialization of the table is not [].')
        # Prepare the resources.
        resources: T_Resources_int = \
            {resource: 0 for resource in self.game_element.resources.keys()}  # Initial resources of the table.
        # Initialize the table.
        self.table = Table(buildings, resources)
        # Update resources from stock to the table during preparation.
        for resource, game_element_resource in self.game_element.resources.items():
            resource.move(n_resources_to_move=game_element_resource.n_preparation_table,
                          from_stock=True, to_table=self.table)
        # Draw material resources from stock to the table during preparation.
        draw_materials: List[MaterialResource] = \
            self.game_element.draw_materials(self.game_element.n_draw_materials_preparation_table)  # Draw materials.
        for material in draw_materials:
            material.move(n_resources_to_move=1, from_stock=True, to_table=self.table)
        pass

    def init_players(self) -> None:
        """
        Initialization (preparation) of the players.
        """
        # Prepare the players.
        self.players = set()
        # Prepare the elements the player has initially.
        resources: T_Resources_int = \
            {resource: 0 for resource in self.game_element.resources.keys()}  # Initial resources the player has.
        buildings: Set[Building] = set()  # Initial buildings the player has purchased.
        actions: Dict[Action, ActionLocation] = \
            {action: ActionLocation.HAND
             for action in self.game_element.actions}  # Initial deck of actions the player has.
        # Check the players: colors are unique, ai names exist, 1! human.
        player_color_names: Dict[str, PlayerColor] = {player_color.name: player_color for player_color in
                                                      self.game_element.player_colors}  # Player colors by their names.
        n_human_players: int = 0  # Number of human players.
        for arg in sys.argv[2:]:
            # Remark: The argument of the command is either <color> for a human or <color=ai_name> for an AI.
            arg_split: List[str] = arg.split('=')  # Argument is split.
            arg_split_len: int = len(arg_split)  # Length of the argument once split.
            arg_player_color_name: str = arg_split[0]  # Player color name obtained from the argument.
            arg_upper_ai_name: str = (arg_split[1].upper() if arg_split_len == 2 else
                                      None)  # Uppercase AI name obtained from the argument.
            # Check the argument of the command.
            if not (1 <= arg_split_len <= 2):
                usage('L\'argument ' + arg + ' de la commande ne respecte pas le format.')
            if arg_player_color_name not in player_color_names.keys():
                usage('La couleur de joueur ' + arg_player_color_name + ' de l\'argument ' + arg
                      + ' de la commande n\'existe pas ou a déjà été attribuée à un autre joueur.')
            if arg_split_len == 2 and arg_upper_ai_name not in AIPlayer.upper_ai_names():
                usage('Le nom ' + arg_upper_ai_name + ' pour une intelligence artificielle de l\'argument ' + arg
                      + ' de la commande n\'existe pas.')
            # Initialization of a player.
            player: Player = None  # Player.
            if arg_split_len == 1:
                n_human_players += 1
                player = HumanPlayer(player_color_names[arg_player_color_name],
                                     resources.copy(), buildings.copy(), actions.copy())
            elif arg_upper_ai_name == BasicAIPlayer.ai_name.upper():
                player = BasicAIPlayer(player_color_names[arg_player_color_name],
                                       resources.copy(), buildings.copy(), actions.copy())
            elif arg_upper_ai_name == AdvancedAIPlayer.ai_name.upper():
                player = AdvancedAIPlayer(player_color_names[arg_player_color_name],
                                          resources.copy(), buildings.copy(), actions.copy())
            else:
                usage('Le nom ' + arg_upper_ai_name + ' pour une intelligence artificielle de l\'argument ' + arg
                      + ' de la commande est inconnu.')
            # Add the player.
            self.players.add(player)
            # Update resources from stock to the player during preparation.
            for resource, game_element_resource in self.game_element.resources.items():
                resource.move(n_resources_to_move=game_element_resource.n_preparation_player,
                              from_stock=True, to_player=player)
            # Draw material resources from stock to the player during preparation.
            draw_materials: List[MaterialResource] = self.game_element.draw_materials(
                self.game_element.n_draw_materials_preparation_player)  # Draw materials.
            for material in draw_materials:
                material.move(n_resources_to_move=1, from_stock=True, to_player=player)
            # Delete the player color.
            del player_color_names[arg_player_color_name]
        if n_human_players > 1:
            usage('Le nombre de joueurs humains ' + str(n_human_players) + ' doit être d\'au plus 1.')
        pass

    def init_gui(self) -> None:
        """
        Initialization of all frames of the graphical user interface.
        """
        self.table.init_frame_table(self)
        self.game_element.init_frame_stock()
        self.init_frame_game()
        self.init_frame_players()
        self.gui.init_frame_translate()

    def init_frame_game(self) -> None:
        """
        Initialization of the frame on some data of the game of the graphical user interface.
        """
        # Frame on some data of the game.
        self.frame_game: LabelFrame = ttk.LabelFrame(master=self.gui.root, text='Jeu')  # Frame on some data of the Game.
        self.frame_game.grid(row=2, column=0, rowspan=self.game_element.n_max_players)
        # Frame for the turn.
        self.widget_turn = ttk.LabelFrame(master=self.frame_game, text='Manche')
        self.widget_turn.grid(row=0, column=0)
        self.create_widget_turn()
        # Frame for (ordered) players.
        n_players: int = len(self.players)  # Number of players.
        self.widget_players: LabelFrame = ttk.LabelFrame(master=self.frame_game,
                                                    text=str(n_players) + ' joueurs')  # Widget for the players.
        self.widget_players.grid(row=0, column=1, rowspan=n_players)
        for i_player, player in enumerate(sorted(self.players)):
            ttk.Label(master=self.widget_players, text=player.name()).grid(row=i_player, column=0)
        # Frame for the number of victory points to reach for a player to win.
        self.widget_n_victory_pts_to_reach: LabelFrame = \
            ttk.LabelFrame(master=self.frame_game, text='PV à atteindre')  # Widget for the number of VPs to reach.
        self.widget_n_victory_pts_to_reach.grid(row=0, column=2)
        ttk.Label(master=self.widget_n_victory_pts_to_reach,
                  text=n2str_txt_label_widget(self.game_element.n_victory_pts_to_reach_for_players[n_players])
                  ).grid(row=0, column=0)

    def init_frame_players(self) -> None:
        """
        Initialization of the frame for the players of the graphical user interface.
        """
        # Frame for the players.
        frame_players: LabelFrame = ttk.LabelFrame(master=self.gui.root, text='Joueurs')  # Frame for the players.
        frame_players.grid(row=0, column=1, rowspan=len(self.players))
        # Frame for all (ordered) players.
        for i_player, player in enumerate(sorted(self.players)):
            player.init_frame_player(frame_players, i_player)

    def create_widget_turn(self) -> None:
        """
        Create a widget for the turn.
        """
        if self.turn:
            ttk.Label(master=self.widget_turn, text=n2str_txt_label_widget(self.turn)).grid(row=0, column=0)

    def display(self, playing_order_players: List[Player]) -> None:
        """
        Display the game (for one turn).
        :param playing_order_players: Playing order among all players.
        """
        if self.turn is not None and not self.is_game_over:
            printing(0, LINE_SEPARATOR)
            printing(0, 'Manche : ' + str(self.turn))
        self.display_stock(1)
        self.display_table(1)
        self.display_players(0, playing_order_players)

    def display_stock(self, n_indent_to_add: int) -> None:
        """
        Display the elements in the stock.
        :param n_indent_to_add: Number of indentations to add.
        """
        printing(0 + n_indent_to_add,
                 'Réserve : ' + ', '.join([str(resource.number) + ' ' + resource.name
                                           for resource in sorted(self.game_element.resources.keys())]))

    def display_table(self, n_indent_to_add: int) -> None:
        """
        Display the elements of the table.
        :param n_indent_to_add: Number of indentations to add.
        """
        printing(0 + n_indent_to_add, 'Au centre de la table :')
        printing(1 + n_indent_to_add, 'Bâtiments :')
        for i_row, building_row in enumerate(self.table.buildings):
            printing(2 + n_indent_to_add,
                     'Ligne n° ' + str(i_row + 1) + ' (de ' + str(len(building_row)) + ' bâtiments) :')
            for building in building_row:
                printing(3 + n_indent_to_add, str(building))
        printing(2 + n_indent_to_add, 'Bâtiments achetables :')
        for building in self.table.buildings_extremities_rows():
            printing(3 + n_indent_to_add, str(building))
        printing(1 + n_indent_to_add,
                 'Ressources : ' + ', '.join([str(number) + ' ' + resource.name
                                              for resource, number in sorted(self.table.resources.items())]))

    def display_players(self, n_indent_to_add: int, playing_order_players: List[Player]) -> None:
        """
        Display the elements of the players.
        :param n_indent_to_add: Number of indentations to add.
        :param playing_order_players: Playing order among all players.
        """
        printing(1 + n_indent_to_add, str(len(self.players)) + ' joueurs (dans l\'ordre de leurs tours de jeu) :')
        for player in playing_order_players:
            self.display_player(n_indent_to_add, player)

    @classmethod
    def display_player(cls, n_indent_to_add: int, player: Player) -> None:
        """
        Display the elements of the player.
        :param n_indent_to_add: Number of indentations to add.
        :param player: Player to display.
        """
        printing(2 + n_indent_to_add, str(player))

    def play(self) -> None:
        """
        Play the game (course of the game).
        ~~~~~
        Each round consists of the following 3 consecutive phases:
        First, all players, separately and in turn, carry out 1st phase
        (“carry out two actions and purchase buildings”).
        In 2nd phase (“supplies”), 3 new Pesos and 3 new building materials are placed from the stock
        in the center of the table.
        In the concluding 3rd phase (“new action card”), all players place exactly 1 new action card
        in front of themselves.
        """
        # End the initialization (preparation) of the (ordered) players by revealing the actions.
        messagebox_showinfo('Fin de la préparation (actions cachées)',
                            'Durant cette phase de préparation,\n'
                            + 'chaque joueur a pris les premières actions de sa main\n'
                            + 'et les a placées faces cachées.\n')
        for player in sorted(self.players):
            player.reveal_actions()
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Fin de la préparation (actions révélées)',
                            'Durant cette phase de préparation,\nles premières actions des joueurs sont révélées.\n')
        # Start of the game.
        self.is_game_over = False
        self.turn = 0
        self.create_widget_turn()
        while not self.is_game_over:
            # New turn.
            self.turn = self.turn + 1
            self.create_widget_turn()
            messagebox_showinfo('Nouvelle manche', 'Manche ' + str(self.turn) + '.')
            self.is_conservation_played = False
            playing_order_players: List[Player] = self.playing_order_players()  # Playing order among all players.
            self.display(playing_order_players)
            # Players carries out actions and can purchase buildings.
            self.play_carry_out_actions_purchase_buildings(playing_order_players)
            # Supplies and players take new actions.
            if not self.is_game_over:
                self.play_supplies()
                self.play_new_actions(playing_order_players)
        # End of the game.
        self.end_of_game()
        pass

    def playing_order_players(self) -> List[Player]:
        """
        Playing order among all players.
        :return: Playing order among all players.
        """
        playing_order_players: List[Player] = []  # Playing order among all players.
        playing_order_details_players: Dict[Player, List[int]] = \
            {player: player.playing_order_details() for player in self.players}  # Playing order details of all players.
        while playing_order_details_players:
            min_playing_order_details: List[int] = \
                min(playing_order_details_players.values())  # Minimum of playing order details.
            players_min_playing_order_details: List[Player] = \
                [player for player, playing_order_details in playing_order_details_players.items() if
                 playing_order_details == min_playing_order_details]  # Players having minimum of playing order details.
            random.shuffle(players_min_playing_order_details)  # Instead of "If they tie, the younger player begins.".
            playing_order_players.extend(players_min_playing_order_details)
            for player_min_playing_order_details in players_min_playing_order_details:
                del playing_order_details_players[player_min_playing_order_details]
        return playing_order_players

    def play_carry_out_actions_purchase_buildings(self, playing_order_players: List[Player]):
        """
        Play the 1st phase: (the players can) carry out two actions and purchase buildings.
        Remark: Can set the game attribute indicating the game is over.
        :param playing_order_players: Playing order among all players.
        ~~~~~
        - The player whose laid-out cards show the lowest number begins and carries out the actions of his 2 cards,
          in any order.
          The player with the second lowest number takes his turn and carries out his two actions in any order.
          Now, the player with the third lowest two-digit number goes, and so on, until each player has taken their turn
          and carried out his 2 actions.
        - The actions are explained in detail on the cards! The 2 action cards remain lying face-up.
          Note: An action cannot be skipped- unless the respective card expressly allows it.
        - Playing order in case of identical numbers:
          In case several players show the same number, the player among them owning the fewest victory points begins.
          If their number of victory points is also the same, the player owning the fewest colored building materials
          in total begins.
          If there still is a tie, the one owning the fewest Pesos begins.
          Then the fewest workers.
          Then the least gray building materials (debris).
          If they tie in all respects, the younger player begins.
        - Purchasing buildings: On a player’s turn, after he has carried out both of his actions, he can buy any number
          of the face-up building cards.
          In order to do so, he must fulfill/hand over what is shown on that particular building card.
          * Building materials and gray debris must be handed over and returned to the box (removed from the game).
          * Pesos and workers are returned to the table edge.
          * If the architect is required, the player must have him laid-out in front of himself now,
            i.e. as one of his currently laid-out action cards.
            The architect is not handed over after the purchase of a building – he remains where he is!
        - Very important: In principle, buildings can only be purchased if they lay on the far left or the far right
          of the two rows of cards – this makes exactly 4 available cards at all times.
          During the course of the game, the cards are essentially purchased “from the outside to the inside”.
          A player may choose in which order he purchases which card(s) in which row.
          For example, provided he has sufficient resources, he can first buy the upper left building card,
          then the lower right one and immediately another lower right one.
          Note: Only the current player in phase 1 may purchase buildings – no one else.
                In the 2nd and 3rd phases no buildings at all can be purchased.
        - “5 to 1“-swap during building purchase
          If a player purchases a building, he may choose to pay 5 Pesos instead of a worker (back to the stock).
          Also, 5 gray building material cubes (debris) can be paid instead of any one colored cube
          of building material (removed from the game to the box).
          Note: It is not possible to swap workers for Pesos or colored building materials for gray building materials!
        - If in the course of the game only 2 cards remain in a row, immediately draw 4 new buildings
          from the face-down draw pile at the table edge and place them face-up between the 2 old cards.
        further course of the game: All of the following rounds are played just as described.
        """
        for i_playing_order_players, player in enumerate(playing_order_players):
            if not self.is_game_over:
                self.play_carry_out_actions(player, i_playing_order_players,
                                            playing_order_players[:i_playing_order_players],
                                            playing_order_players[i_playing_order_players + 1:])
                if not self.is_game_over:
                    self.play_purchase_buildings(player)
        pass

    def play_carry_out_actions(self, player: Player, i_playing_order_players: int,
                               before_playing_order_players: List[Player],
                               after_playing_order_players: List[Player]) -> None:
        """
        [1st subphase of 1st phase] The player have to carry out revealed actions.
        Remark: Action Conservation can set the game attribute indicating the game is over.
        Remark: Action Conservation can set the game attribute indicating the action Conservation was already played
                during the turn.
        :param player: The player whose turn it is.
        :param i_playing_order_players: Order of the player during this turn.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(0, 'Tour de jeu du joueur ' + player.name() + ' (n° ' + str(i_playing_order_players + 1)
                 + ' dans l\'ordre du tour de jeu ' + str(self.turn) + ').')
        printing(1, str(player))
        remaining_revealed_actions: Set[Action] = \
            player.get_actions(ActionLocation.REVEALED)  # Remaining revealed actions of the player.
        while remaining_revealed_actions and not self.is_game_over:
            action_to_carry_out: Action = first(list(remaining_revealed_actions)) if len(
                remaining_revealed_actions) == 1 else player.choose_action_to_carry_out(
                remaining_revealed_actions)  # Action to carry out.
            printing(1, 'Action effectuée : ' + action_to_carry_out.name + '.')
            action_to_carry_out.play(player=player, the_game=self,
                                     before_playing_order_players=before_playing_order_players,
                                     after_playing_order_players=after_playing_order_players)
            if not self.is_game_over:
                printing(2, str(player))
            remaining_revealed_actions.remove(action_to_carry_out)
        pass

    def play_purchase_buildings(self, player: Player):
        """
        [2nd subphase of 1st phase] The player can purchase buildings.
        Remark: Can set the game attribute indicating the game is over.
        :param player: The player whose turn it is.
        """
        is_play_purchase_buildings_ended: bool = False  # Is this subphase the player can purchase buildings ended?
        while not is_play_purchase_buildings_ended:
            # Get all buildings with their coordinates at the extremities of all rows of the table.
            buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
                self.table.buildings_extremities_rows_and_coord()
            # Set all buildings the player can purchase with some resources.
            buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]] = \
                dict()  # Buildings the player can purchase with some resources and with architect if required.
            for building in buildings_extremities_rows_and_coord.keys():
                # If the building requires the architect, the player must have play it during this turn.
                if (not building.is_architect_required) or player.has_architect():
                    # Get all payable resources for all equivalent resources to purchase a building.
                    all_payable_equiv_building_resources: List[
                        T_Resources_int] = player.all_payable_equiv_building_resources(
                        building.all_equiv_building_resources(self.game_element.resources_swap))
                    if all_payable_equiv_building_resources:
                        buildings_purchasable_with_resources[building] = all_payable_equiv_building_resources
            # The payer purchases or not a building at the extremities of all rows of the table.
            if not buildings_purchasable_with_resources:
                #  The player cannot purchase any building.
                printing(1, 'Le joueur ' + player.name() + ' ne peut pas acheter de bâtiment.')
                is_play_purchase_buildings_ended = True
            else:
                choose_building_to_purchase: Union[None, Tuple[Building, T_Resources_int]] = \
                    player.choose_building_to_purchase(
                        buildings_purchasable_with_resources)  # The player chooses (or not) a building to purchase.
                if choose_building_to_purchase is None:
                    # The player does not want to purchase any purchasable building.
                    printing(1, 'Le joueur ' + player.name() + ' ne veut pas acheter de bâtiment parmi le(s) '
                             + str(len(buildings_purchasable_with_resources.keys())) + ' qu\'il pourrait acheter.')
                    is_play_purchase_buildings_ended = True
                else:
                    # The player purchases a purchasable building.
                    printing(1, 'Le joueur ' + player.name() + ' achète l\'un des '
                             + str(len(buildings_purchasable_with_resources.keys())) + ' bâtiment(s).')
                    building_to_purchase, cost_resources_to_purchase_building = \
                        choose_building_to_purchase  # Building to purchase and its cost resources.
                    if building_to_purchase is None:
                        raise ValueError('The player does not chose a correct building to purchase.')
                    if cost_resources_to_purchase_building is None:
                        raise ValueError('The player does not chose correct cost resources to purchase a building.')
                    self.player_purchase_building_resources(player,
                                                            buildings_extremities_rows_and_coord[building_to_purchase],
                                                            building_to_purchase, cost_resources_to_purchase_building)
                    # Is the game over?
                    self.check_is_game_over(player)
                    if self.is_game_over:
                        is_play_purchase_buildings_ended = True
                    else:
                        pass  # The game is not over and the player can perhaps purchase another building.
        pass

    def player_purchase_building_resources(self, player: Player, building_coord: Tuple[int, int], building: Building,
                                           cost_resources: T_Resources_int):
        """
        The player purchases a building with some resources.
        We may have to update the table.
        :param player: The player whose turn it is purchases a building.
        :param building_coord: Coordinates of the building in the table.
        :param building: The building the player wants to purchase.
        :param cost_resources: The (cost) resources the player has to pay for the building.
                               Remark: All numbers of resources are <= 0.
        """
        # Display the old situation.
        printing(1, 'Achat d\'un bâtiment :')
        printing(2, 'Le bâtiment ' + str(building) + ' du centre de la table va être acheté par le joueur '
                 + player.name() + '.')
        self.display_player(1, player)
        self.display_table(3)
        # The player pays resources for the building.
        for resource, number in cost_resources.items():
            resource.move(n_resources_to_move=-number, from_player=player, to_stock=True)
        player.create_widgets_frame_resources()
        self.game_element.create_widgets_frame_resources()
        # The player obtains the building which is deleted from the table.
        player.buildings.add(building)
        player.create_widget_n_tot_victory_pts()
        player.create_widget_n_buildings()
        # The building at some extremity of some row of the table is deleted.
        self.delete_building(building_coord)
        # Display the new situation.
        printing(2, 'Le bâtiment ' + str(building) + ' du centre de la table vient d\'être acheté par le joueur '
                 + player.name() + '.')
        self.display_player(1, player)
        self.display_table(3)

    def delete_building(self, building_coord: Tuple[int, int]) -> None:
        """
        Delete the building in the (j_row)th entry from the (i_row)th row at the extremities of all rows of the table.
        If there is not enough buildings in the updated row, draw new buildings and place them in the updated row.
        :param building_coord: Coordinates of the building in the table.
        """
        # Delete the building in the (j_row)th entry from the (i_row)th row at the extremities of all rows of the table.
        (i_row, j_row) = building_coord  # Coordinates of the building in the table.
        del self.table.buildings[i_row][j_row]  # Delete (j_row)th entry from (i_row)th row.
        self.table.create_widgets_frame_buildings_table(self.game_element)
        # If there is not enough buildings in the updated row, draw new buildings and place them in the updated row.
        n_buildings_row_updated: int = len(self.table.buildings[i_row])  # New number of buildings in the updated row.
        if n_buildings_row_updated > 2:
            pass  # Nothing to do (there is enough buildings in the updated row)!
        else:
            draw_buildings: List[Building] = \
                self.game_element.draw_buildings(
                    n_draw_buildings=self.game_element.n_revealed_buildings_by_row - n_buildings_row_updated,
                    only_in_pile=True)  # Draw buildings from the face-down draw pile at the table edge.
            if draw_buildings:
                if n_buildings_row_updated == 0:
                    self.table.buildings[i_row] = draw_buildings
                elif n_buildings_row_updated == 1:
                    self.table.buildings[i_row] = draw_buildings \
                                                  + ([self.table.buildings[i_row][0]] if j_row == 0 else
                                                     [self.table.buildings[i_row][0]]) \
                                                  + draw_buildings
                else:  # n_buildings_row_updated == 2
                    self.table.buildings[i_row] = [self.table.buildings[i_row][0]] + draw_buildings \
                                                  + [self.table.buildings[i_row][1]]
                printing(2, 'De nouveaux bâtiments ont été placés au centre de la table.')
                self.table.create_widgets_frame_buildings_table(self.game_element)
                self.table.create_widget_n_buildings_in_pile(self.game_element)
            else:
                printing(2, 'Aucun nouveau bâtiment n\'a pu être placé au centre de la table.')
        pass

    def play_supplies(self) -> None:
        """
        Play the 2nd phase: supplies.
        ~~~~~
        - Draw 3 cubes from the bag and place them in the center of the table.
          Additionally, place 3 Pesos from the stock in the center of the table.
          If there are still Pesos and/or building materials left from 1st phase (and/or earlier turns)
          in the center of the table, they remain where they are.
          Pesos and building materials may accumulate in the center of the table over the course of several rounds.
        """
        # Update resources from stock to the table during supplies.
        for resource, game_element_resource in self.game_element.resources.items():
            resource.move(n_resources_to_move=min(game_element_resource.n_supplies, resource.number),
                          from_stock=True, to_table=self.table)
        # Draw material resources from stock to the table during supplies.
        draw_materials: List[MaterialResource] = \
            self.game_element.draw_materials(self.game_element.n_draw_materials_supplies)  # Draw materials during supplies.
        for material in draw_materials:
            material.move(n_resources_to_move=1, from_stock=True, to_table=self.table)
        # Update resources of the GUI.
        self.game_element.create_widgets_frame_resources()
        self.table.create_widgets_frame_resources()
        pass

    def play_new_actions(self, playing_order_players: List[Player]) -> None:
        """
        Play the 3rd phase: new action card.
        :param playing_order_players: Playing order among all players.
        ~~~~~
        - Separately and in turn, each player takes exactly 1 of the cards from his hand and places it
          face-down on top of any of the two action cards laid-out in front of him.
          Note: This happens in exactly the same order as the actions were carried out in 1st phase
                (according to the numbers formed)!
        - Once every player has laid down 1 of his cards, they are revealed.
          The old card underneath is placed face-down next to each player (on his own discard pile).
          Now, each player again arranges his 2 face-up cards by the shown numbers in rising order from left to right,
          in order to form the smallest possible number.
          For example, a 2 and an 8 must always be arranged as 28 – never as 82! A 6 and a 0 always become 06, never 60!
        - Very important: If a player now, i.e. after revealing the face-down action card, holds no more than
                          2 action cards in his hand, he may now take all of his discarded action cards back to his hand
                          – he once again has 11 hand cards at his disposal.
        """
        # Choose (the same number of) actions from revealed to discarded and from his hand to face-down.
        for player in playing_order_players:
            player.choose_new_actions(self.game_element.n_new_actions)
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Nouvelles actions choisies',
                            'Chaque joueur a pris des actions de sa main\net les a placées faces cachées\n'
                            + 'sur des actions précédemment révélées\n(et qui ont déjà été défaussées).\n')
        # Set (all) actions from face-down to revealed.
        for player in self.players:
            player.reveal_actions()
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Nouvelles actions révélées', 'Les actions placées faces cachées ont été révélées.')
        # In particular case, each player can take all discarded actions back to his hand.
        for player in self.players:
            player.take_actions_back(self.game_element.n_min_actions_in_hand)
        pass

    def check_is_game_over(self, player: Player = None):
        """
        Check if the game is over (and set if it the case).
        The game ends when either not exists building or not exists payable building (considering all resources,
        but without considering if the architect is required or not because players can have action Architect
        in their hand by refreshing or when they obtain all cards from discarded pile)
        or the player reaches the number of victory points.
        ~~~~~
        If, in a two-player game, one player has 25 victory points or more laying in front of him after 1st phase,
        the game ends immediately with him the winner – the round will not be finished!
        In a three-player game, 20 victory points must be gained like this,
        and 15 victory points in a four-player game.
        :param player: Player to verify if he wins the game.
        """
        if self.is_game_over:
            printing(0, 'La partie est déjà terminée.')
            pass  # Nothing to redo!
        elif player is not None and player.n_tot_victory_pts() >= \
                self.game_element.n_victory_pts_to_reach_for_players[len(self.players)]:
            # Game over if the player reaches the number of victory points.
            printing(0, 'La partie se termine par le gagnant ' + player.name()
                     + ' qui avec ' + str(player.n_tot_victory_pts())
                     + ' points de victoire a atteint ou dépassé les '
                     + str(self.game_element.n_victory_pts_to_reach_for_players[len(self.players)])
                     + ' requis pour une partie à ' + str(len(self.players)) + ' joueurs.')
            self.is_game_over = True
        elif self.game_element.buildings_in_pile():
            # It is possible that a building in the nonempty face-down draw pile (at the table edge)
            # first will be placed in the center of the table and next will be purchased by a player.
            pass  # The game is not over!
        elif self.game_element.n_buildings_in_location(BuildingLocation.TABLE) == 0:
            # Game over if the face-down draw pile (at the table edge) of buildings and
            # all rows in the center of the table are both empty.
            printing(0, 'La partie se termine car il n\'y a plus de bâtiment '
                     + 'ni dans la pile ni au centre de la table ('
                     + str(self.game_element.n_buildings_in_location(BuildingLocation.PURCHASED)) + ' achetés' + ', '
                     + str(self.game_element.n_buildings_in_location(BuildingLocation.REMOVED)) + ' supprimés' + ')'
                     + '.')
            self.is_game_over = True
        elif list(filter(lambda building_row: len(building_row) > 2, self.table.buildings)):
            # The draw pile of buildings is empty, the center of the table is not empty and some buildings
            # inside (e.g. not at the extremities of) some rows first will become at the extremities and
            # next will be purchased by a player.
            pass  # The game is not over!
        else:
            # The draw pile of buildings is empty.
            # The center of the table is not empty but each row contains 0 or 1 or 2 buildings.
            # Game over if not exists a payable building among all buildings at the extremities of all rows
            # in the center of the table with sufficient available resources everywhere
            # (owned by all players, on the table, and in the stock) to pay for it.
            # We ignore the architect because this action can be already available or can be reintroduce later.
            # Get all equivalent (by applying “5 to 1“-swap resources) available
            # (owned by all players, on the table, and in the stock) resources.
            all_equiv_available_resources: List[T_Resources_int] = \
                Resource.all_equiv_resources({resource: -(sum([player.resources[resource] for player in self.players])
                                                          + self.table.resources[resource]
                                                          + resource.number)
                                              for resource in self.game_element.resources.keys()},
                                             self.game_element.resources_swap)
            is_game_over_in_this_case: bool = True  # Is the game over (in this case)?
            for building in self.table.buildings_extremities_rows():
                if is_game_over_in_this_case:
                    for one_equiv_available_resources in all_equiv_available_resources:
                        if is_game_over_in_this_case:
                            for resource in self.game_element.resources.keys():
                                one_equiv_available_resources[resource] = -one_equiv_available_resources[resource]
                            if building.is_payable(one_equiv_available_resources):
                                is_game_over_in_this_case = False
            if is_game_over_in_this_case:
                printing(0, 'La partie se termine par manque de ressources '
                         + '(des joueurs, au centre de la table, de la réserve) '
                         + 'pour pouvoir acheter ne serait-ce que l\'un quelconque des '
                         + str(len(self.table.buildings_extremities_rows()))
                         + ' bâtiments aux extrémités des lignes au centre de la table.')
                self.is_game_over = True
            else:
                pass  # The game is not over!
        pass

    def end_of_game(self) -> None:
        """
        Display the end of the game (winners e.g. players by decreasing total number of victory points).
        """
        one_line_end_of_game: str = str(len(self.players)) + ' joueurs. ' \
                                    + str(self.turn) + ' manches.'  # End of the game on one line to print.
        printing(0, LINE_SEPARATOR)
        printing(0, 'Fin de la partie :')
        self.display(list(self.players))
        players_vp: Dict[Player, int] = {player: player.n_tot_victory_pts()
                                         for player in self.players}  # Total number of victory points of all players.
        while players_vp:
            max_vp: int = max(players_vp.values())  # Maximum of victory points.
            printing(1, str(max_vp) + ' PV :')
            one_line_end_of_game = one_line_end_of_game + ' ' + str(max_vp) + ' PV pour'
            players_max_vp: List[Player] = \
                [player for player, playing_order_details in sorted(players_vp.items())
                 if playing_order_details == max_vp]  # Players having maximum of victory points.
            for player_max_VPs in players_max_vp:
                printing(2, str(player_max_VPs))
                one_line_end_of_game = one_line_end_of_game + ' ' + player_max_VPs.name()
                del players_vp[player_max_VPs]
            one_line_end_of_game = one_line_end_of_game + (' ;' if players_vp else '.')
        print(one_line_end_of_game)  # Print, in verbose mode or not, this one line end of game.
        messagebox.showinfo('Fin de la partie', one_line_end_of_game.replace('. ', '.\n\n').replace(' ; ', '\n'))
        pass


