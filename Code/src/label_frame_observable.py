from abc import ABC, abstractmethod
from tkinter import LabelFrame
from typing import List


class Observer(ABC):

    @abstractmethod
    def update(self) -> None:
        pass


class WidgetObservable:
    def __init__(self, frame: LabelFrame):
        self.frame = frame

    _observers: List[Observer] = []

    def attach_observer(self, observer: Observer) -> None:
        self._observers.append(observer)

    def detach_observer(self, observer: Observer) -> None:
        self._observers.remove(observer)

    def _notify(self) -> None:
        for observer in self._observers:
            observer.update()

    def set_text(self, text: str):
        self.frame['text'] = text
        self._notify()
