#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
from tkinter import *
from tkinter import messagebox
from typing import Dict, List, TypeVar


VERBOSE_CONSOLE: bool = True  # Verbose mode on the console e.g. print messages.
VERBOSE_GUI: bool = False  # Verbose mode for the graphical user interface e.g. print messages to stop the program.
TXT_INDENT: str = '  '  # Text for one indentation.
LINE_SEPARATOR: str = '~~~~~'  # Line separator.

COLOR_REMOVED = 'magenta'  # Color of a removed element for the GUI.


def printing(n_indent: int, message: str) -> None:
    """
    In verbose mode on the console, print the message.
    :param n_indent: Number of indentations.
    :param message: Message.
    """
    if VERBOSE_CONSOLE:
        print((TXT_INDENT * n_indent) + message)


def messagebox_showinfo(title: str, message: str) -> None:
    """
    In verbose mode for the graphical user interface, show a message box.
    :param title: Title.
    :param message: Message.
    """
    if VERBOSE_GUI:
        messagebox.showinfo(title, message)


def usage(error_msg: str) -> None:
    """
    Usage of the command.
    :param error_msg: Error message.
    """
    print(error_msg)
    print('Usage : python ' + sys.argv[0] + ' <fichier_XML> <couleur>[=<nom_AI>] ... <couleur>[=<nom_AI>]')
    print('Exemple de 4 joueurs (humain (en 2de pos., green) face à 2 IA basic et 1 IA advanced) :'
          + '\n\tpython ' + sys.argv[0] + ' game_elements-Havana.xml red=Basic green yellow=Advanced blue=Basic')
    exit(1)


T = TypeVar('T')  # Declare some generic type variable or class.


def n_random_elements_from_sequence(n_elements: int, sequence: List[T]) -> List[T]:
    """
    Random some elements from a sequence.
    :param n_elements: Number of elements wanted from the sequence.
    :param sequence: Sequence of elements.
    :return: Some random elements from a sequence.
    """
    local_sequence: List[T] = sequence.copy()  # local copy of the sequence.
    random.shuffle(local_sequence)
    return local_sequence[:n_elements]  # If len(local_sequence) <= n_elements, returns local_sequence.


def n_random_elements_from_frequencies(n_elements: int, frequencies: Dict[T, int]) -> List[T]:
    """
    Random some elements according to the frequencies.
    :param n_elements: Number of elements wanted from the frequencies.
    :param frequencies: Frequencies e.g. some nonnegative number for each T.
    :return: Some random elements according to the frequencies.
    """
    return n_random_elements_from_sequence(n_elements, [e for e, n in frequencies.items() for _ in range(n)])


def first(sequence: List[T]) -> T:
    """
    First element of a sequence.
    :param sequence: Sequence of elements.
    :return: First element of a sequence.
    """
    return sequence[0]


def n2str_txt_label_widget(n: int) -> str:
    """
    String for a number into the text of a label of a widget of the graphical user interface.
    :param n: Number.
    :return: String for a number into the text of a label of a widget of the graphical user interface.
    """
    spaces_before_after: str = ' ' * 8  # Spaces before and after le string for the number.
    return spaces_before_after + str(n) + spaces_before_after


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]