import abc
from functools import total_ordering
from tkinter import ttk
from typing import Dict

from utils import *


@total_ordering
class Resource:
    """
    Resources: worker, coin and material resources.
    """

    def __init__(self, name: str, number: int):
        """
        Initialization of a resource.
        :param name: Name of the resource.
        :param number: Number of the resource.
        """
        self.name: str = name  # Name of the resource.
        self.number: int = number  # Number of the resource.
        self.number_removed: int = 0  # Number of the resource removed from the game (returned to the box)

    def __lt__(self, other_resource):
        """
        Specify the order between all resources. Is the resource is lower than the other resource?
        :param other_resource: Another resource.
        :return: Is the resource is lower than the other resource?
        """
        if isinstance(self, WorkerResource):
            #  Either equals if isinstance(other_resource, WorkerResource)
            #  or lower if isinstance(other_resource, (CoinResource, MaterialResource)).
            return True
        if isinstance(self, CoinResource):
            #  Either equals if isinstance(other_resource, CoinResource)
            #  or lower if isinstance(other_resource, MaterialResource).
            return not isinstance(other_resource, WorkerResource)
        if not isinstance(other_resource, MaterialResource):
            return False
        else:
            if not isinstance(self, MaterialResource) or not isinstance(other_resource, MaterialResource):
                raise ValueError('Impossible to compare two (material) resources.')
            if self.is_colored and not other_resource.is_colored:
                return True
            elif not self.is_colored and other_resource.is_colored:
                return False
            else:
                return self.name <= other_resource.name

    @abc.abstractmethod
    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        pass

    @classmethod
    def text_is_cost_after_purchasing_removed(cls, is_cost_after_purchasing_removed: bool) -> str:
        """
        Text corresponding to the resource once consumed:
        either removed from the game (returned to the box) or returned to the table edge.
        :param is_cost_after_purchasing_removed: Once the resource is consumed, is the cost removed?
        :return: Text corresponding to the resource once consumed:
                 either removed from the game (returned to the box) or returned to the table edge.
        """
        return '(supprimé du jeu)' if is_cost_after_purchasing_removed else '(remis en jeu)'

    def move(self, n_resources_to_move: int,
             from_stock: bool = None, from_table: 'Table' = None, from_player: 'Player' = None,
             to_stock: bool = None, to_table: 'Table'=None, to_player: 'Player'=None) -> None:
        """
        Move some number of this resource from
        either the stock or the table or a player to either the stock or the table or a player.
        :param n_resources_to_move: Number of this resource to move.
        :param from_stock: Move some number of this resource from the stock.
        :param from_table: Move some number of this resource from the table.
        :param from_player: Move some number of this resource from a player.
        :param to_stock: Move some number of this resource to the stock.
        :param to_table: Move some number of this resource to the table.
        :param to_player: Move some number of this resource to a player.
        """
        if n_resources_to_move < 0:
            raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                             + ' must be nonnegative.')
        elif (n_resources_to_move == 0) \
                or (from_stock is not None and to_stock is not None) \
                or (from_table is not None and to_table is not None) \
                or (from_player is not None and to_player is not None and from_player == to_player):
            pass  # Nothing to do: additive identity!
        else:
            if from_stock is not None and from_table is None and from_player is None:
                if self.number < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(self.number)
                                     + ' from the stock.')
                else:
                    self.number = self.number - n_resources_to_move
            elif from_stock is None and from_table is not None and from_player is None:
                if from_table.resources[self] < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(from_table.resources[self])
                                     + ' from the table.')
                else:
                    from_table.resources[self] = from_table.resources[self] - n_resources_to_move
            elif from_stock is None and from_table is None and from_player is not None:
                if from_player.resources[self] < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(from_player.resources[self])
                                     + ' from the player ' + from_player.name() + '.')
                else:
                    from_player.resources[self] = from_player.resources[self] - n_resources_to_move
            else:
                raise ValueError('Only one among the stock, the table, a player is possible to move from.')
            if to_stock is not None and to_table is None and to_player is None:
                if self.cost_after_purchasing_to_remove():
                    self.number_removed = self.number_removed + n_resources_to_move
                else:
                    self.number = self.number + n_resources_to_move
            elif to_stock is None and to_table is not None and to_player is None:
                to_table.resources[self] = to_table.resources[self] + n_resources_to_move
            elif to_stock is None and to_table is None and to_player is not None:
                to_player.resources[self] = to_player.resources[self] + n_resources_to_move
            else:
                raise ValueError('Only one among the stock, the table, a player is possible to move to.')
        pass

    @classmethod
    def all_equiv_resources(cls, resources: 'Dict[Resource, int]', resources_swap: 'Dict[Tuple[Resource, Resource], int]'):
        """
        All equivalent (by applying “5 to 1“-swap resources) resources to some base resources.
        For instance, "5 Pesos for 1 worker" and "5 debris for 1 colored cube" with -10 pesos and -5 debris,
        we generate:
            (-10 pesos, -5 debris),    (-1 worker, -5 pesos, -5 debris),    (-2 workers, -5 debris),
            (-10 pesos, -1 brick),     (-1 worker, -5 pesos, -1 brick),     (-2 workers, -1 brick),
            (-10 pesos, -1 sandstone), (-1 worker, -5 pesos, -1 sandstone), (-2 workers, -1 sandstone),
            (-10 pesos, -1 loam),      (-1 worker, -5 pesos, -1 loam),      (-2 workers, -1 loam),
            (-10 pesos, -1 glass),     (-1 worker, -5 pesos, -1 glass),     (-2 workers, -1 glass).
        Remark: We suppose there is no cycle in all “5 to 1“-swap resources.
        Remark: All numbers (of resources) are <= 0.
        :rtype List[Dict[Resource, int]]
        :param resources: Base resources for which we generate all equivalent resources.
        :param resources_swap: Resources swap: number of resources obtained for 1 resource given
                               where key is (resource given, resource obtained).
        :return: All equivalent (by applying “5 to 1“-swap resources) resources to some base resources.
                 Remark: List[T_Resources_int] instead of Set[T_Resources_int] becomes to avoid error
                 "TypeError: unhashable type: 'dict'".
        """
        all_equiv_building_resources: List[T_Resources_int] = \
            [resources.copy()]  # All equivalent (by applying “5 to 1“-swap) resources to some base resources.
        for (resource_given, resource_obtained), number_for_1 in resources_swap.items():
            new_all_equiv_building_resources: List[T_Resources_int] = \
                list()  # New equivalent resources to some resources.
            for one_equiv_building_resources in all_equiv_building_resources:
                for multiplicity in range(0, 1 - one_equiv_building_resources[resource_obtained]):
                    new_one_equiv_building_resources = \
                        one_equiv_building_resources.copy()  # New one equivalent resources.
                    new_one_equiv_building_resources[resource_given] = \
                        new_one_equiv_building_resources[resource_given] - number_for_1 * multiplicity
                    new_one_equiv_building_resources[resource_obtained] = \
                        new_one_equiv_building_resources[resource_obtained] + 1 * multiplicity
                    new_all_equiv_building_resources.append(new_one_equiv_building_resources)
            all_equiv_building_resources = new_all_equiv_building_resources
        return all_equiv_building_resources

    @classmethod
    def create_widgets_frame_resources(cls, frame_resources: LabelFrame, resources: 'Dict[Resource,int]', with_removed: bool):
        """
        [Re]Create all the widgets of the frame for the resources of the graphical user interface.
        :param frame_resources: Frame of all resources.
        :param resources: Resources.
        :param with_removed: Removed resources have to be shown?
        """
        # Destroy all previous widgets.
        for widget_resource in frame_resources.winfo_children():
            widget_resource.destroy()
        # Create new widgets.
        # Frame for all the resources. For each resource: color of materials, name, number, and removed number.
        for i_resource, resource in enumerate(sorted(resources)):
            if isinstance(resource, MaterialResource):
                ttk.Label(master=frame_resources, text=' ' * 5, background=resource.color
                          ).grid(row=0, column=i_resource)
            ttk.Label(master=frame_resources, text=resource.name).grid(row=1, column=i_resource)
            ttk.Label(master=frame_resources, text=n2str_txt_label_widget(resources[resource])
                      ).grid(row=2, column=i_resource)
            if with_removed and resource.cost_after_purchasing_to_remove():
                ttk.Label(master=frame_resources, text=n2str_txt_label_widget(resource.number_removed),
                          foreground=COLOR_REMOVED).grid(row=3, column=i_resource)
        pass


class WorkerResource(Resource):
    """
    Worker (meeples) resources.
    """

    is_cost_after_purchasing_removed: bool = None  # Once the worker resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool):
        """
        Initialization of the worker resource.
        :param name: Name of the worker resource.
        :param number: Number of worker resources.
        :param is_cost_after_purchasing_removed: Once the worker resource is consumed, is the cost removed?
        """
        Resource.__init__(self, name, number)
        WorkerResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed

    def __str__(self) -> str:
        """
        Text for the worker resource.
        :return: Text for the worker resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(WorkerResource.is_cost_after_purchasing_removed)

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return WorkerResource.is_cost_after_purchasing_removed


class CoinResource(Resource):
    """
    Coin (Money) resources: peso(s).
    """

    is_cost_after_purchasing_removed: bool = None  # Once the coin resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool):
        """
        Initialization of the coin resource.
        :param name: Name of the coin resource.
        :param number: Number of coin resources.
        :param is_cost_after_purchasing_removed: Once the coin resource is consumed, is the cost removed?
        """
        Resource.__init__(self, name, number)
        CoinResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed

    def __str__(self) -> str:
        """
        Text for the coin resource.
        :return: Text for the coin resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(CoinResource.is_cost_after_purchasing_removed)

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return CoinResource.is_cost_after_purchasing_removed


class MaterialResource(Resource):
    """
    Material resources (cubes): brick, sandstone, loam, glass or debris.
    """

    is_cost_after_purchasing_removed: bool = None  # Once a material resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool, is_colored: bool, color: str):
        """
        Initialization of a material resource.
        :param name: Name of the material resource.
        :param number: Number of material resources.
        :param is_cost_after_purchasing_removed: Once the material resource is consumed, is the cost removed?
        :param is_colored: Is the material resource colored? (true iif the material resource is not debris)
        :param color: Material resource color.
        """
        Resource.__init__(self, name, number)
        MaterialResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed
        self.is_colored: bool = is_colored  # Is the material resource colored? (true iif the material is not debris)
        self.color: str = color  # Material resource color.

    def __str__(self) -> str:
        """
        Text for a material resource.
        :return: Text for a material resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(MaterialResource.is_cost_after_purchasing_removed) \
               + ', ' + ('colored' if self.is_colored else 'not colored') \
               + ', ' + self.color

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return MaterialResource.is_cost_after_purchasing_removed


T_Resources_int = Dict[Resource, int]  # Declare a type for some number of resources.