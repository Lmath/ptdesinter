.. Havana documentation master file, created by
   sphinx-quickstart on Thu Jul  9 16:13:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Havana's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Model file
==================
.. automodule:: src.actions
   :members:

.. automodule:: src.building
   :members:

.. automodule:: src.game
   :members:

.. automodule:: src.gui
   :members:

.. automodule:: src.havana
   :members:

.. automodule:: src.players
   :members:

.. automodule:: src.ressources
   :members:

.. automodule:: src.table
   :members:

.. automodule:: src.utils
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
