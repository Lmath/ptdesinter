import abc
import collections
import random
from tkinter import LabelFrame, ttk
from typing import Set, Dict, List, Union, Tuple

from actions import ActionLocation, Action
from building import Building
from ressources import MaterialResource, CoinResource, WorkerResource, Resource, T_Resources_int, first, \
    n2str_txt_label_widget, n_random_elements_from_frequencies


class PlayerColor:
    """
    Player color: red, green, blue and yellow.
    """

    def __init__(self, name: str):
        """
        Initialization of a player color.
        :param name: Name of a player color.
        """
        self.name: str = name  # Name of a player color.

    def __lt__(self, other_player_color):
        """
        Specify the order between all color players. Is the color player is lower than the other color player?
        :param other_player_color: Another color player.
        :return: Is the color player is lower than the other color player?
        """
        if self == other_player_color:
            return True
        else:
            return self.name <= other_player_color.name

    def __str__(self) -> str:
        """
        Text for a player color.
        :return: Text for a player color.
        """
        return self.name


class Player:
    """
    Players.
    """

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of a player."
        :param player_color: Player color.
        :param resources: Number of all resources the player has.
        :param buildings: Buildings the player has purchased.
        :param actions: Deck of actions the player has.
        """
        # Player color.
        self.player_color: PlayerColor = player_color  # Player color.
        # Resources.
        self.resources: T_Resources_int = resources  # Number of all resources the player has.
        self.frame_resources: LabelFrame = None  # Frame for all resources the player has.
        # Buildings.
        self.buildings: Set[Building] = buildings  # Buildings the player has purchased.
        self.widget_n_tot_victory_pts: LabelFrame = None  # Widget for the total number of victory points.
        self.widget_n_buildings: LabelFrame = None  # Widget for the number of buildings purchased.
        # Actions.
        self.actions: Dict[Action, ActionLocation] = actions  # Deck of actions the player has.
        self.frame_actions: LabelFrame = None  # Frame for all actions.

    def __lt__(self, other_player):
        """
        Specify the order between all players. Is the player is lower than the other player?
        :param other_player: Another player.
        :return: Is the player is lower than the other player?
        """
        if self == other_player:
            return True
        else:
            return self.name() <= other_player.name()

    def __str__(self) -> str:
        """
        Text for a player.
        :return: Text for a player.
        """
        return str(self.name()) + ' : ' \
               + str(self.n_tot_victory_pts()) + ' PV avec ' + str(len(self.buildings)) + ' bâtiment(s), ' \
               + ', '.join([str(number) + ' ' + resource.name
                            for resource, number in sorted(self.resources.items())]) + ', ' \
               + ', '.join([str(any_action_location) + ' = {'
                            + ', '.join([str(action.value) + ' ' + action.name
                                         for action, action_location in sorted(self.actions.items())
                                         if action_location == any_action_location]) + '}'
                            for any_action_location in ActionLocation])

    def name(self) -> str:
        """
        Default name of the player.
        :return: Default name of the player.
        """
        return '' + self.player_color.name + ''

    def is_human(self) -> bool:
        """
        Is it the human player? (or an artificial intelligence otherwise)
        :return: Is it the human player? (or an artificial intelligence otherwise)
        """
        return isinstance(self, HumanPlayer)

    def n_tot_victory_pts(self) -> int:
        """
        Total number of victory points.
        :return: Total number of victory points.
        """
        return sum([building.n_victory_pts for building in self.buildings])

    def playing_order_details(self) -> List[int]:
        """
        Playing order details.
        :return: Playing order details.
        """
        values_revealed_arranged_actions: List[int] = \
            sorted([action.value for action, action_location in self.actions.items()
                    if action_location == ActionLocation.REVEALED])  # Values of all revealed and arranged actions.
        return [
            # Lowest value for revealed and arranged actions.
            sum(pow(10, len(values_revealed_arranged_actions) - 1 - index) * value
                for index, value in enumerate(values_revealed_arranged_actions)),
            # Fewest victory points.
            self.n_tot_victory_pts(),
            # Fewest colored building materials.
            sum([number for resource, number in self.resources.items()
                 if isinstance(resource, MaterialResource) and resource.is_colored]),
            # Fewest Pesos.
            sum([number for resource, number in self.resources.items() if isinstance(resource, CoinResource)]),
            # Fewest workers.
            sum([number for resource, number in self.resources.items() if isinstance(resource, WorkerResource)]),
            # Least gray building materials (debris).
            sum([number for resource, number in self.resources.items()
                 if isinstance(resource, MaterialResource) and not resource.is_colored])
        ]

    def init_frame_player(self, frame_players: LabelFrame, i_player: int) -> None:
        """
        Initialization of the frame for the player of the graphical user interface.
        :param frame_players: Frame for the players.
        :param i_player: Index of the player.
        """
        # Frame for the player.
        frame_player: LabelFrame = ttk.LabelFrame(master=frame_players, text=self.name())  # Frame for the table.
        frame_player.grid(row=i_player, column=1)
        # Frame for the total number of victory points.
        self.widget_n_tot_victory_pts = ttk.LabelFrame(frame_player, text='Total des PV')  # Total number of VPs.
        self.widget_n_tot_victory_pts.grid(row=0, column=0)
        self.create_widget_n_tot_victory_pts()
        # Frame for the number of buildings.
        self.widget_n_buildings = ttk.LabelFrame(frame_player, text='Nombre de bâtiments')  # Number of buildings.
        self.widget_n_buildings.grid(row=0, column=1)
        self.create_widget_n_buildings()
        # Frame for the resources.
        self.frame_resources = ttk.LabelFrame(master=frame_player, text='Ressources')
        self.frame_resources.grid(row=0, column=2, rowspan=3, columnspan=len(self.resources))
        self.create_widgets_frame_resources()
        # Frame for the actions.
        self.frame_actions = ttk.LabelFrame(master=frame_player, text='Actions')  # Frame for the actions.
        self.frame_actions.grid(row=3, column=0, columnspan=3)
        self.create_widgets_frame_actions()

    def create_widget_n_tot_victory_pts(self) -> None:
        """
        Create a widget for the total number of victory points.
        """
        ttk.Label(master=self.widget_n_tot_victory_pts, text=n2str_txt_label_widget(self.n_tot_victory_pts())
                  ).grid(row=0, column=0)

    def create_widget_n_buildings(self) -> None:
        """
        Create a widget for the total number of buildings.
        """
        ttk.Label(master=self.widget_n_buildings, text=n2str_txt_label_widget(len(self.buildings))
                  ).grid(row=0, column=0)

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources, self.resources, False)

    def create_widgets_frame_actions(self) -> None:
        """
        [Re]Create all the widgets of the frame for the actions grouped by action location.
        """
        # Destroy all previous widgets.
        for widget_action in self.frame_actions.winfo_children():
            widget_action.destroy()
        # Create new widgets.
        for column, (txt_action_location, action_location, hidden) in \
                enumerate([('dans la main', ActionLocation.HAND, not self.is_human()),
                           ('cachées', ActionLocation.FACE_DOWN, not self.is_human()),
                           ('révélées', ActionLocation.REVEALED, False),
                           ('défaussées', ActionLocation.DISCARDED, not self.is_human())]):
            self.create_widgets_frame_actions_by_location(column, 'Actions ' + txt_action_location, action_location,
                                                          hidden)

    def create_widgets_frame_actions_by_location(self, column: int, txt_action_location: str,
                                                 action_location: ActionLocation, hidden: bool) -> None:
        """
        [Re]Create all the widgets of the frame for the actions for some action location.
        :param column: Column in the frame.
        :param txt_action_location: Text associated to the frame.
        :param action_location: Action location.
        :param hidden: Action hidden?
        """
        # Frame for the actions.
        frame_player_action: LabelFrame = \
            ttk.LabelFrame(master=self.frame_actions,
                           text=txt_action_location)  # Frame for the actions for some action location.
        frame_player_action.grid(row=0, column=column)
        # Frame for all the actions for some action location.
        i_column: int = 0  # Index of the column.
        for action, actual_action_location in sorted(self.actions.items()):
            if actual_action_location == action_location:
                widget_action: LabelFrame = action.new_widget(frame_player_action, hidden)  # Widget for the action.
                widget_action.grid(row=0, column=i_column)
                i_column = i_column + 1

    @abc.abstractmethod
    def choose_actions_preparation(self, n_actions: int) -> None:
        """
        Player chooses several of his action cards (in his hand)
        and places them face-down for the time being in front of himself.
        :param n_actions: Number of actions.
        """
        pass

    @abc.abstractmethod
    def choose_new_actions(self, n_new_actions: int) -> None:
        """
        Player takes new action cards from his hand
        and places it face-down on top of some action cards laid-out in front of him.
        :param n_new_actions: Number of new actions.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        """
        Choose to carry out one of the remaining revealed actions.
        :param remaining_revealed_actions: Remaining revealed actions to carry out.
        :return: Action to carry out.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        pass

    @abc.abstractmethod
    def choose_resource_to_delete(self, player: 'Player', resources: T_Resources_int, the_game:'Game' = None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        pass

    @abc.abstractmethod
    def choose_thieved_player(self, thievable_players: 'List[Player]', the_game:'Game' =None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        pass

    @abc.abstractmethod
    def choose_materials_to_thieve_player(self, other_players_with_resources: 'List[Tuple[Player, int, T_Resources_int]]'):
        """
        Choose one player and its (1 or 2) materials to thieve when performing the action Materials Thief.
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players whe can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (1 or 2) materials to thieve.
        """
        pass

    @abc.abstractmethod
    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        """
        Choose some number of building material resources when performing the action Mama.
        :param n_materials: Number of building materials to choose.
        :param material_resources: Building material resources among those the player have to chose.
        :return: Building material resources chosen.
        """
        pass

    def reveal_actions(self) -> None:
        """
        Reveal (all) action cards placed face-down.
        Remark: We do not arrange all revealed action cards according increasing values.
        """
        self.locate_all_actions(from_action_location=ActionLocation.FACE_DOWN,
                                to_action_location=ActionLocation.REVEALED)
        pass

    def take_actions_back(self, n_min_actions_in_hand: int) -> None:
        """
         If the player holds no sufficient action cards in his hand,
         he may now take all of his discarded actions back to his hand.
        :param n_min_actions_in_hand: Minimal number of actions.
        """
        if len([action for action, action_location in self.actions.items()
                if action_location == ActionLocation.HAND]) <= n_min_actions_in_hand:
            self.locate_all_actions(from_action_location=ActionLocation.DISCARDED,
                                    to_action_location=ActionLocation.HAND)
            self.create_widgets_frame_actions()
        pass

    def locate_all_actions(self, from_action_location: ActionLocation, to_action_location: ActionLocation) -> None:
        """
         Move all actions from an action location to an action location.
        :param from_action_location: Action location to move from.
        :param to_action_location: Action location to move to.
        """
        if from_action_location != to_action_location:
            for action, action_location in self.actions.items():
                if action_location == from_action_location:
                    self.actions[action] = to_action_location
        pass

    def get_actions(self, action_location_required: ActionLocation) -> Set[Action]:
        """
        Get all actions in some location.
        :param action_location_required: Action location required.
        :return: Get all actions in some location.
        """
        return set([action for action, action_location in self.actions.items()
                    if action_location == action_location_required])

    def all_payable_equiv_building_resources(self, all_equiv_building_resources: List[T_Resources_int]) \
            -> List[T_Resources_int]:
        """
        All payable resources for all equivalent resources to pay for a building.
        :param all_equiv_building_resources: All equivalent (by applying “5 to 1“-swap resources) resources
                                             to pay for a building.
        :return: All payable resources for all equivalent resources to pay for a building.
                 Remark: List[T_Resources_int] instead of Set[T_Resources_int] becomes to avoid error
                 "TypeError: unhashable type: 'dict'".
        """
        all_payable_equiv_building_resources: List[T_Resources_int] = \
            []  # All payable resources for all equivalent resources to pay for a building.
        for one_equiv_building_resources in all_equiv_building_resources:
            if self.is_payable(one_equiv_building_resources):
                all_payable_equiv_building_resources.append(one_equiv_building_resources)
        return all_payable_equiv_building_resources

    def is_payable(self, one_equiv_building_resources: T_Resources_int) -> bool:
        """
        Is the player can pay the building e.g.
        is each cost of the building resources smaller than or equals to the number of the same resource of the player?
        Remark: However, we do not test if the architect is required or not and
                if the player played action Architect during this turn.
        Remark: All numbers of self.resources[] are >= 0 and all numbers of one_equiv_building_resources are <= 0.
        :param one_equiv_building_resources: Building resources (cost)
                                             or some equivalent (by applying “5 to 1“-swap resources).
        :return: Is the player can pay the building?
        """
        for resource, number in self.resources.items():
            if number < -one_equiv_building_resources[resource]:
                return False
        return True

    def is_protected(self) -> bool:
        """
        Is the player protected during the turn?
        :return: Is the player protected during the turn?
        """
        return list(filter(lambda action: action.is_protected(), self.get_actions(ActionLocation.REVEALED))) != []

    def has_architect(self) -> bool:
        """
        Is the player played action Architect during the turn?
        :return: Is the player played action Architect during the turn?
        """
        return list(filter(lambda action: action.is_architect(), self.get_actions(ActionLocation.REVEALED))) != []


class HumanPlayer(Player):
    """
    Human players.
    """

    # Remark: We consider the human player do not modify values himself;
    #         in particular, he does not change parameters when he have to choose between some possibilities.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an human player."
        :param player_color: Human player color.
        :param resources: Number of all resources the human player has.
        :param buildings: Buildings the human player has purchased.
        :param actions: Deck of actions the human player has.
        """
        Player.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of an human player.
        :return: Name of an human player.
        """
        return '' + self.player_color.name + '(you!)'

    # Remark: All (following) methods choose_... of HumanPlayer can be programmed
    #         during the disintegration Tutored Project of the alternance training LP DAGPI 2019-2020.
    #         It requieres answers and responses with the console and/or the graphical user interface.

    def choose_actions_preparation(self, n_actions: int) -> None:
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.HAND][:n_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_new_actions(self, n_new_actions: int) -> None:
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.REVEALED][:n_new_actions]:
            self.actions[action] = ActionLocation.DISCARDED
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.HAND][:n_new_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        return first(list(remaining_revealed_actions))
        pass

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        return None
        pass

    def choose_action_to_refresh(self) -> Union[None, Action]:
        return None
        pass

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        return None
        pass

    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        return first(list(resources.keys()))
        pass

    def choose_thieved_player(self, thievable_players, the_game=None):
        return first(thievable_players)
        pass

    def choose_materials_to_thieve_player(self, other_players_with_resources):
        (thieved_player, n_materials_to_thieve, material_resources) = first(other_players_with_resources)
        if n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        elif n_materials_to_thieve == 1:
            return thieved_player, (first(list(material_resources.keys())), None)
        else:
            first_material_resource: MaterialResource = first(list(material_resources.keys()))
            if material_resources[first_material_resource] >= 2:
                return thieved_player, (first_material_resource, first_material_resource)
            else:
                second_material_resource: MaterialResource = first([material_resource
                                                                          for material_resource in material_resources.keys()
                                                                          if material_resource != first_material_resource])
                return thieved_player, (first_material_resource, second_material_resource)
        pass

    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        return dict(collections.Counter([material_resource for material_resource, number in material_resources.items()
                                         for _ in range(number)][:n_materials]))
        pass


class AIPlayer(Player):
    """
    AI (artificial intelligence) players.
    -----
    In order to compare the 2 AIs, we run 1000 games for each possibility:
    AA, AB, BB, AAA, AAB, ABB, BBB, AAAA, AAAB, AABB, ABBB, BBBB
    where A is for Advanced and B is for Basic such that we have 2 to 4 players.
    The statistics are the following:
        2 players, 14.650 turns, 26.741 PVs for the winner, 20.993500 PVs for IAs Advanced, --------- PVs for IAs Basic
        2 players, 15.792 turns, 26.755 PVs for the winner, 23.486000 PVs for IAs Advanced, 18.227000 PVs for IAs Basic
        2 players, 17.168 turns, 26.686 PVs for the winner, --------- PVs for IAs Advanced, 21.001000 PVs for IAs Basic
        3 players, 13.178 turns, 21.632 PVs for the winner, 14.643667 PVs for IAs Advanced, --------- PVs for IAs Basic
        3 players, 13.714 turns, 21.733 PVs for the winner, 15.909000 PVs for IAs Advanced, 11.632000 PVs for IAs Basic
        3 players, 14.395 turns, 21.599 PVs for the winner, 17.592000 PVs for IAs Advanced, 12.836500 PVs for IAs Basic
        3 players, 15.359 turns, 21.645 PVs for the winner, --------- PVs for IAs Advanced, 14.700667 PVs for IAs Basic
        4 players, 11.532 turns, 16.446 PVs for the winner, 09.626500 PVs for IAs Advanced, --------- PVs for IAs Basic
        4 players, 11.858 turns, 16.629 PVs for the winner, 10.375667 PVs for IAs Advanced, 07.143000 PVs for IAs Basic
        4 players, 12.114 turns, 16.601 PVs for the winner, 10.970000 PVs for IAs Advanced, 07.642500 PVs for IAs Basic
        4 players, 12.793 turns, 16.601 PVs for the winner, 12.156000 PVs for IAs Advanced, 08.644333 PVs for IAs Basic
        4 players, 13.290 turns, 16.586 PVs for the winner, --------- PVs for IAs Advanced, 09.556250 PVs for IAs Basic
    """

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an AI player."
        :param player_color: AI player color.
        :param resources: Number of all resources the AI player has.
        :param buildings: Buildings the AI player has purchased.
        :param actions: Deck of actions the AI player has.
        """
        Player.__init__(self, player_color, resources, buildings, actions)

    @staticmethod
    def upper_ai_names() -> List[str]:
        """
        Uppercase AI (artificial intelligence) names.
        :return: Uppercase AI (artificial intelligence) names.
        """
        return [BasicAIPlayer.ai_name.upper(), AdvancedAIPlayer.ai_name.upper()]

    def choose_actions_preparation(self, n_actions: int) -> None:
        """
        AI player chooses several of his action cards (in his hand)
        and places them face-down for the time being in front of himself.
        :param n_actions: Number of actions.
        """
        hand_to_face_down_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.HAND]  # Actions in the hand to be placed face-down.
        if len(hand_to_face_down_actions) != len(self.actions):
            raise ValueError(str(len(self.actions) - len(hand_to_face_down_actions))
                             + ' actions are not in the hand of player ' + self.name() + '.')
        if len(hand_to_face_down_actions) < n_actions:
            raise ValueError('There is not enough ' + str(n_actions) + ' actions to place face-down among '
                             + str(len(hand_to_face_down_actions)) + ' actions in the hand of player '
                             + self.name() + '.')
        random.shuffle(hand_to_face_down_actions)
        for action in hand_to_face_down_actions[:n_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_new_actions(self, n_new_actions: int) -> None:
        """
        AI player takes new action cards from his hand
        and places it face-down on top of some action cards laid-out in front of him.
        :param n_new_actions: # Number of new actions.
        Remark: We do not use random.choices(population=[...], k=n_new_actions) because
                it returns a k sized list of elements chosen from the population with replacement
                (cf. https://docs.python.org/3.7/library/random.html).
        """
        # First: set the actions to move.
        revealed_to_discarded_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.REVEALED]  # Revealed actions to be discarded.
        if len(revealed_to_discarded_actions) < n_new_actions:
            raise ValueError('There is not enough ' + str(n_new_actions) + ' new actions to discard among '
                             + str(len(revealed_to_discarded_actions)) + ' revealed actions of player '
                             + self.name() + '.')
        random.shuffle(revealed_to_discarded_actions)
        revealed_to_discarded_actions = revealed_to_discarded_actions[:n_new_actions]
        hand_to_face_down_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.HAND]  # Actions in the hand to be placed face-down.
        if len(hand_to_face_down_actions) < n_new_actions:
            raise ValueError('There is not enough ' + str(n_new_actions) + ' new actions to place face-down among '
                             + str(len(hand_to_face_down_actions)) + ' actions in the hand of player '
                             + self.name() + '.')
        random.shuffle(hand_to_face_down_actions)
        hand_to_face_down_actions = hand_to_face_down_actions[:n_new_actions]
        # Next: move the actions set.
        for action in revealed_to_discarded_actions:
            self.actions[action] = ActionLocation.DISCARDED
        for action in hand_to_face_down_actions:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        """
        Choose to carry out one of the remaining revealed actions.
        :param remaining_revealed_actions: Remaining revealed actions to carry out.
        :return: Action to carry out.
        """
        return random.choice(list(remaining_revealed_actions))

    @abc.abstractmethod
    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        pass

    @abc.abstractmethod
    def choose_resource_to_delete(self, player: Player, resources: T_Resources_int, the_game: 'Game' = None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        pass

    @abc.abstractmethod
    def choose_thieved_player(self, thievable_players: List[Player], the_game: 'Game' = None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        pass

    @abc.abstractmethod
    def choose_materials_to_thieve_player(self, other_players_with_resources: List[Tuple[Player, int, T_Resources_int]]):
        """
        Choose one player and its (1 or 2) materials to thieve when performing the action Materials Thief.
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players whe can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (1 or 2) materials to thieve.
        """
        pass

    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        """
        Choose some number of building material resources when performing the action Mama.
        :param n_materials: Number of building materials to choose.
        :param material_resources: Building material resources among those the player have to chose.
        :return: Building material resources chosen.
        """
        return dict(collections.Counter(n_random_elements_from_frequencies(n_materials, material_resources)))


class BasicAIPlayer(AIPlayer):
    """
    Basic AI (artificial intelligence) player.
    """

    ai_name: str = 'Basic'  # Basic AI name.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of a basic AI player."
        :param player_color: Basic AI player color.
        :param resources: Number of all resources the basic AI player has.
        :param buildings: Buildings the basic AI player has purchased.
        :param actions: Deck of actions the basic AI player has.
        """
        AIPlayer.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of a basic AI player.
        :return: Name of a basic AI player.
        """
        return '' + self.player_color.name + '=' + BasicAIPlayer.ai_name + ''

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the AI player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        purchasable_buildings: List[Building] = \
            [building for building in buildings_purchasable_with_resources.keys()]  # Purchasable buildings.
        i_purchasable_buildings: int = \
            random.randrange(1 + len(purchasable_buildings))  # Index of purchasable buildings.
        if i_purchasable_buildings == len(purchasable_buildings):
            return None
        else:
            building: Building = purchasable_buildings[i_purchasable_buildings]  # Building chosen to purchase.
            i_all_possible_resources: int = \
                random.randrange(
                    len(buildings_purchasable_with_resources[building]))  # Index of all possible resources.
            one_possible_resource: T_Resources_int = \
                buildings_purchasable_with_resources[building][
                    i_all_possible_resources]  # Possible resources chosen to purchase the building.
            return building, one_possible_resource

    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        Remark: Code inspection disagrees with
                return random.choice([None]+list(self.get_actions(ActionLocation.DISCARDED))).
        :return: Action to refresh.
        """
        discarded_actions_or_nothing: List[Union[None, Action]] = \
            list(self.get_actions(ActionLocation.DISCARDED)).copy()  # All discarded actions and None.
        discarded_actions_or_nothing.append(None)
        return random.choice(discarded_actions_or_nothing)

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        Remark: Code inspection disagrees with return random.choice([None]+buildings_extremities_rows).
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        buildings_extremities_rows_or_nothing: List[Union[None, Building]] = \
            buildings_extremities_rows.copy()  # All buildings at the extremities of all rows of the table and None.
        buildings_extremities_rows_or_nothing.append(None)
        return random.choice(buildings_extremities_rows_or_nothing)

    def choose_resource_to_delete(self, player: Player, resources: T_Resources_int, the_game:'Game' = None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        return random.choice([resource for resource, number in resources.items() for _ in range(number)])

    def choose_thieved_player(self, thievable_players:List[Player], the_game:'Game' = None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        return random.choice(thievable_players)

    def choose_materials_to_thieve_player(self, other_players_with_resources : List[Tuple[Player, int, Dict[MaterialResource, int]]]):
        """
        Choose one player and its (0 or 1 or 2) material(s) to thieve when performing the action Materials Thief.
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players who can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (0 or 1 or 2) material(s) to thieve.
        """
        # Get (player to thieve, number of materials to thieve, material resources of the player to thieve).
        (thieved_player, n_materials_to_thieve, material_resources) = random.choice(other_players_with_resources)
        if n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        else:  # 1 <= n_materials_to_thieve <= 2
            flatten_material_resources: List[MaterialResource] = \
                n_random_elements_from_frequencies(2,
                                                         material_resources)  # Flatten materials of the player to thieve.
            return thieved_player, (flatten_material_resources[0],
                                    None if n_materials_to_thieve == 1 else flatten_material_resources[1])
        pass


class AdvancedAIPlayer(AIPlayer):
    """
    Advanced AI (artificial intelligence) player.
    """

    ai_name: str = 'Advanced'  # Advanced AI name.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an advanced AI player."
        :param player_color: Advanced AI player color.
        :param resources: Number of all resources the advanced AI player has.
        :param buildings: Buildings the advanced AI player has purchased.
        :param actions: Deck of actions the advanced AI player has.
        """
        AIPlayer.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of an advanced AI player.
        :return: Name of an advanced AI player.
        """
        return '' + self.player_color.name + '=' + AdvancedAIPlayer.ai_name + ''

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose a building to purchase.
        We weight the choice according to the number of PVs of the buildings and we consume a minimum of resources.
        :param buildings_purchasable_with_resources: Buildings the advanced AI player can purchase
                                                     with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        purchasable_buildings: List[Building] = [building
                                                 for building in buildings_purchasable_with_resources.keys()
                                                 for _ in range(building.n_victory_pts)]  # Purchasable buildings.
        building: Building = \
            purchasable_buildings[random.randrange(len(purchasable_buildings))]  # Building chosen to purchase.
        all_possible_resources: List[T_Resources_int] = \
            buildings_purchasable_with_resources[building]  # All possible resources for the building chosen.
        min_cost_resources: int = \
            max([sum([number for resource, number in one_possible_resource.items()])  # (max of nonpositive numbers)
                 for one_possible_resource in
                 all_possible_resources])  # Minimum cost of resources among all possible resources.
        min_all_possible_resources: List[T_Resources_int] = \
            [one_possible_resource for one_possible_resource in all_possible_resources
             if sum([number for resource, number in one_possible_resource.items()]) ==
             min_cost_resources]  # All possible resources requiring minimum of resources.
        return building, min_all_possible_resources[random.randrange(len(min_all_possible_resources))]

    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        discarded_actions: List[Action] = list(self.get_actions(ActionLocation.DISCARDED))  # Discarded actions.
        return random.choice(discarded_actions) if discarded_actions else None

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        # Get buildings at the extremities of all rows of the table the player cannot purchase
        # (due to either the architect required but not played this turn or to the cost for the building).
        non_purchasable_buildings_extremities_rows: List[Building] = \
            [building for building in buildings_extremities_rows
             if (building.is_architect_required and not self.has_architect())
             or not building.is_payable(self.resources)]
        return None if not non_purchasable_buildings_extremities_rows else \
            random.choice(non_purchasable_buildings_extremities_rows)

    def choose_resource_to_delete(self, player: Player, resources: T_Resources_int, the_game:'Game'=None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        if worker_resource in resources.keys():
            # If possible, delete a worker resource.
            return worker_resource
        else:
            # If the player does not have worker resource to delete, the player have only material resources and at
            # least 2 different types of resources, perhaps debris. Delete a colored material resource (so, not debris).
            return random.choice([material_resource for material_resource, number in resources.items()
                                  if isinstance(material_resource, MaterialResource) and material_resource.is_colored
                                  for _ in range(number)])  # Remark: We use isinstance() only to avoid warnings.

    def choose_thieved_player(self, thievable_players: List[Player], the_game: 'Game'=None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        thievable_players_repetitions: List[Player] = \
            [thievable_player for thievable_player in thievable_players for _ in range(1 + thievable_player.resources[
                coin_resource])]  # Players who can be thieved with repetition according to the number of their coins.
        return random.choice(thievable_players_repetitions)

    def choose_materials_to_thieve_player(self, other_players_with_resources: List[Tuple[Player, int, Dict[MaterialResource, int]]]):
        """
        Choose one player and its (0 or 1 or 2) material(s) to thieve when performing the action Materials Thief.
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players who can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (0 or 1 or 2) material(s) to thieve.
        """
        # Get maximum number of materials to thieve.
        max_n_materials_to_thieve: int = \
            max([n_materials_to_thieve for (_, n_materials_to_thieve, _) in other_players_with_resources])
        # Get (player to thieve, number of materials to thieve, material resources of the player to thieve).
        (thieved_player, _, material_resources) = \
            random.choice([(thieved_player, n_materials_to_thieve, material_resources)
                           for (thieved_player, n_materials_to_thieve, material_resources)
                           in other_players_with_resources
                           if n_materials_to_thieve == max_n_materials_to_thieve])
        if max_n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        else:  # 1 <= max_n_materials_to_thieve <= 2
            flatten_material_resources: List[MaterialResource] = \
                n_random_elements_from_frequencies(2,
                                                         material_resources)  # Flatten materials of the player to thieve.
            return thieved_player, (flatten_material_resources[0],
                                    None if max_n_materials_to_thieve == 1 else flatten_material_resources[1])
        pass