from enum import Enum, auto
from tkinter import LabelFrame, ttk
from typing import Dict, Tuple, List

from ressources import T_Resources_int, Resource


class Building:
    """
    Buildings ("cards" or tiles).
    """

    def __init__(self, is_architect_required: bool, resources: T_Resources_int, n_victory_pts: int):
        """
        Initialization of a building.
        :param is_architect_required: Is the architect is required for the building?
        :param resources: Cost of each resource for this building.
        :param n_victory_pts: Number of victory points of the building.
        """
        self.is_architect_required: bool = is_architect_required  # Is the architect is required for the building?
        self.resources: T_Resources_int = resources  # Cost of each resource for this building.
        self.n_victory_pts: int = n_victory_pts  # Number of victory points of the building.
        self.is_removed = False  # Is the building removed?

    def __lt__(self, other_building):
        """
        Specify the order between all buildings. Is the building is lower than the other building?
        :param other_building: Another building.
        :return: Is the building is lower than the other building?
        """
        if self == other_building:
            return True
        elif self.n_victory_pts != other_building.n_victory_pts:
            return self.n_victory_pts < other_building.n_victory_pts
        elif self.is_architect_required != other_building.is_architect_required:
            return self.is_architect_required
        else:
            for resource, number in sorted(self.resources.items()):  # In this order!
                if self.resources[resource] != other_building.resources[resource]:
                    return self.resources[resource] < other_building.resources[resource]
        return True  # All numbers of self are equals to other_building.

    def __str__(self) -> str:
        """
        Text for a building.
        :return: Text for a building.
        """
        return ('supprimé, ' if self.is_removed else '') \
               + str(self.n_victory_pts) + ' PV, ' \
               + ('avec architecte, ' if self.is_architect_required else '') \
               + ', '.join([str(cost_number) + ' ' + resource.name
                            for resource, cost_number in sorted(self.resources.items()) if cost_number != 0])

    def is_payable(self, payment_resources: T_Resources_int) -> bool:
        """
        Is the building is payable e.g. the cost smaller than or equals to the payment for all resources?
        Remark: However, we do not test if the architect is required or not and
                if the player played action Architect during this turn.
        Remark: All numbers of self.resources[] are <= 0 and all numbers of payment_resources are >= 0.
        :param payment_resources: Payment resources for the building.
        :return: Is the building payable e.g. is the cost smaller than or equals to the payment for all resources?
        """
        if self.is_removed:
            raise ValueError('The building ' + str(self) + ' is removed and cannot be payable')
        for resource, cost_number in self.resources.items():
            if -cost_number > payment_resources[resource]:
                return False
        return True

    def all_equiv_building_resources(self, resources_swap: Dict[Tuple[Resource, Resource], int]) \
            -> List[T_Resources_int]:
        """
        All equivalent (by applying “5 to 1“-swap resources) resources to pay for the building.
        :param resources_swap: Resources swap: number of resources obtained for 1 resource given
                               where key is (resource given, resource obtained).
        :return: All equivalent (by applying “5 to 1“-swap resources) resources to pay for the building.
        """
        if self.is_removed:
            raise ValueError('The building ' + str(self)
                             + ' is removed and cannot be considered for computing all equivalent '
                             + '(by applying “5 to 1“-swap resources) resources to pay for it.')
        return Resource.all_equiv_resources(self.resources, resources_swap)

    def new_widget(self, frame_master: LabelFrame) -> LabelFrame:
        """
        Widget of the building.
        :param frame_master: Master frame.
        :return: Widget of the building.
        """
        # Frame for the building (number of victory points, architect required?, cost number of all the resources).
        widget_building: LabelFrame = ttk.LabelFrame(master=frame_master, text='Bâtiment')  # Widget for the building.
        n_info_building: int = \
            1 \
            + (1 if self.is_architect_required else 0) \
            + len(list(filter(lambda number: number < 0,
                              self.resources.values())))  # Number of pieces of information of the building.
        widget_building.grid(row=0, column=0, rowspan=n_info_building)
        ttk.Label(master=widget_building, text=str(self.n_victory_pts) + ' PV').grid(row=0, column=0)
        i_info_building: int = 0  # Index of the pieces of information for this building.
        if self.is_architect_required:
            i_info_building = i_info_building + 1
            ttk.Label(master=widget_building, text='Architecte').grid(row=i_info_building, column=0)
        for resource, number in sorted(self.resources.items()):
            if number < 0:
                i_info_building = i_info_building + 1
                ttk.Label(master=widget_building, text=str(-number) + ' ' + resource.name
                          ).grid(row=i_info_building, column=0)
        return widget_building


class BuildingLocation(Enum):
    """
    Enumeration of all the possible building locations.
    """
    TABLE = auto()  # The building is in the center of the table.
    PURCHASED = auto()  # The building was purchased by a player.
    PILE = auto()  # The building is in the face-down draw pile at the table edge.
    REMOVED = auto()  # The building is removed (e.g. returned to the box).

    def __str__(self) -> str:
        """
        Text for a building location.
        :return: Text for a building location.
        """
        return self.name