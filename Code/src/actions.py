import abc
import math
from enum import Enum, auto
from tkinter import LabelFrame, ttk
from typing import List, Union, Dict, Tuple

from building import Building

from ressources import MaterialResource, CoinResource, T_Resources_int, WorkerResource, Resource
from utils import n2str_txt_label_widget, printing, first


class ActionLocation(Enum):
    """
    Enumeration of all the possible action locations.
    """
    HAND = auto()  # Action card in the hand of the player.
    FACE_DOWN = auto()  # Action card placed face-down in front of the player.
    REVEALED = auto()  # Action card revealed and arranged with the lower value (numbered) card on the left.
    DISCARDED = auto()  # Action card in the discard pile.

    def __str__(self) -> str:
        """
        Text for an action location.
        :return: Text for an action location.
        """
        return self.name


class Action:
    """
    Actions (cards).
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool = None):
        """
        Initialization of an action.
        :param name: Name of the action.
        :param value: Value of the action.
        :param text: Text of the action.
        :param can_be_skipped: Is the action can be skipped? Remark: It is not used!
        """
        self.name: str = name  # Name of the action.
        self.value: int = value  # Value of the action.
        self.text: str = text  # Text of the action.
        self.can_be_skipped: bool = can_be_skipped  # Is the action can be skipped?

    def __lt__(self, other_action):
        """
        Specify the order between all actions. Is the action is lower than the other action?
        :param other_action: Another action.
        :return: Is the action is lower than the other action?
        """
        if self == other_action:
            return True
        elif self.value != other_action.value:
            return self.value < other_action.value
        else:
            return self.name <= other_action.name

    def __str__(self) -> str:
        """
        Text for an action.
        :return: Text for an action.
        """
        return str(self.value) + ' ' + self.name \
               + ' ' + ('(peut être effectuée)' if self.can_be_skipped else '(doit être effectuée)') + ';' \
               + ' ' + (self.text if self.text else '')

    def is_architect(self) -> bool:
        """
        Is it the architect action?
        :return: Is it the architect action?
        """
        return isinstance(self, ArchitectAction)

    def is_protected(self) -> bool:
        """
        Is the action protects the player during the turn?
        :return: Is the action protects the player during the turn?
        """
        return isinstance(self, ProtectionAction)

    def new_widget(self, frame_master: LabelFrame, hidden: bool) -> LabelFrame:
        """
        Widget of the action.
        :param frame_master: Master frame.
        :param hidden: Action hidden?
        :return: Widget of the action.
        """
        # Frame for the action (value and name).
        widget_action: LabelFrame = ttk.LabelFrame(master=frame_master, text='Action')  # Widget for the action.
        widget_action.grid(row=0, column=0, rowspan=2)
        if hidden:
            ttk.Label(master=widget_action, text='~').grid(row=0, column=0)
            ttk.Label(master=widget_action, text='~~~~~~~').grid(row=1, column=0)
        else:
            ttk.Label(master=widget_action, text=n2str_txt_label_widget(self.value)).grid(row=0, column=0)
            ttk.Label(master=widget_action, text=self.name).grid(row=1, column=0)
        return widget_action

    @abc.abstractmethod
    def play(self, player: 'Player', the_game: 'Game'=None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        Remark: We rename "game" by "the_game" in order to avoid (weak) warning
                "shadowing names defined in outer scopes" during the code inspection.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        pass

    @classmethod
    def thievable_players(cls, before_playing_order_players: 'List[Player]', after_playing_order_players: 'List[Player]'):
        """
        Players who can be thieved by the player whose turn it is.
        :rtype List[Player]
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        :return: Players who can be thieved by the player whose turn it is.
        """
        from players import Player
        thievable_players: List[Player] = list()  # Players who can be thieved by the player whose turn it is.
        thievable_playing_order_players: List[Player] = \
            after_playing_order_players if after_playing_order_players else \
                before_playing_order_players  # Correct playing order players.
        if thievable_playing_order_players:
            for thievable_player in thievable_playing_order_players:
                if thievable_player.is_protected():
                    printing(2, 'Le joueur ' + thievable_player.name()
                                   + ' est protégé du voleur de pièces ou de matériaux.')
                else:
                    thievable_players.append(thievable_player)
        else:
            pass
        return thievable_players  # Can be [] but not equals to None!


class SiestaAction(Action):
    """
    Action Siesta.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Siesta.
        :param name: Name of the action Siesta.
        :param value: Value of the action Siesta.
        :param text: Text of the action Siesta.
        :param can_be_skipped: Is the action Siesta can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game'=None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        This card causes no action, but it is very helpful in generating as low a number as possible,
        thus playing early on in the turn and making the most use of the second action card.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(2, 'Le joueur ' + player.name() + ' fait la sieste.')
        pass  # Nothing to do!


class RefreshmentAction(Action):
    """
    Action Refreshment.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Refreshment.
        :param name: Name of the action Refreshment.
        :param value: Value of the action Refreshment.
        :param text: Text of the action Refreshment.
        :param can_be_skipped: Is the action Refreshment can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The player may, but does not have to, carry out the action.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        if not player.get_actions(ActionLocation.DISCARDED):
            printing(2, 'Le joueur ' + player.name() + ' n\'a pas d\'action à récupérer.')
        else:
            action_to_refresh: Union[None, Action] = player.choose_action_to_refresh()  # Action to refresh.
            if action_to_refresh is None:
                printing(2, 'Le joueur ' + player.name() + ' n\'a pas souhaité récupérer d\'action.')
            else:
                if action_to_refresh not in player.get_actions(ActionLocation.DISCARDED):
                    raise ValueError('The player do not chose a discarded action.')
                player.actions[action_to_refresh] = ActionLocation.HAND
                player.create_widgets_frame_actions()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré l\'action ' + action_to_refresh.name
                               + ' dans sa main (auparavant dans sa défausse).')
        pass


class ProtectionAction(Action):
    """
    Action Protection.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Protection.
        :param name: Name of the action Protection.
        :param value: Value of the action Protection.
        :param text: Text of the action Protection.
        :param can_be_skipped: Is the action Protection can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If you want to save for valuable buildings by collecting a lot of building materials and/or Pesos,
        you may also want to protect yourself in time from thieves and tax collectors.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(2, 'Le joueur ' + player.name()
                       + ' se protège du percepteur et des voleurs de pièces et de matériaux.')
        # Nothing to do but the player is protected
        # from the others when they play "Tax Collector", "Pesos Thief" or "Materials Thief" during their turn.
        pass


class DebrisAction(Action):
    """
    Action Debris.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Debris.
        :param name: Name of the action Debris.
        :param value: Value of the action Debris.
        :param text: Text of the action Debris.
        :param can_be_skipped: Is the action Debris can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The action has to be carried out.
        Example: It is Vanessa’s turn to play.
        There are 4 debris in the center of the table (4 gray building materials).
        Vanessa must take all 4 gray building materials from the center of the table.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        for resource, number in sorted(the_game.table.resources.items()):
            if isinstance(resource, MaterialResource) and not resource.is_colored:
                resource.move(n_resources_to_move=number, from_table=the_game.table, to_player=player)
                the_game.table.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré (tous) les ' + str(number) + ' '
                               + resource.name + ' du centre de la table.')
        pass


class ConservationAction(Action):
    """
    Action Conservation.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Conservation.
        :param name: Name of the action Conservation.
        :param value: Value of the action Conservation.
        :param text: Text of the action Conservation.
        :param can_be_skipped: Is the action Conservation can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        Remark: Can set the game attribute indicating the action Conservation was already played during the turn.
        Remark: Can set the game attribute indicating the game is over.
        ~~~~~
        If the player thus removes 1 building card from the edge of a row of cards, return that card to the box.
        Very important: Only 1 building in total, i.e. by all players, may be removed in a round.
        Example: Sarah, Tobias and Vanessa each have a Conservation laid out in front of them.
        It is Sarah’s turn to play before Tobias and Vanessa.
        She uses her Conservation card and removes the leftmost building card in the upper row.
        Neither Tobias nor Vanessa can use their Conservation cards to remove a building in this round.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        if the_game.is_conservation_played:
            printing(2, 'Le joueur ' + player.name()
                           + ' ne peut pas effectuer cette action qui a déjà été jouée par un joueur précédent'
                           + ' qui a supprimé un bâtiment du centre de la table.')
        else:
            buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
                the_game.table.buildings_extremities_rows_and_coord()  # Buildings and their coordinates at extremities.
            building_to_remove: Building = \
                player.choose_building_to_remove(
                    list(buildings_extremities_rows_and_coord.keys()))  # Building the player wants to remove.
            if building_to_remove is None:
                printing(2, 'Le joueur ' + player.name() +
                         ' n\'a pas souhaité supprimer de bâtiment du centre de la table.')
            else:
                if building_to_remove not in buildings_extremities_rows_and_coord.keys():
                    raise ValueError(
                        'The player does not chose a building at the extremities of all rows of the table.')
                # Display the old situation.
                printing(2, 'Le bâtiment ' + str(building_to_remove) +
                         ' du centre de la table va être supprimé par le joueur ' + player.name() + '.')
                the_game.display_table(3)
                # The building at some extremity of some row of the table is removed.
                building_to_remove.is_removed = True
                the_game.table.create_widget_n_buildings_removed(the_game.game_element)
                the_game.delete_building(buildings_extremities_rows_and_coord[building_to_remove])
                # Display the new situation.
                printing(2, 'Le bâtiment ' + str(building_to_remove) +
                         ' du centre de la table vient d\'être supprimé par le joueur ' + player.name() + '.')
                the_game.display_table(3)
                # The action Conservation is played (a building was removed) and
                # the game can ends depending on buildings of table or in pile (not player).
                the_game.is_conservation_played = True
                the_game.check_is_game_over()
        pass


class TaxCollectorAction(Action):
    """
    Action Tax Collector.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Tax Collector.
        :param name: Name of the action Tax Collector.
        :param value: Value of the action Tax Collector.
        :param text: Text of the action Tax Collector.
        :param can_be_skipped: Is the action Tax Collector can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Take 1 Peso from the stock and remove 1 worker or 1 piece of building material from each following player.
        Example: It is Peter’s turn to play – he has the Tax Collector laid out.
        In this turn, Vanessa has already played, Tobias and Sarah have yet to play.
        Peter takes 1 Peso from the stock.
        Now he has to remove either 1 worker or 1 building material of his choice from each following player,
        i.e. Tobias and Sarah (not Vanessa!).
        Workers are returned to the stock, building materials are returned to the box.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # The player takes coin(s) from the stock.
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        if coin_resource.number == 0:
            printing(2, 'Le joueur ' + player.name() + ' ne peut pas récupérer de ' + coin_resource.name
                           + ' de la réserve (car il n\'y en a plus).')
        else:
            n_coins: int = 1  # Number of coin(s) to take.
            coin_resource.move(n_resources_to_move=n_coins, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_coins) + ' ' + coin_resource.name
                           + ' de la réserve.')
        # The player deletes 1 worker or 1 material resource from following players.
        for following_player in after_playing_order_players:
            if following_player.is_protected():
                printing(2, 'Le joueur ' + following_player.name() + ' est protégé (des taxes) du percepteur.')
            else:
                # Create worker and material resources available by the following player.
                following_player_resources: T_Resources_int = \
                    dict()  # Resources of the following player the tax collector can delete.
                worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
                if following_player.resources[worker_resource] >= 1:
                    following_player_resources[worker_resource] = following_player.resources[worker_resource]
                for material_resource in the_game.game_element.material_resources():
                    if following_player.resources[material_resource] >= 1:
                        following_player_resources[material_resource] = following_player.resources[material_resource]
                # The player can choose the worker or material resource to delete
                # depending on available resources the following player have.
                if following_player_resources:
                    # Resource of the following player the tax collector have to delete.
                    resource_to_delete_from_following_player: Resource = \
                        self.resource_to_delete_from_following_player(player, the_game, following_player,
                                                                      following_player_resources)
                    resource_to_delete_from_following_player.move(n_resources_to_move=1,
                                                                  from_player=following_player, to_stock=True)
                    following_player.create_widgets_frame_resources()
                    the_game.game_element.create_widgets_frame_resources()
                    printing(2, 'Le joueur ' + following_player.name() + ' a été taxé d\'1 '
                                   + resource_to_delete_from_following_player.name + ' par le percepteur'
                                   + (', au choix du joueur ' + player.name() if len(following_player_resources) >= 2 else '')
                                   + '.')
                else:
                    printing(2, 'Le joueur ' + following_player.name() +
                             ' n\'a aucune ressouce à être taxée par le percepteur.')
        pass

    @staticmethod
    def resource_to_delete_from_following_player(player: 'Player', the_game: 'Game', following_player: 'Game',
                                                 following_player_resources: T_Resources_int) -> Resource:
        """
        Resource of the following player the tax collector have to delete.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param following_player: Following player the tax collector can delete.
        :param following_player_resources: Resources of the following player the tax collector can delete.
        :return: Resource of the following player the tax collector have to delete.
        """
        if len(following_player_resources) == 1:
            return first(list(following_player_resources.keys()))
        else:
            # Resource of the following player the tax collector have to delete.
            resource_to_delete_from_following_player: Resource = \
                player.choose_resource_to_delete(following_player, following_player_resources, the_game)
            if resource_to_delete_from_following_player is None:
                raise ValueError('The player does not choose 1 resource from the following player '
                                 + following_player.name() + '.')
            if resource_to_delete_from_following_player not in following_player_resources.keys():
                raise ValueError('The player chooses 1 resource '
                                 + resource_to_delete_from_following_player.name
                                 + ' from the following player ' + following_player.name()
                                 + ' who cannot be deleted (because he does not have such resource).')
            return resource_to_delete_from_following_player


class WorkerAction(Action):
    """
    Action Worker.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Worker.
        :param name: Name of the action Worker.
        :param value: Value of the action Worker.
        :param text: Text of the action Worker.
        :param can_be_skipped: Is the action Worker can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Attention: In order to be allowed to take 2 workers, you do not have to be the player with the lowest
        in the round – you merely have to be the first player in the current round to carry out this action.
        If there are not any workers left in the stock,
        the player is out of luck and cannot carry out his action this round.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # 2 worker resources for the 1st player carrying out this action, 1 for the others.
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        n_workers: int = \
            2 if self not in [action for previous_player in before_playing_order_players
                              for action in previous_player.get_actions(ActionLocation.REVEALED)] else \
                1  # Number of workers (from the stock to the player).
        n_workers_effective: int = min(n_workers, worker_resource.number)  # Effective number of workers.
        worker_resource.move(n_resources_to_move=n_workers_effective, from_stock=True, to_player=player)
        the_game.game_element.create_widgets_frame_resources()
        player.create_widgets_frame_resources()
        printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_workers_effective)
                       + ' (sur ' + str(n_workers) + ' comme escompté) ' + worker_resource.name + ' de la réserve.')
        pass


class ArchitectAction(Action):
    """
    Action Architect.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Architect.
        :param name: Name of the action Architect.
        :param value: Value of the action Architect.
        :param text: Text of the action Architect.
        :param can_be_skipped: Is the action Architect can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The player must take a worker from the stock.
        The player is, however, not obligated to purchase a building with his architect even is possible.
        Purchasing buildings is strictly voluntary.
        If there are not any workers left in the stock,
        the player is out of luck and cannot carry out his action this round.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        if worker_resource.number >= 1:
            worker_resource.move(n_resources_to_move=1, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré 1 ' + worker_resource.name + ' de la réserve.')
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a pas récupéré de ' + worker_resource.name
                           + ' de la réserve.')
        pass


class PesosThiefAction(Action):
    """
    Action Pesos Thief.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Pesos Thief.
        :param name: Name of the action Pesos Thief.
        :param value: Value of the action Pesos Thief.
        :param text: Text of the action Pesos Thief.
        :param can_be_skipped: Is the action Pesos Thief can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If there are more than 1 players following, the Pesos Thief may choose any of those players to steal from.
        If there is no player following the Pesos Thief in this turn,
        the Pesos Thief may choose any player to steal from.
        Example: Vanessa has the Pesos Thief laid out. Sarah has already played, Peter and Tobias have yet to play.
        Vanessa has to steal from either Peter or Tobias – who she chooses is entirely up to her.
        Vanessa chooses Tobias, who has 7 Pesos in front of him.
        She takes 3 Pesos from him (3.5 = half of the Pesos; rounded down to 3).
        Attention: It is possible to have several players who played the Pesos Thief steal from one and the same player.
        It is also possible that all players have laid out the Pesos Thief – all Pesos Thieves are used consecutively
        (when it is their respective player’s turn to play).
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        from players import Player
        thievable_players: List[Player] = \
            Action.thievable_players(before_playing_order_players,
                                     after_playing_order_players)  # Players who can be thieved.
        n_thievable_players: int = len(thievable_players)  # Number of players who can be thieved.
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        if n_thievable_players == 0:
            printing(2, 'Le joueur ' + player.name() + ' n\'a volé aucun ' + coin_resource.name
                           + ' aux autres joueurs.')
        else:
            thieved_player: Player = \
                first(thievable_players) if n_thievable_players == 1 else \
                player.choose_thieved_player(thievable_players, the_game)  # Player thieved.
            if thieved_player not in thievable_players:
                raise ValueError('The player chooses player ' + thieved_player.name() + ' who cannot be thieved.')
            n_coins_thieved: int = math.floor(thieved_player.resources[coin_resource] / 2)  # Number of coins thieved.
            coin_resource.move(n_resources_to_move=n_coins_thieved, from_player=thieved_player, to_player=player)
            thieved_player.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a volé ' + str(n_coins_thieved) + ' ' + coin_resource.name
                           + ' au joueur ' + thieved_player.name()
                           + ' parmi le(s) ' + str(len(thievable_players)) + ' pouvant être volé(s).')
        pass


class MaterialsThiefAction(Action):
    """
    Action Materials Thief.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Materials Thief.
        :param name: Name of the action Materials Thief.
        :param value: Value of the action Materials Thief.
        :param text: Text of the action Materials Thief.
        :param can_be_skipped: Is the action Materials Thief can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        All rules for playing the Pesos Thief (see above) apply to the Materials Thief accordingly.
        Important: The Materials Thief usually steals only one building material.
        It is entirely up to him which building material he takes from the respective player
        (he may also choose a gray building material!).
        Should the victim own more than 3 colored building materials,
        the Materials Thief may steal 2 building materials of his choice from that player.
        Example: Tobias has laid out the Materials Thief and wants to steal from Sarah, who is following him.
        At the moment, Sarah has 4 colored building materials (1x yellow, 1x brown and 2x red),
        as well as 3x debris in front of her.
        Tobias may take 2 building materials of his choice from Sarah.
        He chooses 1 brown and 1 gray building material.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        from players import Player
        thievable_players: List[Player] = \
            Action.thievable_players(before_playing_order_players,
                                     after_playing_order_players)  # Players who can be thieved.
        # Determine other players and their material resources the player can thieve.
        other_players_with_resources: List[Tuple[Player, int, Dict[MaterialResource, int]]] = \
            list()  # Other players who can be thieved for materials.
        for thievable_player in thievable_players:
            thievable_material_resources: Dict[MaterialResource, int] = \
                dict()  # Material resources of a player who can be thieved.
            for material_resource, number in thievable_player.resources.items():
                if number > 0 and isinstance(material_resource, MaterialResource):
                    thievable_material_resources[material_resource] = number
            if thievable_material_resources:
                if sum([number for resource, number in thievable_material_resources.items()
                        if resource.is_colored]) > 3:
                    other_players_with_resources.append((thievable_player, 2, thievable_material_resources))
                else:
                    other_players_with_resources.append((thievable_player, 1, thievable_material_resources))
            else:
                other_players_with_resources.append(
                    (thievable_player, 0,
                     thievable_material_resources))  # The player who can be thieved does not have any resource.
        # The player chooses the player and its (0 or 1 or 2) material(s) to thieve.
        if other_players_with_resources:
            (thieved_player, (one_material_resource_to_thieve, another_material_resource_to_thieve)) = \
                player.choose_materials_to_thieve_player(other_players_with_resources)
            if thieved_player not in thievable_players:
                raise ValueError('The player chooses player ' + thieved_player.name() + ' who cannot be thieved.')
            (_, n_material_resources_to_thieve, thieved_player_material_resources) = \
                [other_player_with_resources for other_player_with_resources in other_players_with_resources
                 if other_player_with_resources[0] == thieved_player][
                    0]  # Played to thieve with his material resources.
            if not ((n_material_resources_to_thieve == 0
                     and one_material_resource_to_thieve is None
                     and another_material_resource_to_thieve is None) or
                    (n_material_resources_to_thieve == 1
                     and one_material_resource_to_thieve is not None
                     and another_material_resource_to_thieve is None) or
                    (n_material_resources_to_thieve == 2
                     and one_material_resource_to_thieve is not None
                     and another_material_resource_to_thieve is not None)):
                raise ValueError('The player do not chose a correct number ' + str(n_material_resources_to_thieve)
                                 + ' of material resources to thieve to player ' + thieved_player.name() + '.')
            if n_material_resources_to_thieve == 0:
                printing(2, 'Le joueur ' + player.name() + ' ne vole aucun matériau au joueur '
                               + thieved_player.name() + ' (qui n\'en a pas) parmi le(s) '
                               + str(len(thievable_players)) + ' pouvant être volé(s).')
            else:
                if one_material_resource_to_thieve not in thieved_player_material_resources.keys():
                    raise ValueError('The player chooses a first material resource '
                                     + one_material_resource_to_thieve.name + ' to thieve to player '
                                     + thieved_player.name() + ' who does not have this resource.')
                if n_material_resources_to_thieve == 2 \
                        and another_material_resource_to_thieve not in thieved_player_material_resources.keys():
                    raise ValueError('The player chooses a second material resource '
                                     + another_material_resource_to_thieve.name + ' to thieve to player '
                                     + thieved_player.name() + ' who does not have this resource.')
                if n_material_resources_to_thieve == 2 \
                        and one_material_resource_to_thieve == another_material_resource_to_thieve \
                        and thieved_player_material_resources[one_material_resource_to_thieve] < 2:
                    raise ValueError('The player chooses too many material resource '
                                     + one_material_resource_to_thieve.name
                                     + ' to thieve to player ' + thieved_player.name() + '.')
                # The player thieves a first material resource.
                one_material_resource_to_thieve.move(n_resources_to_move=1,
                                                     from_player=thieved_player, to_player=player)
                printing(2, 'Le joueur ' + player.name() + ' a volé 1 ' + one_material_resource_to_thieve.name
                               + ' au joueur ' + thieved_player.name()
                               + ' parmi le(s) ' + str(len(thievable_players)) + ' pouvant être volé(s).')
                # The player thieves (or not) a second material resource.
                if n_material_resources_to_thieve == 2:
                    another_material_resource_to_thieve.move(n_resources_to_move=1,
                                                             from_player=thieved_player, to_player=player)
                    printing(2, 'Le joueur ' + player.name()
                                   + ' a aussi volé 1 ' + another_material_resource_to_thieve.name
                                   + ' au même joueur ' + thieved_player.name() + '.')
                else:
                    printing(2, 'Le joueur ' + player.name()
                                   + ' n\'a pas volé d\'autre matériau au même joueur ' + thieved_player.name() + '.')
                # Update GUI: the player thieves 1 or 2 material resource(s).
                thieved_player.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a volé aucun matériau aux autres joueurs.')
        pass


class BlackMarketAction(Action):
    """
    Action Black Market.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Black Market.
        :param name: Name of the action Black Market.
        :param value: Value of the action Black Market.
        :param text: Text of the action Black Market.
        :param can_be_skipped: Is the action Black Market can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Attention: In order to be allowed to take 2 building materials from the bag,
        you do not have to be the player with the lowest number in the round –
        you merely have to be the first player in the current round to carry out this action.
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # 2 material resources for the 1st player carrying out this action, 1 for the others.
        draw_materials: List[MaterialResource] = the_game.game_element.draw_materials(
            2 if self not in [action for previous_player in before_playing_order_players
                              for action in previous_player.get_actions(ActionLocation.REVEALED)] else
            1)  # Draw material resources.
        for material_resource in draw_materials:
            material_resource.move(n_resources_to_move=1, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré 1 ' + material_resource.name + ' de la réserve.')
        pass


class PesosAction(Action):
    """
    Action Pesos.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Pesos.
        :param name: Name of the action Pesos.
        :param value: Value of the action Pesos.
        :param text: Text of the action Pesos.
        :param can_be_skipped: Is the action Pesos can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If several players have the Pesos card laid out, they are all used consecutively
        (when it is their respective player’s turn to play).
        Example: Vanessa and Sarah both have the Pesos card laid out in front of them,
        there are 10 Pesos in the center of the table.
        Vanessa precedes Sarah and takes 5 Pesos.
        Out of the remaining 5 Pesos, Sarah takes 3 Pesos (2.5 = half of the Pesos; rounded up to 3).
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        n_coins: int = math.ceil(the_game.table.resources[coin_resource] / 2)  # Number of coins.
        coin_resource.move(n_resources_to_move=n_coins, from_table=the_game.table, to_player=player)
        the_game.table.create_widgets_frame_resources()
        player.create_widgets_frame_resources()
        printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_coins) + ' ' + coin_resource.name
                       + ' du centre de la table.')
        pass


class MamaAction(Action):
    """
    Action Mama.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Mama.
        :param name: Name of the action Mama.
        :param value: Value of the action Mama.
        :param text: Text of the action Mama.
        :param can_be_skipped: Is the action Mama can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player: 'Player', the_game: 'Game' =None, before_playing_order_players: 'List[Player]'=None, after_playing_order_players: 'List[Player]'=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If several players have the Mama card laid out, they are all used consecutively
        (when it is their respective player’s turn to play).
        It is entirely up to the player which building materials (half of the colored and half of the gray ones)
        he takes from the center of the table.
        Example: Peter has the Mama laid out.
        There are 6 colored building materials (1x brown, 2x yellow, and 3x red) and 5 debris
        in the center of the table.
        Peter takes 3 colored building materials (he chooses 2x yellow and 1x brown)
        as well as 3 gray building materials (half of 5 = 2.5; rounded up to 3).
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # The player chooses half of the colored building materials of the table.
        colored_materials: Dict[MaterialResource, int] = dict()  # Colored material resources of the table.
        for resource, number in the_game.table.resources.items():
            if number > 0 and isinstance(resource, MaterialResource) and resource.is_colored:
                colored_materials[resource] = number
        if colored_materials:
            n_colored_materials_to_take: int = math.ceil(
                sum(colored_materials.values()) / 2)  # Number of colored building materials the player can take.
            if len(colored_materials) == 1:
                first(list(colored_materials.keys())).move(n_resources_to_move=n_colored_materials_to_take,
                                                                 from_table=the_game.table, to_player=player)
            else:
                colored_materials_chosen: Dict[MaterialResource, int] = player.choose_material_resources(
                    n_colored_materials_to_take,
                    colored_materials)  # Colored building materials in the table chosen by the player.
                if sum(colored_materials_chosen.values()) != n_colored_materials_to_take:
                    raise ValueError('The player do not chose the correct total number of building material resources '
                                     + str(n_colored_materials_to_take) + '.')
                if list(filter(lambda number: number < 0, colored_materials_chosen.values())):
                    raise ValueError('The player chooses a negative number for some building material resource.')
                if not set([colored_material_chosen for colored_material_chosen, number
                            in colored_materials_chosen.items() if number > 0]) <= set(colored_materials.keys()):
                    raise ValueError('The player chooses some non available building material resources.')
                for resource, number in colored_materials_chosen.items():
                    if number > colored_materials[resource]:
                        raise ValueError('The player chooses too many ' + str(number) + ' among available '
                                         + str(colored_materials[resource]) + ' building material resource '
                                         + resource.name + '.')
                    resource.move(n_resources_to_move=number, from_table=the_game.table, to_player=player)
            # The player takes colored building materials from the table.
            the_game.table.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_colored_materials_to_take)
                           + ' matériau(x) du centre de la table.')
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a récupéré aucun matériau du centre de la table.')
        # The player takes half of the gray (not colored) building materials (debris) of the table.
        for resource, number in sorted(the_game.table.resources.items()):
            if isinstance(resource, MaterialResource) and not resource.is_colored:
                n_resources: int = math.ceil(number / 2)  # Number of resources.
                resource.move(n_resources_to_move=n_resources, from_table=the_game.table, to_player=player)
                the_game.table.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_resources) + ' ' + resource.name
                               + ' du centre de la table.')
        pass