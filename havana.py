#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on March 21 14:15:16 2019. Ended without Graphical User Interface on April 5 10:09:08 2019.
Board game Havana.
@author: Olivier
"""

# TODO? abc for classes Player, AIPlayer, Resource, Building, Action.
# TODO? replace :type Class of parameter p by p: Class; see Resource.move()

# Remark: We use isinstance(resource, <type>Resource) instead of resource.type() == ResourceType.<type>
#         in order to avoid warning check when we inspect the code.
#         So we also use isinstance in some other cases.

# Design error: the stock must be rely to the game instead of the game element.

import abc
import collections
import datetime
from enum import Enum, auto
import math
from functools import total_ordering
from os import path
import random
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from typing import Dict, List, Set, Tuple, Union, TypeVar
import xml.etree.ElementTree as ElemTree

VERBOSE_CONSOLE: bool = True  # Verbose mode on the console e.g. print messages.
VERBOSE_GUI: bool = False  # Verbose mode for the graphical user interface e.g. print messages to stop the program.
TXT_INDENT: str = '  '  # Text for one indentation.
LINE_SEPARATOR: str = '~~~~~'  # Line separator.

COLOR_REMOVED = 'magenta'  # Color of a removed element for the GUI.


def printing(n_indent: int, message: str) -> None:
    """
    In verbose mode on the console, print the message.
    :param n_indent: Number of indentations.
    :param message: Message.
    """
    if VERBOSE_CONSOLE:
        print((TXT_INDENT * n_indent) + message)


def messagebox_showinfo(title: str, message: str) -> None:
    """
    In verbose mode for the graphical user interface, show a message box.
    :param title: Title.
    :param message: Message.
    """
    if VERBOSE_GUI:
        messagebox.showinfo(title, message)


def usage(error_msg: str) -> None:
    """
    Usage of the command.
    :param error_msg: Error message.
    """
    print(error_msg)
    print('Usage : python ' + sys.argv[0] + ' <fichier_XML> <couleur>[=<nom_AI>] ... <couleur>[=<nom_AI>]')
    print('Exemple de 4 joueurs (humain (en 2de pos., green) face à 2 IA basic et 1 IA advanced) :'
          + '\n\tpython ' + sys.argv[0] + ' game_elements-Havana.xml red=Basic green yellow=Advanced blue=Basic')
    exit(1)


T = TypeVar('T')  # Declare some generic type variable or class.


def n_random_elements_from_sequence(n_elements: int, sequence: List[T]) -> List[T]:
    """
    Random some elements from a sequence.
    :param n_elements: Number of elements wanted from the sequence.
    :param sequence: Sequence of elements.
    :return: Some random elements from a sequence.
    """
    local_sequence: List[T] = sequence.copy()  # local copy of the sequence.
    random.shuffle(local_sequence)
    return local_sequence[:n_elements]  # If len(local_sequence) <= n_elements, returns local_sequence.


def n_random_elements_from_frequencies(n_elements: int, frequencies: Dict[T, int]) -> List[T]:
    """
    Random some elements according to the frequencies.
    :param n_elements: Number of elements wanted from the frequencies.
    :param frequencies: Frequencies e.g. some nonnegative number for each T.
    :return: Some random elements according to the frequencies.
    """
    return n_random_elements_from_sequence(n_elements, [e for e, n in frequencies.items() for _ in range(n)])


def first(sequence: List[T]) -> T:
    """
    First element of a sequence.
    :param sequence: Sequence of elements.
    :return: First element of a sequence.
    """
    return sequence[0]


def n2str_txt_label_widget(n: int) -> str:
    """
    String for a number into the text of a label of a widget of the graphical user interface.
    :param n: Number.
    :return: String for a number into the text of a label of a widget of the graphical user interface.
    """
    spaces_before_after: str = ' ' * 8  # Spaces before and after le string for the number.
    return spaces_before_after + str(n) + spaces_before_after


class PlayerColor:
    """
    Player color: red, green, blue and yellow.
    """

    def __init__(self, name: str):
        """
        Initialization of a player color.
        :param name: Name of a player color.
        """
        self.name: str = name  # Name of a player color.

    def __lt__(self, other_player_color):
        """
        Specify the order between all color players. Is the color player is lower than the other color player?
        :param other_player_color: Another color player.
        :return: Is the color player is lower than the other color player?
        """
        if self == other_player_color:
            return True
        else:
            return self.name <= other_player_color.name

    def __str__(self) -> str:
        """
        Text for a player color.
        :return: Text for a player color.
        """
        return self.name


@total_ordering
class Resource:
    """
    Resources: worker, coin and material resources.
    """

    def __init__(self, name: str, number: int):
        """
        Initialization of a resource.
        :param name: Name of the resource.
        :param number: Number of the resource.
        """
        self.name: str = name  # Name of the resource.
        self.number: int = number  # Number of the resource.
        self.number_removed: int = 0  # Number of the resource removed from the game (returned to the box)

    def __lt__(self, other_resource):
        """
        Specify the order between all resources. Is the resource is lower than the other resource?
        :param other_resource: Another resource.
        :return: Is the resource is lower than the other resource?
        """
        if isinstance(self, WorkerResource):
            #  Either equals if isinstance(other_resource, WorkerResource)
            #  or lower if isinstance(other_resource, (CoinResource, MaterialResource)).
            return True
        if isinstance(self, CoinResource):
            #  Either equals if isinstance(other_resource, CoinResource)
            #  or lower if isinstance(other_resource, MaterialResource).
            return not isinstance(other_resource, WorkerResource)
        if not isinstance(other_resource, MaterialResource):
            return False
        else:
            if not isinstance(self, MaterialResource) or not isinstance(other_resource, MaterialResource):
                raise ValueError('Impossible to compare two (material) resources.')
            if self.is_colored and not other_resource.is_colored:
                return True
            elif not self.is_colored and other_resource.is_colored:
                return False
            else:
                return self.name <= other_resource.name

    @abc.abstractmethod
    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        pass

    @classmethod
    def text_is_cost_after_purchasing_removed(cls, is_cost_after_purchasing_removed: bool) -> str:
        """
        Text corresponding to the resource once consumed:
        either removed from the game (returned to the box) or returned to the table edge.
        :param is_cost_after_purchasing_removed: Once the resource is consumed, is the cost removed?
        :return: Text corresponding to the resource once consumed:
                 either removed from the game (returned to the box) or returned to the table edge.
        """
        return '(supprimé du jeu)' if is_cost_after_purchasing_removed else '(remis en jeu)'

    def move(self, n_resources_to_move: int,
             from_stock: bool = None, from_table=None, from_player=None,
             to_stock: bool = None, to_table=None, to_player=None) -> None:
        """
        Move some number of this resource from
        either the stock or the table or a player to either the stock or the table or a player.
        :type from_table: Table
        :type from_player: Player
        :type to_table: Table
        :type to_player: Player
        :param n_resources_to_move: Number of this resource to move.
        :param from_stock: Move some number of this resource from the stock.
        :param from_table: Move some number of this resource from the table.
        :param from_player: Move some number of this resource from a player.
        :param to_stock: Move some number of this resource to the stock.
        :param to_table: Move some number of this resource to the table.
        :param to_player: Move some number of this resource to a player.
        """
        if n_resources_to_move < 0:
            raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                             + ' must be nonnegative.')
        elif (n_resources_to_move == 0) \
                or (from_stock is not None and to_stock is not None) \
                or (from_table is not None and to_table is not None) \
                or (from_player is not None and to_player is not None and from_player == to_player):
            pass  # Nothing to do: additive identity!
        else:
            if from_stock is not None and from_table is None and from_player is None:
                if self.number < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(self.number)
                                     + ' from the stock.')
                else:
                    self.number = self.number - n_resources_to_move
            elif from_stock is None and from_table is not None and from_player is None:
                if from_table.resources[self] < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(from_table.resources[self])
                                     + ' from the table.')
                else:
                    from_table.resources[self] = from_table.resources[self] - n_resources_to_move
            elif from_stock is None and from_table is None and from_player is not None:
                if from_player.resources[self] < n_resources_to_move:
                    raise ValueError('The number of ' + self.name + ' to move ' + str(n_resources_to_move)
                                     + ' must be less than or equals to the number ' + str(from_player.resources[self])
                                     + ' from the player ' + from_player.name() + '.')
                else:
                    from_player.resources[self] = from_player.resources[self] - n_resources_to_move
            else:
                raise ValueError('Only one among the stock, the table, a player is possible to move from.')
            if to_stock is not None and to_table is None and to_player is None:
                if self.cost_after_purchasing_to_remove():
                    self.number_removed = self.number_removed + n_resources_to_move
                else:
                    self.number = self.number + n_resources_to_move
            elif to_stock is None and to_table is not None and to_player is None:
                to_table.resources[self] = to_table.resources[self] + n_resources_to_move
            elif to_stock is None and to_table is None and to_player is not None:
                to_player.resources[self] = to_player.resources[self] + n_resources_to_move
            else:
                raise ValueError('Only one among the stock, the table, a player is possible to move to.')
        pass

    @classmethod
    def all_equiv_resources(cls, resources, resources_swap):
        """
        All equivalent (by applying “5 to 1“-swap resources) resources to some base resources.
        For instance, "5 Pesos for 1 worker" and "5 debris for 1 colored cube" with -10 pesos and -5 debris,
        we generate:
            (-10 pesos, -5 debris),    (-1 worker, -5 pesos, -5 debris),    (-2 workers, -5 debris),
            (-10 pesos, -1 brick),     (-1 worker, -5 pesos, -1 brick),     (-2 workers, -1 brick),
            (-10 pesos, -1 sandstone), (-1 worker, -5 pesos, -1 sandstone), (-2 workers, -1 sandstone),
            (-10 pesos, -1 loam),      (-1 worker, -5 pesos, -1 loam),      (-2 workers, -1 loam),
            (-10 pesos, -1 glass),     (-1 worker, -5 pesos, -1 glass),     (-2 workers, -1 glass).
        Remark: We suppose there is no cycle in all “5 to 1“-swap resources.
        Remark: All numbers (of resources) are <= 0.
        :type resources: Dict[Resource, int]
        :type resources_swap: Dict[Tuple[Resource, Resource], int]
        :rtype List[Dict[Resource, int]]
        :param resources: Base resources for which we generate all equivalent resources.
        :param resources_swap: Resources swap: number of resources obtained for 1 resource given
                               where key is (resource given, resource obtained).
        :return: All equivalent (by applying “5 to 1“-swap resources) resources to some base resources.
                 Remark: List[T_Resources_int] instead of Set[T_Resources_int] becomes to avoid error
                 "TypeError: unhashable type: 'dict'".
        """
        all_equiv_building_resources: List[T_Resources_int] = \
            [resources.copy()]  # All equivalent (by applying “5 to 1“-swap) resources to some base resources.
        for (resource_given, resource_obtained), number_for_1 in resources_swap.items():
            new_all_equiv_building_resources: List[T_Resources_int] = \
                list()  # New equivalent resources to some resources.
            for one_equiv_building_resources in all_equiv_building_resources:
                for multiplicity in range(0, 1 - one_equiv_building_resources[resource_obtained]):
                    new_one_equiv_building_resources = \
                        one_equiv_building_resources.copy()  # New one equivalent resources.
                    new_one_equiv_building_resources[resource_given] = \
                        new_one_equiv_building_resources[resource_given] - number_for_1 * multiplicity
                    new_one_equiv_building_resources[resource_obtained] = \
                        new_one_equiv_building_resources[resource_obtained] + 1 * multiplicity
                    new_all_equiv_building_resources.append(new_one_equiv_building_resources)
            all_equiv_building_resources = new_all_equiv_building_resources
        return all_equiv_building_resources

    @classmethod
    def create_widgets_frame_resources(cls, frame_resources: LabelFrame, resources, with_removed: bool):
        """
        [Re]Create all the widgets of the frame for the resources of the graphical user interface.
        :type resources:Dict[Resource,int]
        :param frame_resources: Frame of all resources.
        :param resources: Resources.
        :param with_removed: Removed resources have to be shown?
        """
        # Destroy all previous widgets.
        for widget_resource in frame_resources.winfo_children():
            widget_resource.destroy()
        # Create new widgets.
        # Frame for all the resources. For each resource: color of materials, name, number, and removed number.
        for i_resource, resource in enumerate(sorted(resources)):
            if isinstance(resource, MaterialResource):
                ttk.Label(master=frame_resources, text=' ' * 5, background=resource.color
                          ).grid(row=0, column=i_resource)
            ttk.Label(master=frame_resources, text=resource.name).grid(row=1, column=i_resource)
            ttk.Label(master=frame_resources, text=n2str_txt_label_widget(resources[resource])
                      ).grid(row=2, column=i_resource)
            if with_removed and resource.cost_after_purchasing_to_remove():
                ttk.Label(master=frame_resources, text=n2str_txt_label_widget(resource.number_removed),
                          foreground=COLOR_REMOVED).grid(row=3, column=i_resource)
        pass


class WorkerResource(Resource):
    """
    Worker (meeples) resources.
    """

    is_cost_after_purchasing_removed: bool = None  # Once the worker resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool):
        """
        Initialization of the worker resource.
        :param name: Name of the worker resource.
        :param number: Number of worker resources.
        :param is_cost_after_purchasing_removed: Once the worker resource is consumed, is the cost removed?
        """
        Resource.__init__(self, name, number)
        WorkerResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed

    def __str__(self) -> str:
        """
        Text for the worker resource.
        :return: Text for the worker resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(WorkerResource.is_cost_after_purchasing_removed)

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return WorkerResource.is_cost_after_purchasing_removed


class CoinResource(Resource):
    """
    Coin (Money) resources: peso(s).
    """

    is_cost_after_purchasing_removed: bool = None  # Once the coin resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool):
        """
        Initialization of the coin resource.
        :param name: Name of the coin resource.
        :param number: Number of coin resources.
        :param is_cost_after_purchasing_removed: Once the coin resource is consumed, is the cost removed?
        """
        Resource.__init__(self, name, number)
        CoinResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed

    def __str__(self) -> str:
        """
        Text for the coin resource.
        :return: Text for the coin resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(CoinResource.is_cost_after_purchasing_removed)

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return CoinResource.is_cost_after_purchasing_removed


class MaterialResource(Resource):
    """
    Material resources (cubes): brick, sandstone, loam, glass or debris.
    """

    is_cost_after_purchasing_removed: bool = None  # Once a material resource is consumed, is the cost removed?

    def __init__(self, name: str, number: int, is_cost_after_purchasing_removed: bool, is_colored: bool, color: str):
        """
        Initialization of a material resource.
        :param name: Name of the material resource.
        :param number: Number of material resources.
        :param is_cost_after_purchasing_removed: Once the material resource is consumed, is the cost removed?
        :param is_colored: Is the material resource colored? (true iif the material resource is not debris)
        :param color: Material resource color.
        """
        Resource.__init__(self, name, number)
        MaterialResource.is_cost_after_purchasing_removed = is_cost_after_purchasing_removed
        self.is_colored: bool = is_colored  # Is the material resource colored? (true iif the material is not debris)
        self.color: str = color  # Material resource color.

    def __str__(self) -> str:
        """
        Text for a material resource.
        :return: Text for a material resource.
        """
        return str(self.number) + ' ' + self.name + ' ' \
               + Resource.text_is_cost_after_purchasing_removed(MaterialResource.is_cost_after_purchasing_removed) \
               + ', ' + ('colored' if self.is_colored else 'not colored') \
               + ', ' + self.color

    def cost_after_purchasing_to_remove(self) -> bool:
        """
        Once the resource is consumed, is the cost removed?
        :return: Once the resource is consumed, is the cost removed?
        """
        return MaterialResource.is_cost_after_purchasing_removed


T_Resources_int = Dict[Resource, int]  # Declare a type for some number of resources.


class BuildingLocation(Enum):
    """
    Enumeration of all the possible building locations.
    """
    TABLE = auto()  # The building is in the center of the table.
    PURCHASED = auto()  # The building was purchased by a player.
    PILE = auto()  # The building is in the face-down draw pile at the table edge.
    REMOVED = auto()  # The building is removed (e.g. returned to the box).

    def __str__(self) -> str:
        """
        Text for a building location.
        :return: Text for a building location.
        """
        return self.name


class Building:
    """
    Buildings ("cards" or tiles).
    """

    def __init__(self, is_architect_required: bool, resources: T_Resources_int, n_victory_pts: int):
        """
        Initialization of a building.
        :param is_architect_required: Is the architect is required for the building?
        :param resources: Cost of each resource for this building.
        :param n_victory_pts: Number of victory points of the building.
        """
        self.is_architect_required: bool = is_architect_required  # Is the architect is required for the building?
        self.resources: T_Resources_int = resources  # Cost of each resource for this building.
        self.n_victory_pts: int = n_victory_pts  # Number of victory points of the building.
        self.is_removed = False  # Is the building removed?

    def __lt__(self, other_building):
        """
        Specify the order between all buildings. Is the building is lower than the other building?
        :param other_building: Another building.
        :return: Is the building is lower than the other building?
        """
        if self == other_building:
            return True
        elif self.n_victory_pts != other_building.n_victory_pts:
            return self.n_victory_pts < other_building.n_victory_pts
        elif self.is_architect_required != other_building.is_architect_required:
            return self.is_architect_required
        else:
            for resource, number in sorted(self.resources.items()):  # In this order!
                if self.resources[resource] != other_building.resources[resource]:
                    return self.resources[resource] < other_building.resources[resource]
        return True  # All numbers of self are equals to other_building.

    def __str__(self) -> str:
        """
        Text for a building.
        :return: Text for a building.
        """
        return ('supprimé, ' if self.is_removed else '') \
               + str(self.n_victory_pts) + ' PV, ' \
               + ('avec architecte, ' if self.is_architect_required else '') \
               + ', '.join([str(cost_number) + ' ' + resource.name
                            for resource, cost_number in sorted(self.resources.items()) if cost_number != 0])

    def is_payable(self, payment_resources: T_Resources_int) -> bool:
        """
        Is the building is payable e.g. the cost smaller than or equals to the payment for all resources?
        Remark: However, we do not test if the architect is required or not and
                if the player played action Architect during this turn.
        Remark: All numbers of self.resources[] are <= 0 and all numbers of payment_resources are >= 0.
        :param payment_resources: Payment resources for the building.
        :return: Is the building payable e.g. is the cost smaller than or equals to the payment for all resources?
        """
        if self.is_removed:
            raise ValueError('The building ' + str(self) + ' is removed and cannot be payable')
        for resource, cost_number in self.resources.items():
            if -cost_number > payment_resources[resource]:
                return False
        return True

    def all_equiv_building_resources(self, resources_swap: Dict[Tuple[Resource, Resource], int]) \
            -> List[T_Resources_int]:
        """
        All equivalent (by applying “5 to 1“-swap resources) resources to pay for the building.
        :param resources_swap: Resources swap: number of resources obtained for 1 resource given
                               where key is (resource given, resource obtained).
        :return: All equivalent (by applying “5 to 1“-swap resources) resources to pay for the building.
        """
        if self.is_removed:
            raise ValueError('The building ' + str(self)
                             + ' is removed and cannot be considered for computing all equivalent '
                             + '(by applying “5 to 1“-swap resources) resources to pay for it.')
        return Resource.all_equiv_resources(self.resources, resources_swap)

    def new_widget(self, frame_master: LabelFrame) -> LabelFrame:
        """
        Widget of the building.
        :param frame_master: Master frame.
        :return: Widget of the building.
        """
        # Frame for the building (number of victory points, architect required?, cost number of all the resources).
        widget_building: LabelFrame = ttk.LabelFrame(master=frame_master, text='Bâtiment')  # Widget for the building.
        n_info_building: int = \
            1 \
            + (1 if self.is_architect_required else 0) \
            + len(list(filter(lambda number: number < 0,
                              self.resources.values())))  # Number of pieces of information of the building.
        widget_building.grid(row=0, column=0, rowspan=n_info_building)
        ttk.Label(master=widget_building, text=str(self.n_victory_pts) + ' PV').grid(row=0, column=0)
        i_info_building: int = 0  # Index of the pieces of information for this building.
        if self.is_architect_required:
            i_info_building = i_info_building + 1
            ttk.Label(master=widget_building, text='Architecte').grid(row=i_info_building, column=0)
        for resource, number in sorted(self.resources.items()):
            if number < 0:
                i_info_building = i_info_building + 1
                ttk.Label(master=widget_building, text=str(-number) + ' ' + resource.name
                          ).grid(row=i_info_building, column=0)
        return widget_building


class ActionLocation(Enum):
    """
    Enumeration of all the possible action locations.
    """
    HAND = auto()  # Action card in the hand of the player.
    FACE_DOWN = auto()  # Action card placed face-down in front of the player.
    REVEALED = auto()  # Action card revealed and arranged with the lower value (numbered) card on the left.
    DISCARDED = auto()  # Action card in the discard pile.

    def __str__(self) -> str:
        """
        Text for an action location.
        :return: Text for an action location.
        """
        return self.name


class Action:
    """
    Actions (cards).
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool = None):
        """
        Initialization of an action.
        :param name: Name of the action.
        :param value: Value of the action.
        :param text: Text of the action.
        :param can_be_skipped: Is the action can be skipped? Remark: It is not used!
        """
        self.name: str = name  # Name of the action.
        self.value: int = value  # Value of the action.
        self.text: str = text  # Text of the action.
        self.can_be_skipped: bool = can_be_skipped  # Is the action can be skipped?

    def __lt__(self, other_action):
        """
        Specify the order between all actions. Is the action is lower than the other action?
        :param other_action: Another action.
        :return: Is the action is lower than the other action?
        """
        if self == other_action:
            return True
        elif self.value != other_action.value:
            return self.value < other_action.value
        else:
            return self.name <= other_action.name

    def __str__(self) -> str:
        """
        Text for an action.
        :return: Text for an action.
        """
        return str(self.value) + ' ' + self.name \
               + ' ' + ('(peut être effectuée)' if self.can_be_skipped else '(doit être effectuée)') + ';' \
               + ' ' + (self.text if self.text else '')

    def is_architect(self) -> bool:
        """
        Is it the architect action?
        :return: Is it the architect action?
        """
        return isinstance(self, ArchitectAction)

    def is_protected(self) -> bool:
        """
        Is the action protects the player during the turn?
        :return: Is the action protects the player during the turn?
        """
        return isinstance(self, ProtectionAction)

    def new_widget(self, frame_master: LabelFrame, hidden: bool) -> LabelFrame:
        """
        Widget of the action.
        :param frame_master: Master frame.
        :param hidden: Action hidden?
        :return: Widget of the action.
        """
        # Frame for the action (value and name).
        widget_action: LabelFrame = ttk.LabelFrame(master=frame_master, text='Action')  # Widget for the action.
        widget_action.grid(row=0, column=0, rowspan=2)
        if hidden:
            ttk.Label(master=widget_action, text='~').grid(row=0, column=0)
            ttk.Label(master=widget_action, text='~~~~~~~').grid(row=1, column=0)
        else:
            ttk.Label(master=widget_action, text=n2str_txt_label_widget(self.value)).grid(row=0, column=0)
            ttk.Label(master=widget_action, text=self.name).grid(row=1, column=0)
        return widget_action

    @abc.abstractmethod
    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        Remark: We rename "game" by "the_game" in order to avoid (weak) warning
                "shadowing names defined in outer scopes" during the code inspection.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        pass

    @classmethod
    def thievable_players(cls, before_playing_order_players, after_playing_order_players):
        """
        Players who can be thieved by the player whose turn it is.
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :rtype List[Player]
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        :return: Players who can be thieved by the player whose turn it is.
        """
        thievable_players: List[Player] = list()  # Players who can be thieved by the player whose turn it is.
        thievable_playing_order_players: List[Player] = \
            after_playing_order_players if after_playing_order_players else \
                before_playing_order_players  # Correct playing order players.
        if thievable_playing_order_players:
            for thievable_player in thievable_playing_order_players:
                if thievable_player.is_protected():
                    printing(2, 'Le joueur ' + thievable_player.name()
                             + ' est protégé du voleur de pièces ou de matériaux.')
                else:
                    thievable_players.append(thievable_player)
        else:
            pass
        return thievable_players  # Can be [] but not equals to None!


class SiestaAction(Action):
    """
    Action Siesta.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Siesta.
        :param name: Name of the action Siesta.
        :param value: Value of the action Siesta.
        :param text: Text of the action Siesta.
        :param can_be_skipped: Is the action Siesta can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        This card causes no action, but it is very helpful in generating as low a number as possible,
        thus playing early on in the turn and making the most use of the second action card.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(2, 'Le joueur ' + player.name() + ' fait la sieste.')
        pass  # Nothing to do!


class RefreshmentAction(Action):
    """
    Action Refreshment.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Refreshment.
        :param name: Name of the action Refreshment.
        :param value: Value of the action Refreshment.
        :param text: Text of the action Refreshment.
        :param can_be_skipped: Is the action Refreshment can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The player may, but does not have to, carry out the action.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        if not player.get_actions(ActionLocation.DISCARDED):
            printing(2, 'Le joueur ' + player.name() + ' n\'a pas d\'action à récupérer.')
        else:
            action_to_refresh: Union[None, Action] = player.choose_action_to_refresh()  # Action to refresh.
            if action_to_refresh is None:
                printing(2, 'Le joueur ' + player.name() + ' n\'a pas souhaité récupérer d\'action.')
            else:
                if action_to_refresh not in player.get_actions(ActionLocation.DISCARDED):
                    raise ValueError('The player do not chose a discarded action.')
                player.actions[action_to_refresh] = ActionLocation.HAND
                player.create_widgets_frame_actions()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré l\'action ' + action_to_refresh.name
                         + ' dans sa main (auparavant dans sa défausse).')
        pass


class ProtectionAction(Action):
    """
    Action Protection.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Protection.
        :param name: Name of the action Protection.
        :param value: Value of the action Protection.
        :param text: Text of the action Protection.
        :param can_be_skipped: Is the action Protection can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If you want to save for valuable buildings by collecting a lot of building materials and/or Pesos,
        you may also want to protect yourself in time from thieves and tax collectors.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(2, 'Le joueur ' + player.name()
                 + ' se protège du percepteur et des voleurs de pièces et de matériaux.')
        # Nothing to do but the player is protected
        # from the others when they play "Tax Collector", "Pesos Thief" or "Materials Thief" during their turn.
        pass


class DebrisAction(Action):
    """
    Action Debris.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Debris.
        :param name: Name of the action Debris.
        :param value: Value of the action Debris.
        :param text: Text of the action Debris.
        :param can_be_skipped: Is the action Debris can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The action has to be carried out.
        Example: It is Vanessa’s turn to play.
        There are 4 debris in the center of the table (4 gray building materials).
        Vanessa must take all 4 gray building materials from the center of the table.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        for resource, number in sorted(the_game.table.resources.items()):
            if isinstance(resource, MaterialResource) and not resource.is_colored:
                resource.move(n_resources_to_move=number, from_table=the_game.table, to_player=player)
                the_game.table.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré (tous) les ' + str(number) + ' '
                         + resource.name + ' du centre de la table.')
        pass


class ConservationAction(Action):
    """
    Action Conservation.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Conservation.
        :param name: Name of the action Conservation.
        :param value: Value of the action Conservation.
        :param text: Text of the action Conservation.
        :param can_be_skipped: Is the action Conservation can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        Remark: Can set the game attribute indicating the action Conservation was already played during the turn.
        Remark: Can set the game attribute indicating the game is over.
        ~~~~~
        If the player thus removes 1 building card from the edge of a row of cards, return that card to the box.
        Very important: Only 1 building in total, i.e. by all players, may be removed in a round.
        Example: Sarah, Tobias and Vanessa each have a Conservation laid out in front of them.
        It is Sarah’s turn to play before Tobias and Vanessa.
        She uses her Conservation card and removes the leftmost building card in the upper row.
        Neither Tobias nor Vanessa can use their Conservation cards to remove a building in this round.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        if the_game.is_conservation_played:
            printing(2, 'Le joueur ' + player.name()
                     + ' ne peut pas effectuer cette action qui a déjà été jouée par un joueur précédent'
                     + ' qui a supprimé un bâtiment du centre de la table.')
        else:
            buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
                the_game.table.buildings_extremities_rows_and_coord()  # Buildings and their coordinates at extremities.
            building_to_remove: Building = \
                player.choose_building_to_remove(
                    list(buildings_extremities_rows_and_coord.keys()))  # Building the player wants to remove.
            if building_to_remove is None:
                printing(2, 'Le joueur ' + player.name() +
                         ' n\'a pas souhaité supprimer de bâtiment du centre de la table.')
            else:
                if building_to_remove not in buildings_extremities_rows_and_coord.keys():
                    raise ValueError(
                        'The player does not chose a building at the extremities of all rows of the table.')
                # Display the old situation.
                printing(2, 'Le bâtiment ' + str(building_to_remove) +
                         ' du centre de la table va être supprimé par le joueur ' + player.name() + '.')
                the_game.display_table(3)
                # The building at some extremity of some row of the table is removed.
                building_to_remove.is_removed = True
                the_game.table.create_widget_n_buildings_removed(the_game.game_element)
                the_game.delete_building(buildings_extremities_rows_and_coord[building_to_remove])
                # Display the new situation.
                printing(2, 'Le bâtiment ' + str(building_to_remove) +
                         ' du centre de la table vient d\'être supprimé par le joueur ' + player.name() + '.')
                the_game.display_table(3)
                # The action Conservation is played (a building was removed) and
                # the game can ends depending on buildings of table or in pile (not player).
                the_game.is_conservation_played = True
                the_game.check_is_game_over()
        pass


class TaxCollectorAction(Action):
    """
    Action Tax Collector.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Tax Collector.
        :param name: Name of the action Tax Collector.
        :param value: Value of the action Tax Collector.
        :param text: Text of the action Tax Collector.
        :param can_be_skipped: Is the action Tax Collector can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Take 1 Peso from the stock and remove 1 worker or 1 piece of building material from each following player.
        Example: It is Peter’s turn to play – he has the Tax Collector laid out.
        In this turn, Vanessa has already played, Tobias and Sarah have yet to play.
        Peter takes 1 Peso from the stock.
        Now he has to remove either 1 worker or 1 building material of his choice from each following player,
        i.e. Tobias and Sarah (not Vanessa!).
        Workers are returned to the stock, building materials are returned to the box.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # The player takes coin(s) from the stock.
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        if coin_resource.number == 0:
            printing(2, 'Le joueur ' + player.name() + ' ne peut pas récupérer de ' + coin_resource.name
                     + ' de la réserve (car il n\'y en a plus).')
        else:
            n_coins: int = 1  # Number of coin(s) to take.
            coin_resource.move(n_resources_to_move=n_coins, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_coins) + ' ' + coin_resource.name
                     + ' de la réserve.')
        # The player deletes 1 worker or 1 material resource from following players.
        for following_player in after_playing_order_players:
            if following_player.is_protected():
                printing(2, 'Le joueur ' + following_player.name() + ' est protégé (des taxes) du percepteur.')
            else:
                # Create worker and material resources available by the following player.
                following_player_resources: T_Resources_int = \
                    dict()  # Resources of the following player the tax collector can delete.
                worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
                if following_player.resources[worker_resource] >= 1:
                    following_player_resources[worker_resource] = following_player.resources[worker_resource]
                for material_resource in the_game.game_element.material_resources():
                    if following_player.resources[material_resource] >= 1:
                        following_player_resources[material_resource] = following_player.resources[material_resource]
                # The player can choose the worker or material resource to delete
                # depending on available resources the following player have.
                if following_player_resources:
                    # Resource of the following player the tax collector have to delete.
                    resource_to_delete_from_following_player: Resource = \
                        self.resource_to_delete_from_following_player(player, the_game, following_player,
                                                                      following_player_resources)
                    resource_to_delete_from_following_player.move(n_resources_to_move=1,
                                                                  from_player=following_player, to_stock=True)
                    following_player.create_widgets_frame_resources()
                    the_game.game_element.create_widgets_frame_resources()
                    printing(2, 'Le joueur ' + following_player.name() + ' a été taxé d\'1 '
                             + resource_to_delete_from_following_player.name + ' par le percepteur'
                             + (', au choix du joueur ' + player.name() if len(following_player_resources) >= 2 else '')
                             + '.')
                else:
                    printing(2, 'Le joueur ' + following_player.name() +
                             ' n\'a aucune ressouce à être taxée par le percepteur.')
        pass

    @staticmethod
    def resource_to_delete_from_following_player(player, the_game, following_player,
                                                 following_player_resources: T_Resources_int) -> Resource:
        """
        Resource of the following player the tax collector have to delete.
        :type player: Player
        :type the_game: Game
        :type following_player: Player
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param following_player: Following player the tax collector can delete.
        :param following_player_resources: Resources of the following player the tax collector can delete.
        :return: Resource of the following player the tax collector have to delete.
        """
        if len(following_player_resources) == 1:
            return first(list(following_player_resources.keys()))
        else:
            # Resource of the following player the tax collector have to delete.
            resource_to_delete_from_following_player: Resource = \
                player.choose_resource_to_delete(following_player, following_player_resources, the_game)
            if resource_to_delete_from_following_player is None:
                raise ValueError('The player does not choose 1 resource from the following player '
                                 + following_player.name() + '.')
            if resource_to_delete_from_following_player not in following_player_resources.keys():
                raise ValueError('The player chooses 1 resource '
                                 + resource_to_delete_from_following_player.name
                                 + ' from the following player ' + following_player.name()
                                 + ' who cannot be deleted (because he does not have such resource).')
            return resource_to_delete_from_following_player


class WorkerAction(Action):
    """
    Action Worker.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Worker.
        :param name: Name of the action Worker.
        :param value: Value of the action Worker.
        :param text: Text of the action Worker.
        :param can_be_skipped: Is the action Worker can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Attention: In order to be allowed to take 2 workers, you do not have to be the player with the lowest
        in the round – you merely have to be the first player in the current round to carry out this action.
        If there are not any workers left in the stock,
        the player is out of luck and cannot carry out his action this round.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # 2 worker resources for the 1st player carrying out this action, 1 for the others.
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        n_workers: int = \
            2 if self not in [action for previous_player in before_playing_order_players
                              for action in previous_player.get_actions(ActionLocation.REVEALED)] else \
                1  # Number of workers (from the stock to the player).
        n_workers_effective: int = min(n_workers, worker_resource.number)  # Effective number of workers.
        worker_resource.move(n_resources_to_move=n_workers_effective, from_stock=True, to_player=player)
        the_game.game_element.create_widgets_frame_resources()
        player.create_widgets_frame_resources()
        printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_workers_effective)
                 + ' (sur ' + str(n_workers) + ' comme escompté) ' + worker_resource.name + ' de la réserve.')
        pass


class ArchitectAction(Action):
    """
    Action Architect.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Architect.
        :param name: Name of the action Architect.
        :param value: Value of the action Architect.
        :param text: Text of the action Architect.
        :param can_be_skipped: Is the action Architect can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        The player must take a worker from the stock.
        The player is, however, not obligated to purchase a building with his architect even is possible.
        Purchasing buildings is strictly voluntary.
        If there are not any workers left in the stock,
        the player is out of luck and cannot carry out his action this round.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        if worker_resource.number >= 1:
            worker_resource.move(n_resources_to_move=1, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré 1 ' + worker_resource.name + ' de la réserve.')
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a pas récupéré de ' + worker_resource.name
                     + ' de la réserve.')
        pass


class PesosThiefAction(Action):
    """
    Action Pesos Thief.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Pesos Thief.
        :param name: Name of the action Pesos Thief.
        :param value: Value of the action Pesos Thief.
        :param text: Text of the action Pesos Thief.
        :param can_be_skipped: Is the action Pesos Thief can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If there are more than 1 players following, the Pesos Thief may choose any of those players to steal from.
        If there is no player following the Pesos Thief in this turn,
        the Pesos Thief may choose any player to steal from.
        Example: Vanessa has the Pesos Thief laid out. Sarah has already played, Peter and Tobias have yet to play.
        Vanessa has to steal from either Peter or Tobias – who she chooses is entirely up to her.
        Vanessa chooses Tobias, who has 7 Pesos in front of him.
        She takes 3 Pesos from him (3.5 = half of the Pesos; rounded down to 3).
        Attention: It is possible to have several players who played the Pesos Thief steal from one and the same player.
        It is also possible that all players have laid out the Pesos Thief – all Pesos Thieves are used consecutively
        (when it is their respective player’s turn to play).
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        thievable_players: List[Player] = \
            Action.thievable_players(before_playing_order_players,
                                     after_playing_order_players)  # Players who can be thieved.
        n_thievable_players: int = len(thievable_players)  # Number of players who can be thieved.
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        if n_thievable_players == 0:
            printing(2, 'Le joueur ' + player.name() + ' n\'a volé aucun ' + coin_resource.name
                     + ' aux autres joueurs.')
        else:
            thieved_player: Player = \
                first(thievable_players) if n_thievable_players == 1 else \
                player.choose_thieved_player(thievable_players, the_game)  # Player thieved.
            if thieved_player not in thievable_players:
                raise ValueError('The player chooses player ' + thieved_player.name() + ' who cannot be thieved.')
            n_coins_thieved: int = math.floor(thieved_player.resources[coin_resource] / 2)  # Number of coins thieved.
            coin_resource.move(n_resources_to_move=n_coins_thieved, from_player=thieved_player, to_player=player)
            thieved_player.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a volé ' + str(n_coins_thieved) + ' ' + coin_resource.name
                     + ' au joueur ' + thieved_player.name()
                     + ' parmi le(s) ' + str(len(thievable_players)) + ' pouvant être volé(s).')
        pass


class MaterialsThiefAction(Action):
    """
    Action Materials Thief.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Materials Thief.
        :param name: Name of the action Materials Thief.
        :param value: Value of the action Materials Thief.
        :param text: Text of the action Materials Thief.
        :param can_be_skipped: Is the action Materials Thief can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        All rules for playing the Pesos Thief (see above) apply to the Materials Thief accordingly.
        Important: The Materials Thief usually steals only one building material.
        It is entirely up to him which building material he takes from the respective player
        (he may also choose a gray building material!).
        Should the victim own more than 3 colored building materials,
        the Materials Thief may steal 2 building materials of his choice from that player.
        Example: Tobias has laid out the Materials Thief and wants to steal from Sarah, who is following him.
        At the moment, Sarah has 4 colored building materials (1x yellow, 1x brown and 2x red),
        as well as 3x debris in front of her.
        Tobias may take 2 building materials of his choice from Sarah.
        He chooses 1 brown and 1 gray building material.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        thievable_players: List[Player] = \
            Action.thievable_players(before_playing_order_players,
                                     after_playing_order_players)  # Players who can be thieved.
        # Determine other players and their material resources the player can thieve.
        other_players_with_resources: List[Tuple[Player, int, Dict[MaterialResource, int]]] = \
            list()  # Other players who can be thieved for materials.
        for thievable_player in thievable_players:
            thievable_material_resources: Dict[MaterialResource, int] = \
                dict()  # Material resources of a player who can be thieved.
            for material_resource, number in thievable_player.resources.items():
                if number > 0 and isinstance(material_resource, MaterialResource):
                    thievable_material_resources[material_resource] = number
            if thievable_material_resources:
                if sum([number for resource, number in thievable_material_resources.items()
                        if resource.is_colored]) > 3:
                    other_players_with_resources.append((thievable_player, 2, thievable_material_resources))
                else:
                    other_players_with_resources.append((thievable_player, 1, thievable_material_resources))
            else:
                other_players_with_resources.append(
                    (thievable_player, 0,
                     thievable_material_resources))  # The player who can be thieved does not have any resource.
        # The player chooses the player and its (0 or 1 or 2) material(s) to thieve.
        if other_players_with_resources:
            (thieved_player, (one_material_resource_to_thieve, another_material_resource_to_thieve)) = \
                player.choose_materials_to_thieve_player(other_players_with_resources)
            if thieved_player not in thievable_players:
                raise ValueError('The player chooses player ' + thieved_player.name() + ' who cannot be thieved.')
            (_, n_material_resources_to_thieve, thieved_player_material_resources) = \
                [other_player_with_resources for other_player_with_resources in other_players_with_resources
                 if other_player_with_resources[0] == thieved_player][
                    0]  # Played to thieve with his material resources.
            if not ((n_material_resources_to_thieve == 0
                     and one_material_resource_to_thieve is None
                     and another_material_resource_to_thieve is None) or
                    (n_material_resources_to_thieve == 1
                     and one_material_resource_to_thieve is not None
                     and another_material_resource_to_thieve is None) or
                    (n_material_resources_to_thieve == 2
                     and one_material_resource_to_thieve is not None
                     and another_material_resource_to_thieve is not None)):
                raise ValueError('The player do not chose a correct number ' + str(n_material_resources_to_thieve)
                                 + ' of material resources to thieve to player ' + thieved_player.name() + '.')
            if n_material_resources_to_thieve == 0:
                printing(2, 'Le joueur ' + player.name() + ' ne vole aucun matériau au joueur '
                         + thieved_player.name() + ' (qui n\'en a pas) parmi le(s) '
                         + str(len(thievable_players)) + ' pouvant être volé(s).')
            else:
                if one_material_resource_to_thieve not in thieved_player_material_resources.keys():
                    raise ValueError('The player chooses a first material resource '
                                     + one_material_resource_to_thieve.name + ' to thieve to player '
                                     + thieved_player.name() + ' who does not have this resource.')
                if n_material_resources_to_thieve == 2 \
                        and another_material_resource_to_thieve not in thieved_player_material_resources.keys():
                    raise ValueError('The player chooses a second material resource '
                                     + another_material_resource_to_thieve.name + ' to thieve to player '
                                     + thieved_player.name() + ' who does not have this resource.')
                if n_material_resources_to_thieve == 2 \
                        and one_material_resource_to_thieve == another_material_resource_to_thieve \
                        and thieved_player_material_resources[one_material_resource_to_thieve] < 2:
                    raise ValueError('The player chooses too many material resource '
                                     + one_material_resource_to_thieve.name
                                     + ' to thieve to player ' + thieved_player.name() + '.')
                # The player thieves a first material resource.
                one_material_resource_to_thieve.move(n_resources_to_move=1,
                                                     from_player=thieved_player, to_player=player)
                printing(2, 'Le joueur ' + player.name() + ' a volé 1 ' + one_material_resource_to_thieve.name
                         + ' au joueur ' + thieved_player.name()
                         + ' parmi le(s) ' + str(len(thievable_players)) + ' pouvant être volé(s).')
                # The player thieves (or not) a second material resource.
                if n_material_resources_to_thieve == 2:
                    another_material_resource_to_thieve.move(n_resources_to_move=1,
                                                             from_player=thieved_player, to_player=player)
                    printing(2, 'Le joueur ' + player.name()
                             + ' a aussi volé 1 ' + another_material_resource_to_thieve.name
                             + ' au même joueur ' + thieved_player.name() + '.')
                else:
                    printing(2, 'Le joueur ' + player.name()
                             + ' n\'a pas volé d\'autre matériau au même joueur ' + thieved_player.name() + '.')
                # Update GUI: the player thieves 1 or 2 material resource(s).
                thieved_player.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a volé aucun matériau aux autres joueurs.')
        pass


class BlackMarketAction(Action):
    """
    Action Black Market.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Black Market.
        :param name: Name of the action Black Market.
        :param value: Value of the action Black Market.
        :param text: Text of the action Black Market.
        :param can_be_skipped: Is the action Black Market can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        Attention: In order to be allowed to take 2 building materials from the bag,
        you do not have to be the player with the lowest number in the round –
        you merely have to be the first player in the current round to carry out this action.
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # 2 material resources for the 1st player carrying out this action, 1 for the others.
        draw_materials: List[MaterialResource] = the_game.game_element.draw_materials(
            2 if self not in [action for previous_player in before_playing_order_players
                              for action in previous_player.get_actions(ActionLocation.REVEALED)] else
            1)  # Draw material resources.
        for material_resource in draw_materials:
            material_resource.move(n_resources_to_move=1, from_stock=True, to_player=player)
            the_game.game_element.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré 1 ' + material_resource.name + ' de la réserve.')
        pass


class PesosAction(Action):
    """
    Action Pesos.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Pesos.
        :param name: Name of the action Pesos.
        :param value: Value of the action Pesos.
        :param text: Text of the action Pesos.
        :param can_be_skipped: Is the action Pesos can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If several players have the Pesos card laid out, they are all used consecutively
        (when it is their respective player’s turn to play).
        Example: Vanessa and Sarah both have the Pesos card laid out in front of them,
        there are 10 Pesos in the center of the table.
        Vanessa precedes Sarah and takes 5 Pesos.
        Out of the remaining 5 Pesos, Sarah takes 3 Pesos (2.5 = half of the Pesos; rounded up to 3).
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        n_coins: int = math.ceil(the_game.table.resources[coin_resource] / 2)  # Number of coins.
        coin_resource.move(n_resources_to_move=n_coins, from_table=the_game.table, to_player=player)
        the_game.table.create_widgets_frame_resources()
        player.create_widgets_frame_resources()
        printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_coins) + ' ' + coin_resource.name
                 + ' du centre de la table.')
        pass


class MamaAction(Action):
    """
    Action Mama.
    """

    def __init__(self, name: str, value: int, text: str, can_be_skipped: bool):
        """
        Initialization of the action Mama.
        :param name: Name of the action Mama.
        :param value: Value of the action Mama.
        :param text: Text of the action Mama.
        :param can_be_skipped: Is the action Mama can be skipped?
        """
        Action.__init__(self, name, value, text, can_be_skipped)

    def play(self, player, the_game=None, before_playing_order_players=None, after_playing_order_players=None) -> None:
        """
        Play the action for the player whose turn it is.
        ~~~~~
        If several players have the Mama card laid out, they are all used consecutively
        (when it is their respective player’s turn to play).
        It is entirely up to the player which building materials (half of the colored and half of the gray ones)
        he takes from the center of the table.
        Example: Peter has the Mama laid out.
        There are 6 colored building materials (1x brown, 2x yellow, and 3x red) and 5 debris
        in the center of the table.
        Peter takes 3 colored building materials (he chooses 2x yellow and 1x brown)
        as well as 3 gray building materials (half of 5 = 2.5; rounded up to 3).
        :type player: Player
        :type the_game: Game
        :type before_playing_order_players: List[Player]
        :type after_playing_order_players: List[Player]
        :param player: The player whose turn it is.
        :param the_game: Game.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        # The player chooses half of the colored building materials of the table.
        colored_materials: Dict[MaterialResource, int] = dict()  # Colored material resources of the table.
        for resource, number in the_game.table.resources.items():
            if number > 0 and isinstance(resource, MaterialResource) and resource.is_colored:
                colored_materials[resource] = number
        if colored_materials:
            n_colored_materials_to_take: int = math.ceil(
                sum(colored_materials.values()) / 2)  # Number of colored building materials the player can take.
            if len(colored_materials) == 1:
                first(list(colored_materials.keys())).move(n_resources_to_move=n_colored_materials_to_take,
                                                           from_table=the_game.table, to_player=player)
            else:
                colored_materials_chosen: Dict[MaterialResource, int] = player.choose_material_resources(
                    n_colored_materials_to_take,
                    colored_materials)  # Colored building materials in the table chosen by the player.
                if sum(colored_materials_chosen.values()) != n_colored_materials_to_take:
                    raise ValueError('The player do not chose the correct total number of building material resources '
                                     + str(n_colored_materials_to_take) + '.')
                if list(filter(lambda number: number < 0, colored_materials_chosen.values())):
                    raise ValueError('The player chooses a negative number for some building material resource.')
                if not set([colored_material_chosen for colored_material_chosen, number
                            in colored_materials_chosen.items() if number > 0]) <= set(colored_materials.keys()):
                    raise ValueError('The player chooses some non available building material resources.')
                for resource, number in colored_materials_chosen.items():
                    if number > colored_materials[resource]:
                        raise ValueError('The player chooses too many ' + str(number) + ' among available '
                                         + str(colored_materials[resource]) + ' building material resource '
                                         + resource.name + '.')
                    resource.move(n_resources_to_move=number, from_table=the_game.table, to_player=player)
            # The player takes colored building materials from the table.
            the_game.table.create_widgets_frame_resources()
            player.create_widgets_frame_resources()
            printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_colored_materials_to_take)
                     + ' matériau(x) du centre de la table.')
        else:
            printing(2, 'Le joueur ' + player.name() + ' n\'a récupéré aucun matériau du centre de la table.')
        # The player takes half of the gray (not colored) building materials (debris) of the table.
        for resource, number in sorted(the_game.table.resources.items()):
            if isinstance(resource, MaterialResource) and not resource.is_colored:
                n_resources: int = math.ceil(number / 2)  # Number of resources.
                resource.move(n_resources_to_move=n_resources, from_table=the_game.table, to_player=player)
                the_game.table.create_widgets_frame_resources()
                player.create_widgets_frame_resources()
                printing(2, 'Le joueur ' + player.name() + ' a récupéré ' + str(n_resources) + ' ' + resource.name
                         + ' du centre de la table.')
        pass


class Table:
    """
    Table i.e. elements in the center of the table.
    """

    def __init__(self, buildings: List[List[Building]], resources: T_Resources_int):
        """
        Initialization of the table.
        :param buildings: Buildings revealed and placed face-up in the center of the table.
        :param resources: Number of all resources in the center of the table.
        """
        # Attributes.
        # Buildings revealed and placed face-up in the center of the table.
        self.buildings: List[List[Building]] = buildings  # Buildings revealed in the center of the table.
        self.frame_buildings_table: LabelFrame = None  # Frame for the buildings revealed in the center of the table.
        self.widget_n_buildings_in_pile: LabelFrame = None  # Widget for the number of buildings in the pile.
        self.widget_n_buildings_removed: LabelFrame = None  # Widget for the number of buildings removed.
        # Number of all resources in the center of the table.
        self.resources: T_Resources_int = resources  # Number of all resources in the center of the table.
        self.frame_resources: LabelFrame = None  # Frame for all resources in the center of the table.

    def init_frame_table(self, the_game) -> None:
        """
        Initialization of the frame for the table of the graphical user interface.
        :type the_game: Game.
        :param the_game: Game.
        """
        # Frame for the table.
        frame_table: LabelFrame = ttk.LabelFrame(master=the_game.gui.root, text='Table')  # Frame for the table.
        frame_table.grid(row=0, column=0)
        # Frame for the buildings.
        frame_buildings: LabelFrame = ttk.LabelFrame(master=frame_table, text='Bâtiments')  # Frame for the buildings.
        frame_buildings.grid(row=0, column=0)
        # Frame for the buildings in the center of the table.
        self.frame_buildings_table = \
            ttk.LabelFrame(master=frame_buildings,
                           text='Bâtiments sur la table (' + str(len(self.buildings)) + ' lignes'
                                # + ' de ' + '+'.join([str(len(row)) for row in self.buildings])
                                + ')')
        rowspan: int = the_game.game_element.n_rows_revealed_buildings \
                       * (2 + len(the_game.game_element.resources))  # Rowspan.
        self.frame_buildings_table.grid(row=0, column=0, rowspan=rowspan, columnspan=2)
        self.create_widgets_frame_buildings_table(the_game.game_element)
        # Frame for the number of buildings in the pile.
        self.widget_n_buildings_in_pile = ttk.LabelFrame(master=frame_buildings, text='Bâtiments dans la pile')
        self.widget_n_buildings_in_pile.grid(row=rowspan + 1, column=0)
        self.create_widget_n_buildings_in_pile(the_game.game_element)
        # Frame for the number of buildings removed.
        self.widget_n_buildings_removed = ttk.LabelFrame(frame_buildings, text='Bâtiments supprimés')
        self.widget_n_buildings_removed.grid(row=rowspan + 1, column=1)
        self.create_widget_n_buildings_removed(the_game.game_element)
        # Frame for the resources.
        self.frame_resources = ttk.LabelFrame(master=frame_table, text='Ressources')
        self.frame_resources.grid(row=1, column=0, rowspan=3, columnspan=len(self.resources))
        self.create_widgets_frame_resources()

    def create_widgets_frame_buildings_table(self, the_game_element) -> None:
        """
        [Re]Create all the widgets of the frame for the buildings in the center of the table.
        :type the_game_element: GameElement.
        :param the_game_element: Game element.
        """
        # Destroy all previous widgets.
        for widget_building in self.frame_buildings_table.winfo_children():
            widget_building.destroy()
        # Create new widgets.
        row_offset: int = 2 + len(the_game_element.resources)  # Offset of the rows.
        for i_row in range(the_game_element.n_rows_revealed_buildings):
            for i_column, building in enumerate(self.buildings[i_row]):
                widget_building: LabelFrame = \
                    building.new_widget(self.frame_buildings_table)  # Widget for the building.
                widget_building.grid(row=row_offset * i_row, column=i_column)
        pass

    def create_widget_n_buildings_in_pile(self, the_game_element) -> None:
        """
        Create a widget for the number of buildings in the pile.
        :type the_game_element: GameElement.
        :param the_game_element: Game element.
        """
        ttk.Label(master=self.widget_n_buildings_in_pile,
                  text=n2str_txt_label_widget(the_game_element.n_buildings_in_location(BuildingLocation.PILE))
                  ).grid(row=0, column=0)

    def create_widget_n_buildings_removed(self, the_game_element) -> None:
        """
        Create a widget for the number of buildings removed.
        :type the_game_element: GameElement.
        :param the_game_element: Game element.
        """
        ttk.Label(master=self.widget_n_buildings_removed,
                  text=n2str_txt_label_widget(the_game_element.n_buildings_in_location(BuildingLocation.REMOVED)),
                  foreground=COLOR_REMOVED).grid(row=0, column=0)

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources, self.resources, False)

    def buildings_extremities_rows_and_coord(self) -> Dict[Building, Tuple[int, int]]:
        """
        Buildings (revealed and placed face-up) with their coordinates at the extremities (beginning and end)
        of all rows in the center of the table.
        :return Buildings (revealed and placed face-up) with their coordinates at the extremities (beginning and end)
                of all rows in the center of the table.
        """
        buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
            dict()  # Buildings with their coordinates at the extremities of all rows of the table.
        for i_row, buildings_row in enumerate(self.buildings):
            n_buildings_row: int = len(buildings_row)  # Number of buildings in the row.
            if n_buildings_row >= 1:
                buildings_extremities_rows_and_coord[buildings_row[0]] = (i_row, 0)
            if n_buildings_row >= 2:
                buildings_extremities_rows_and_coord[buildings_row[-1]] = (i_row, n_buildings_row - 1)
        return buildings_extremities_rows_and_coord

    def buildings_extremities_rows(self) -> List[Building]:
        """
        Buildings (revealed and placed face-up) at the extremities (beginning and end) of all rows
        in the center of the table.
        :return Buildings (revealed and placed face-up) at the extremities (beginning and end) of all rows
                in the center of the table.
        """
        return list(self.buildings_extremities_rows_and_coord().keys())


class Player:
    """
    Players.
    """

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of a player."
        :param player_color: Player color.
        :param resources: Number of all resources the player has.
        :param buildings: Buildings the player has purchased.
        :param actions: Deck of actions the player has.
        """
        # Player color.
        self.player_color: PlayerColor = player_color  # Player color.
        # Resources.
        self.resources: T_Resources_int = resources  # Number of all resources the player has.
        self.frame_resources: LabelFrame = None  # Frame for all resources the player has.
        # Buildings.
        self.buildings: Set[Building] = buildings  # Buildings the player has purchased.
        self.widget_n_tot_victory_pts: LabelFrame = None  # Widget for the total number of victory points.
        self.widget_n_buildings: LabelFrame = None  # Widget for the number of buildings purchased.
        # Actions.
        self.actions: Dict[Action, ActionLocation] = actions  # Deck of actions the player has.
        self.frame_actions: LabelFrame = None  # Frame for all actions.

    def __lt__(self, other_player):
        """
        Specify the order between all players. Is the player is lower than the other player?
        :param other_player: Another player.
        :return: Is the player is lower than the other player?
        """
        if self == other_player:
            return True
        else:
            return self.name() <= other_player.name()

    def __str__(self) -> str:
        """
        Text for a player.
        :return: Text for a player.
        """
        return str(self.name()) + ' : ' \
               + str(self.n_tot_victory_pts()) + ' PV avec ' + str(len(self.buildings)) + ' bâtiment(s), ' \
               + ', '.join([str(number) + ' ' + resource.name
                            for resource, number in sorted(self.resources.items())]) + ', ' \
               + ', '.join([str(any_action_location) + ' = {'
                            + ', '.join([str(action.value) + ' ' + action.name
                                         for action, action_location in sorted(self.actions.items())
                                         if action_location == any_action_location]) + '}'
                            for any_action_location in ActionLocation])

    def name(self) -> str:
        """
        Default name of the player.
        :return: Default name of the player.
        """
        return '' + self.player_color.name + ''

    def is_human(self) -> bool:
        """
        Is it the human player? (or an artificial intelligence otherwise)
        :return: Is it the human player? (or an artificial intelligence otherwise)
        """
        return isinstance(self, HumanPlayer)

    def n_tot_victory_pts(self) -> int:
        """
        Total number of victory points.
        :return: Total number of victory points.
        """
        return sum([building.n_victory_pts for building in self.buildings])

    def playing_order_details(self) -> List[int]:
        """
        Playing order details.
        :return: Playing order details.
        """
        values_revealed_arranged_actions: List[int] = \
            sorted([action.value for action, action_location in self.actions.items()
                    if action_location == ActionLocation.REVEALED])  # Values of all revealed and arranged actions.
        return [
            # Lowest value for revealed and arranged actions.
            sum(pow(10, len(values_revealed_arranged_actions) - 1 - index) * value
                for index, value in enumerate(values_revealed_arranged_actions)),
            # Fewest victory points.
            self.n_tot_victory_pts(),
            # Fewest colored building materials.
            sum([number for resource, number in self.resources.items()
                 if isinstance(resource, MaterialResource) and resource.is_colored]),
            # Fewest Pesos.
            sum([number for resource, number in self.resources.items() if isinstance(resource, CoinResource)]),
            # Fewest workers.
            sum([number for resource, number in self.resources.items() if isinstance(resource, WorkerResource)]),
            # Least gray building materials (debris).
            sum([number for resource, number in self.resources.items()
                 if isinstance(resource, MaterialResource) and not resource.is_colored])
        ]

    def init_frame_player(self, frame_players: LabelFrame, i_player: int) -> None:
        """
        Initialization of the frame for the player of the graphical user interface.
        :param frame_players: Frame for the players.
        :param i_player: Index of the player.
        """
        # Frame for the player.
        frame_player: LabelFrame = ttk.LabelFrame(master=frame_players, text=self.name())  # Frame for the table.
        frame_player.grid(row=i_player, column=1)
        # Frame for the total number of victory points.
        self.widget_n_tot_victory_pts = ttk.LabelFrame(frame_player, text='Total des PV')  # Total number of VPs.
        self.widget_n_tot_victory_pts.grid(row=0, column=0)
        self.create_widget_n_tot_victory_pts()
        # Frame for the number of buildings.
        self.widget_n_buildings = ttk.LabelFrame(frame_player, text='Nombre de bâtiments')  # Number of buildings.
        self.widget_n_buildings.grid(row=0, column=1)
        self.create_widget_n_buildings()
        # Frame for the resources.
        self.frame_resources = ttk.LabelFrame(master=frame_player, text='Ressources')
        self.frame_resources.grid(row=0, column=2, rowspan=3, columnspan=len(self.resources))
        self.create_widgets_frame_resources()
        # Frame for the actions.
        self.frame_actions = ttk.LabelFrame(master=frame_player, text='Actions')  # Frame for the actions.
        self.frame_actions.grid(row=3, column=0, columnspan=3)
        self.create_widgets_frame_actions()

    def create_widget_n_tot_victory_pts(self) -> None:
        """
        Create a widget for the total number of victory points.
        """
        ttk.Label(master=self.widget_n_tot_victory_pts, text=n2str_txt_label_widget(self.n_tot_victory_pts())
                  ).grid(row=0, column=0)

    def create_widget_n_buildings(self) -> None:
        """
        Create a widget for the total number of buildings.
        """
        ttk.Label(master=self.widget_n_buildings, text=n2str_txt_label_widget(len(self.buildings))
                  ).grid(row=0, column=0)

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources, self.resources, False)

    def create_widgets_frame_actions(self) -> None:
        """
        [Re]Create all the widgets of the frame for the actions grouped by action location.
        """
        # Destroy all previous widgets.
        for widget_action in self.frame_actions.winfo_children():
            widget_action.destroy()
        # Create new widgets.
        for column, (txt_action_location, action_location, hidden) in \
                enumerate([('dans la main', ActionLocation.HAND, not self.is_human()),
                           ('cachées', ActionLocation.FACE_DOWN, not self.is_human()),
                           ('révélées', ActionLocation.REVEALED, False),
                           ('défaussées', ActionLocation.DISCARDED, not self.is_human())]):
            self.create_widgets_frame_actions_by_location(column, 'Actions ' + txt_action_location, action_location,
                                                          hidden)

    def create_widgets_frame_actions_by_location(self, column: int, txt_action_location: str,
                                                 action_location: ActionLocation, hidden: bool) -> None:
        """
        [Re]Create all the widgets of the frame for the actions for some action location.
        :param column: Column in the frame.
        :param txt_action_location: Text associated to the frame.
        :param action_location: Action location.
        :param hidden: Action hidden?
        """
        # Frame for the actions.
        frame_player_action: LabelFrame = \
            ttk.LabelFrame(master=self.frame_actions,
                           text=txt_action_location)  # Frame for the actions for some action location.
        frame_player_action.grid(row=0, column=column)
        # Frame for all the actions for some action location.
        i_column: int = 0  # Index of the column.
        for action, actual_action_location in sorted(self.actions.items()):
            if actual_action_location == action_location:
                widget_action: LabelFrame = action.new_widget(frame_player_action, hidden)  # Widget for the action.
                widget_action.grid(row=0, column=i_column)
                i_column = i_column + 1

    @abc.abstractmethod
    def choose_actions_preparation(self, n_actions: int) -> None:
        """
        Player chooses several of his action cards (in his hand)
        and places them face-down for the time being in front of himself.
        :param n_actions: Number of actions.
        """
        pass

    @abc.abstractmethod
    def choose_new_actions(self, n_new_actions: int) -> None:
        """
        Player takes new action cards from his hand
        and places it face-down on top of some action cards laid-out in front of him.
        :param n_new_actions: Number of new actions.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        """
        Choose to carry out one of the remaining revealed actions.
        :param remaining_revealed_actions: Remaining revealed actions to carry out.
        :return: Action to carry out.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        pass

    @abc.abstractmethod
    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :type player: Player
        :type the_game: Game
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        pass

    @abc.abstractmethod
    def choose_thieved_player(self, thievable_players, the_game=None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :type thievable_players: List[Player]
        :type the_game: Game
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        pass

    @abc.abstractmethod
    def choose_materials_to_thieve_player(self, other_players_with_resources):
        """
        Choose one player and its (1 or 2) materials to thieve when performing the action Materials Thief.
        :type other_players_with_resources: List[Tuple[Player, int, T_Resources_int]]
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players whe can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (1 or 2) materials to thieve.
        """
        pass

    @abc.abstractmethod
    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        """
        Choose some number of building material resources when performing the action Mama.
        :param n_materials: Number of building materials to choose.
        :param material_resources: Building material resources among those the player have to chose.
        :return: Building material resources chosen.
        """
        pass

    def reveal_actions(self) -> None:
        """
        Reveal (all) action cards placed face-down.
        Remark: We do not arrange all revealed action cards according increasing values.
        """
        self.locate_all_actions(from_action_location=ActionLocation.FACE_DOWN,
                                to_action_location=ActionLocation.REVEALED)
        pass

    def take_actions_back(self, n_min_actions_in_hand: int) -> None:
        """
         If the player holds no sufficient action cards in his hand,
         he may now take all of his discarded actions back to his hand.
        :param n_min_actions_in_hand: Minimal number of actions.
        """
        if len([action for action, action_location in self.actions.items()
                if action_location == ActionLocation.HAND]) <= n_min_actions_in_hand:
            self.locate_all_actions(from_action_location=ActionLocation.DISCARDED,
                                    to_action_location=ActionLocation.HAND)
            self.create_widgets_frame_actions()
        pass

    def locate_all_actions(self, from_action_location: ActionLocation, to_action_location: ActionLocation) -> None:
        """
         Move all actions from an action location to an action location.
        :param from_action_location: Action location to move from.
        :param to_action_location: Action location to move to.
        """
        if from_action_location != to_action_location:
            for action, action_location in self.actions.items():
                if action_location == from_action_location:
                    self.actions[action] = to_action_location
        pass

    def get_actions(self, action_location_required: ActionLocation) -> Set[Action]:
        """
        Get all actions in some location.
        :param action_location_required: Action location required.
        :return: Get all actions in some location.
        """
        return set([action for action, action_location in self.actions.items()
                    if action_location == action_location_required])

    def all_payable_equiv_building_resources(self, all_equiv_building_resources: List[T_Resources_int]) \
            -> List[T_Resources_int]:
        """
        All payable resources for all equivalent resources to pay for a building.
        :param all_equiv_building_resources: All equivalent (by applying “5 to 1“-swap resources) resources
                                             to pay for a building.
        :return: All payable resources for all equivalent resources to pay for a building.
                 Remark: List[T_Resources_int] instead of Set[T_Resources_int] becomes to avoid error
                 "TypeError: unhashable type: 'dict'".
        """
        all_payable_equiv_building_resources: List[T_Resources_int] = \
            []  # All payable resources for all equivalent resources to pay for a building.
        for one_equiv_building_resources in all_equiv_building_resources:
            if self.is_payable(one_equiv_building_resources):
                all_payable_equiv_building_resources.append(one_equiv_building_resources)
        return all_payable_equiv_building_resources

    def is_payable(self, one_equiv_building_resources: T_Resources_int) -> bool:
        """
        Is the player can pay the building e.g.
        is each cost of the building resources smaller than or equals to the number of the same resource of the player?
        Remark: However, we do not test if the architect is required or not and
                if the player played action Architect during this turn.
        Remark: All numbers of self.resources[] are >= 0 and all numbers of one_equiv_building_resources are <= 0.
        :param one_equiv_building_resources: Building resources (cost)
                                             or some equivalent (by applying “5 to 1“-swap resources).
        :return: Is the player can pay the building?
        """
        for resource, number in self.resources.items():
            if number < -one_equiv_building_resources[resource]:
                return False
        return True

    def is_protected(self) -> bool:
        """
        Is the player protected during the turn?
        :return: Is the player protected during the turn?
        """
        return list(filter(lambda action: action.is_protected(), self.get_actions(ActionLocation.REVEALED))) != []

    def has_architect(self) -> bool:
        """
        Is the player played action Architect during the turn?
        :return: Is the player played action Architect during the turn?
        """
        return list(filter(lambda action: action.is_architect(), self.get_actions(ActionLocation.REVEALED))) != []


class HumanPlayer(Player):
    """
    Human players.
    """

    # Remark: We consider the human player do not modify values himself;
    #         in particular, he does not change parameters when he have to choose between some possibilities.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an human player."
        :param player_color: Human player color.
        :param resources: Number of all resources the human player has.
        :param buildings: Buildings the human player has purchased.
        :param actions: Deck of actions the human player has.
        """
        Player.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of an human player.
        :return: Name of an human player.
        """
        return '' + self.player_color.name + '(you!)'

    # Remark: All (following) methods choose_... of HumanPlayer can be programmed
    #         during the disintegration Tutored Project of the alternance training LP DAGPI 2019-2020.
    #         It requieres answers and responses with the console and/or the graphical user interface.

    def choose_actions_preparation(self, n_actions: int) -> None:
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.HAND][:n_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_new_actions(self, n_new_actions: int) -> None:
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.REVEALED][:n_new_actions]:
            self.actions[action] = ActionLocation.DISCARDED
        for action in [action for action, action_location in self.actions.items()
                       if action_location == ActionLocation.HAND][:n_new_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        return first(list(remaining_revealed_actions))
        pass

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        return None
        pass

    def choose_action_to_refresh(self) -> Union[None, Action]:
        return None
        pass

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        return None
        pass

    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        return first(list(resources.keys()))
        pass

    def choose_thieved_player(self, thievable_players, the_game=None):
        return first(thievable_players)
        pass

    def choose_materials_to_thieve_player(self, other_players_with_resources):
        (thieved_player, n_materials_to_thieve, material_resources) = first(other_players_with_resources)
        if n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        elif n_materials_to_thieve == 1:
            return thieved_player, (first(list(material_resources.keys())), None)
        else:
            first_material_resource: MaterialResource = first(list(material_resources.keys()))
            if material_resources[first_material_resource] >= 2:
                return thieved_player, (first_material_resource, first_material_resource)
            else:
                second_material_resource: MaterialResource = first([material_resource
                                                                    for material_resource in material_resources.keys()
                                                                    if material_resource != first_material_resource])
                return thieved_player, (first_material_resource, second_material_resource)
        pass

    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        return dict(collections.Counter([material_resource for material_resource, number in material_resources.items()
                                         for _ in range(number)][:n_materials]))
        pass


class AIPlayer(Player):
    """
    AI (artificial intelligence) players.
    -----
    In order to compare the 2 AIs, we run 1000 games for each possibility:
    AA, AB, BB, AAA, AAB, ABB, BBB, AAAA, AAAB, AABB, ABBB, BBBB
    where A is for Advanced and B is for Basic such that we have 2 to 4 players.
    The statistics are the following:
        2 players, 14.650 turns, 26.741 PVs for the winner, 20.993500 PVs for IAs Advanced, --------- PVs for IAs Basic
        2 players, 15.792 turns, 26.755 PVs for the winner, 23.486000 PVs for IAs Advanced, 18.227000 PVs for IAs Basic
        2 players, 17.168 turns, 26.686 PVs for the winner, --------- PVs for IAs Advanced, 21.001000 PVs for IAs Basic
        3 players, 13.178 turns, 21.632 PVs for the winner, 14.643667 PVs for IAs Advanced, --------- PVs for IAs Basic
        3 players, 13.714 turns, 21.733 PVs for the winner, 15.909000 PVs for IAs Advanced, 11.632000 PVs for IAs Basic
        3 players, 14.395 turns, 21.599 PVs for the winner, 17.592000 PVs for IAs Advanced, 12.836500 PVs for IAs Basic
        3 players, 15.359 turns, 21.645 PVs for the winner, --------- PVs for IAs Advanced, 14.700667 PVs for IAs Basic
        4 players, 11.532 turns, 16.446 PVs for the winner, 09.626500 PVs for IAs Advanced, --------- PVs for IAs Basic
        4 players, 11.858 turns, 16.629 PVs for the winner, 10.375667 PVs for IAs Advanced, 07.143000 PVs for IAs Basic
        4 players, 12.114 turns, 16.601 PVs for the winner, 10.970000 PVs for IAs Advanced, 07.642500 PVs for IAs Basic
        4 players, 12.793 turns, 16.601 PVs for the winner, 12.156000 PVs for IAs Advanced, 08.644333 PVs for IAs Basic
        4 players, 13.290 turns, 16.586 PVs for the winner, --------- PVs for IAs Advanced, 09.556250 PVs for IAs Basic
    """

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an AI player."
        :param player_color: AI player color.
        :param resources: Number of all resources the AI player has.
        :param buildings: Buildings the AI player has purchased.
        :param actions: Deck of actions the AI player has.
        """
        Player.__init__(self, player_color, resources, buildings, actions)

    @staticmethod
    def upper_ai_names() -> List[str]:
        """
        Uppercase AI (artificial intelligence) names.
        :return: Uppercase AI (artificial intelligence) names.
        """
        return [BasicAIPlayer.ai_name.upper(), AdvancedAIPlayer.ai_name.upper()]

    def choose_actions_preparation(self, n_actions: int) -> None:
        """
        AI player chooses several of his action cards (in his hand)
        and places them face-down for the time being in front of himself.
        :param n_actions: Number of actions.
        """
        hand_to_face_down_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.HAND]  # Actions in the hand to be placed face-down.
        if len(hand_to_face_down_actions) != len(self.actions):
            raise ValueError(str(len(self.actions) - len(hand_to_face_down_actions))
                             + ' actions are not in the hand of player ' + self.name() + '.')
        if len(hand_to_face_down_actions) < n_actions:
            raise ValueError('There is not enough ' + str(n_actions) + ' actions to place face-down among '
                             + str(len(hand_to_face_down_actions)) + ' actions in the hand of player '
                             + self.name() + '.')
        random.shuffle(hand_to_face_down_actions)
        for action in hand_to_face_down_actions[:n_actions]:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_new_actions(self, n_new_actions: int) -> None:
        """
        AI player takes new action cards from his hand
        and places it face-down on top of some action cards laid-out in front of him.
        :param n_new_actions: # Number of new actions.
        Remark: We do not use random.choices(population=[...], k=n_new_actions) because
                it returns a k sized list of elements chosen from the population with replacement
                (cf. https://docs.python.org/3.7/library/random.html).
        """
        # First: set the actions to move.
        revealed_to_discarded_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.REVEALED]  # Revealed actions to be discarded.
        if len(revealed_to_discarded_actions) < n_new_actions:
            raise ValueError('There is not enough ' + str(n_new_actions) + ' new actions to discard among '
                             + str(len(revealed_to_discarded_actions)) + ' revealed actions of player '
                             + self.name() + '.')
        random.shuffle(revealed_to_discarded_actions)
        revealed_to_discarded_actions = revealed_to_discarded_actions[:n_new_actions]
        hand_to_face_down_actions: List[Action] = \
            [action for action, action_location in self.actions.items()
             if action_location == ActionLocation.HAND]  # Actions in the hand to be placed face-down.
        if len(hand_to_face_down_actions) < n_new_actions:
            raise ValueError('There is not enough ' + str(n_new_actions) + ' new actions to place face-down among '
                             + str(len(hand_to_face_down_actions)) + ' actions in the hand of player '
                             + self.name() + '.')
        random.shuffle(hand_to_face_down_actions)
        hand_to_face_down_actions = hand_to_face_down_actions[:n_new_actions]
        # Next: move the actions set.
        for action in revealed_to_discarded_actions:
            self.actions[action] = ActionLocation.DISCARDED
        for action in hand_to_face_down_actions:
            self.actions[action] = ActionLocation.FACE_DOWN
        pass

    def choose_action_to_carry_out(self, remaining_revealed_actions: Set[Action]) -> Action:
        """
        Choose to carry out one of the remaining revealed actions.
        :param remaining_revealed_actions: Remaining revealed actions to carry out.
        :return: Action to carry out.
        """
        return random.choice(list(remaining_revealed_actions))

    @abc.abstractmethod
    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        pass

    @abc.abstractmethod
    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        pass

    @abc.abstractmethod
    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        pass

    @abc.abstractmethod
    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :type player: Player
        :type the_game: Game
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        pass

    @abc.abstractmethod
    def choose_thieved_player(self, thievable_players, the_game=None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :type thievable_players: List[Player]
        :type the_game: Game
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        pass

    @abc.abstractmethod
    def choose_materials_to_thieve_player(self, other_players_with_resources):
        """
        Choose one player and its (1 or 2) materials to thieve when performing the action Materials Thief.
        :type other_players_with_resources: List[Tuple[Player, int, T_Resources_int]]
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players whe can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (1 or 2) materials to thieve.
        """
        pass

    def choose_material_resources(self, n_materials: int, material_resources: Dict[MaterialResource, int]) \
            -> Dict[MaterialResource, int]:
        """
        Choose some number of building material resources when performing the action Mama.
        :param n_materials: Number of building materials to choose.
        :param material_resources: Building material resources among those the player have to chose.
        :return: Building material resources chosen.
        """
        return dict(collections.Counter(n_random_elements_from_frequencies(n_materials, material_resources)))


class BasicAIPlayer(AIPlayer):
    """
    Basic AI (artificial intelligence) player.
    """

    ai_name: str = 'Basic'  # Basic AI name.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of a basic AI player."
        :param player_color: Basic AI player color.
        :param resources: Number of all resources the basic AI player has.
        :param buildings: Buildings the basic AI player has purchased.
        :param actions: Deck of actions the basic AI player has.
        """
        AIPlayer.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of a basic AI player.
        :return: Name of a basic AI player.
        """
        return '' + self.player_color.name + '=' + BasicAIPlayer.ai_name + ''

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose (or not) a building to purchase.
        :param buildings_purchasable_with_resources: Buildings the AI player can purchase with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        purchasable_buildings: List[Building] = \
            [building for building in buildings_purchasable_with_resources.keys()]  # Purchasable buildings.
        i_purchasable_buildings: int = \
            random.randrange(1 + len(purchasable_buildings))  # Index of purchasable buildings.
        if i_purchasable_buildings == len(purchasable_buildings):
            return None
        else:
            building: Building = purchasable_buildings[i_purchasable_buildings]  # Building chosen to purchase.
            i_all_possible_resources: int = \
                random.randrange(
                    len(buildings_purchasable_with_resources[building]))  # Index of all possible resources.
            one_possible_resource: T_Resources_int = \
                buildings_purchasable_with_resources[building][
                    i_all_possible_resources]  # Possible resources chosen to purchase the building.
            return building, one_possible_resource

    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        Remark: Code inspection disagrees with
                return random.choice([None]+list(self.get_actions(ActionLocation.DISCARDED))).
        :return: Action to refresh.
        """
        discarded_actions_or_nothing: List[Union[None, Action]] = \
            list(self.get_actions(ActionLocation.DISCARDED)).copy()  # All discarded actions and None.
        discarded_actions_or_nothing.append(None)
        return random.choice(discarded_actions_or_nothing)

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        Remark: Code inspection disagrees with return random.choice([None]+buildings_extremities_rows).
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        buildings_extremities_rows_or_nothing: List[Union[None, Building]] = \
            buildings_extremities_rows.copy()  # All buildings at the extremities of all rows of the table and None.
        buildings_extremities_rows_or_nothing.append(None)
        return random.choice(buildings_extremities_rows_or_nothing)

    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :type player: Player
        :type the_game: Game
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        return random.choice([resource for resource, number in resources.items() for _ in range(number)])

    def choose_thieved_player(self, thievable_players, the_game=None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :type thievable_players: List[Player]
        :type the_game: Game
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        return random.choice(thievable_players)

    def choose_materials_to_thieve_player(self, other_players_with_resources):
        """
        Choose one player and its (0 or 1 or 2) material(s) to thieve when performing the action Materials Thief.
        :type other_players_with_resources: List[Tuple[Player, int, Dict[MaterialResource, int]]]
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players who can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (0 or 1 or 2) material(s) to thieve.
        """
        # Get (player to thieve, number of materials to thieve, material resources of the player to thieve).
        (thieved_player, n_materials_to_thieve, material_resources) = random.choice(other_players_with_resources)
        if n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        else:  # 1 <= n_materials_to_thieve <= 2
            flatten_material_resources: List[MaterialResource] = \
                n_random_elements_from_frequencies(2,
                                                   material_resources)  # Flatten materials of the player to thieve.
            return thieved_player, (flatten_material_resources[0],
                                    None if n_materials_to_thieve == 1 else flatten_material_resources[1])
        pass


class AdvancedAIPlayer(AIPlayer):
    """
    Advanced AI (artificial intelligence) player.
    """

    ai_name: str = 'Advanced'  # Advanced AI name.

    def __init__(self, player_color: PlayerColor, resources: T_Resources_int, buildings: Set[Building],
                 actions: Dict[Action, ActionLocation]):
        """
        Initialization of an advanced AI player."
        :param player_color: Advanced AI player color.
        :param resources: Number of all resources the advanced AI player has.
        :param buildings: Buildings the advanced AI player has purchased.
        :param actions: Deck of actions the advanced AI player has.
        """
        AIPlayer.__init__(self, player_color, resources, buildings, actions)

    def name(self) -> str:
        """
        Name of an advanced AI player.
        :return: Name of an advanced AI player.
        """
        return '' + self.player_color.name + '=' + AdvancedAIPlayer.ai_name + ''

    def choose_building_to_purchase(self,
                                    buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]]) \
            -> Union[None, Tuple[Building, T_Resources_int]]:
        """
        Choose a building to purchase.
        We weight the choice according to the number of PVs of the buildings and we consume a minimum of resources.
        :param buildings_purchasable_with_resources: Buildings the advanced AI player can purchase
                                                     with some possible resources.
        :return: Choose (or not) a building to purchase with some resources.
        """
        purchasable_buildings: List[Building] = [building
                                                 for building in buildings_purchasable_with_resources.keys()
                                                 for _ in range(building.n_victory_pts)]  # Purchasable buildings.
        building: Building = \
            purchasable_buildings[random.randrange(len(purchasable_buildings))]  # Building chosen to purchase.
        all_possible_resources: List[T_Resources_int] = \
            buildings_purchasable_with_resources[building]  # All possible resources for the building chosen.
        min_cost_resources: int = \
            max([sum([number for resource, number in one_possible_resource.items()])  # (max of nonpositive numbers)
                 for one_possible_resource in
                 all_possible_resources])  # Minimum cost of resources among all possible resources.
        min_all_possible_resources: List[T_Resources_int] = \
            [one_possible_resource for one_possible_resource in all_possible_resources
             if sum([number for resource, number in one_possible_resource.items()]) ==
             min_cost_resources]  # All possible resources requiring minimum of resources.
        return building, min_all_possible_resources[random.randrange(len(min_all_possible_resources))]

    def choose_action_to_refresh(self) -> Union[None, Action]:
        """
        Choose (or not) an action discarded to be refreshed (e.g. to get in the hand of the player)
        when performing the action Refreshment.
        :return: Action to refresh.
        """
        discarded_actions: List[Action] = list(self.get_actions(ActionLocation.DISCARDED))  # Discarded actions.
        return random.choice(discarded_actions) if discarded_actions else None

    def choose_building_to_remove(self, buildings_extremities_rows: List[Building]) -> Union[None, Building]:
        """
        Choose (or not) a building at the extremities (beginning and end) of all rows in the center of the table
        to remove when performing the action Conservation.
        :param buildings_extremities_rows: Buildings at the extremities of all rows of the table.
        :return: Building to remove.
        """
        # Get buildings at the extremities of all rows of the table the player cannot purchase
        # (due to either the architect required but not played this turn or to the cost for the building).
        non_purchasable_buildings_extremities_rows: List[Building] = \
            [building for building in buildings_extremities_rows
             if (building.is_architect_required and not self.has_architect())
             or not building.is_payable(self.resources)]
        return None if not non_purchasable_buildings_extremities_rows else \
            random.choice(non_purchasable_buildings_extremities_rows)

    def choose_resource_to_delete(self, player, resources: T_Resources_int, the_game=None) -> Resource:
        """
        Choose one resource (a worker or a material) of a following player to delete
        when performing the action Tax Collector.
        :type player: Player
        :type the_game: Game
        :param player: Following player for whom one resource is deleted.
        :param resources: Resources of the following player.
        :param the_game: Game.
        :return: Resource of a following player to delete.
        """
        worker_resource: WorkerResource = the_game.game_element.worker_resource()  # Worker resource.
        if worker_resource in resources.keys():
            # If possible, delete a worker resource.
            return worker_resource
        else:
            # If the player does not have worker resource to delete, the player have only material resources and at
            # least 2 different types of resources, perhaps debris. Delete a colored material resource (so, not debris).
            return random.choice([material_resource for material_resource, number in resources.items()
                                  if isinstance(material_resource, MaterialResource) and material_resource.is_colored
                                  for _ in range(number)])  # Remark: We use isinstance() only to avoid warnings.

    def choose_thieved_player(self, thievable_players, the_game=None):
        """
        Choose one thieved player among several players who can be thieved when performing the action Pesos Thief.
        :type thievable_players: List[Player]
        :type the_game: Game
        :rtype Player
        :param thievable_players: (several) Players who can be thieved.
        :param the_game: Game.
        :return: Player thieved.
        """
        coin_resource: CoinResource = the_game.game_element.coin_resource()  # Coin resource.
        thievable_players_repetitions: List[Player] = \
            [thievable_player for thievable_player in thievable_players for _ in range(1 + thievable_player.resources[
                coin_resource])]  # Players who can be thieved with repetition according to the number of their coins.
        return random.choice(thievable_players_repetitions)

    def choose_materials_to_thieve_player(self, other_players_with_resources):
        """
        Choose one player and its (0 or 1 or 2) material(s) to thieve when performing the action Materials Thief.
        :type other_players_with_resources: List[Tuple[Player, int, Dict[MaterialResource, int]]]
        :rtype Tuple[Player, Tuple[Union[None, MaterialResource], Union[None, MaterialResource]]]
        :param other_players_with_resources: Other players who can be thieved for materials. For each player,
                                             we consider the number of materials to thieve and available materials.
        :return: Player and its (0 or 1 or 2) material(s) to thieve.
        """
        # Get maximum number of materials to thieve.
        max_n_materials_to_thieve: int = \
            max([n_materials_to_thieve for (_, n_materials_to_thieve, _) in other_players_with_resources])
        # Get (player to thieve, number of materials to thieve, material resources of the player to thieve).
        (thieved_player, _, material_resources) = \
            random.choice([(thieved_player, n_materials_to_thieve, material_resources)
                           for (thieved_player, n_materials_to_thieve, material_resources)
                           in other_players_with_resources
                           if n_materials_to_thieve == max_n_materials_to_thieve])
        if max_n_materials_to_thieve == 0:
            return thieved_player, (None, None)
        else:  # 1 <= max_n_materials_to_thieve <= 2
            flatten_material_resources: List[MaterialResource] = \
                n_random_elements_from_frequencies(2,
                                                   material_resources)  # Flatten materials of the player to thieve.
            return thieved_player, (flatten_material_resources[0],
                                    None if max_n_materials_to_thieve == 1 else flatten_material_resources[1])
        pass


class GameElementResource:
    """
    Numbers of some resource for the elements of the game.
    """

    def __init__(self, n_preparation_table: int, n_preparation_player: int, n_supplies: int):
        """
        Initialization of numbers of some resource for the elements of the game.
        :param n_preparation_table: Number of a resource to place in the center of the table during the preparation.
        :param n_preparation_player: Number of a resource to place in the center of the table during the preparation.
        :param n_supplies: Number of a resource to place in the center of the table during the 2nd phase of supplies.
        """
        # Number of a resource to place in the center of the table during the preparation.
        self.n_preparation_table: int = n_preparation_table
        # Number of a resource for each player during the preparation.
        self.n_preparation_player: int = n_preparation_player
        # Number of a resource to place in the center of the table during the 2nd phase of supplies.
        self.n_supplies: int = n_supplies

    def __str__(self) -> str:
        """
        Text for the numbers of some resource for the elements of the game.
        :return: Text for the numbers of some resource for the elements of the game.
        """
        return str(self.n_preparation_table) + ' ressource(s) à placer au centre de la table lors de la préparation, ' \
               + str(self.n_preparation_player) + ' ressource(s) à donner à chaque joueur lors de la préparation, ' \
               + str(self.n_supplies) \
               + ' ressource(s) à ajouter au centre de la table lors de la phase d\'approvisionnements'


class GameElement:
    """
    Elements of the game read from the XML file.
    """

    def __init__(self):
        """
        Initialization of the elements of the game read from the XML file.
        """
        # Attributes (begin).
        # Name of the game.
        self.game_name: str = None
        # Game. Remark: Like a public attribute only for friend class Game.
        self.game: Game = None
        # Minimal number of players.
        self.n_min_players: int = None
        # Maximal number of players.
        self.n_max_players: int = None
        # The number of victory points to reach for a player to win (values) according to the number of players (keys).
        self.n_victory_pts_to_reach_for_players: Dict[int, int] = None
        # Number of actions i.e. number of cards placed face-down in front of each player.
        self.n_actions: int = None
        # Number of rows of buildings revealed and placed face-up in the center of the table.
        self.n_rows_revealed_buildings: int = None
        # Number of buildings by row revealed and placed face-up in the center of the table.
        self.n_revealed_buildings_by_row: int = None
        # Number of material resources to draw from the bag to place in the center of the table during the preparation.
        self.n_draw_materials_preparation_table: int = None
        # Number of material resources to draw from the bag for each player during the preparation.
        self.n_draw_materials_preparation_player: int = None
        # Number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies.
        self.n_draw_materials_supplies: int = None
        # Number of new actions i.e. number of cards each player takes from his hand and places it face-down on top of
        # any of the action cards laid-out in front of him during the 3rd phase of new action card.
        self.n_new_actions: int = None
        # Minimal number of actions i.e. each player has no more than this number of action cards in his hand
        # at the end of the 3rd phase of new action card.
        self.n_min_actions_in_hand: int = None
        # Player colors.
        self.player_colors: Set[PlayerColor] = None
        # Resources and numbers of some resource for the elements of the game.
        self.resources: Dict[Resource, GameElementResource] = None
        self.frame_resources: LabelFrame = None  # Frame for all resources in the stock.
        # Buildings.
        self.buildings: Set[Building] = None
        # Actions.
        self.actions: Set[Action] = None
        # Resources swap: number of resources obtained for 1 resource given
        # where key is (resource given, resource obtained).
        self.resources_swap: Dict[Tuple[Resource, Resource], int] = None
        # Attributes (end).
        # Check if there is enough arguments, at least the XML file.
        n_args: int = len(sys.argv)  # Number of arguments of the command.
        if n_args < 2:
            usage('Le nombre d\'arguments ' + str(n_args)
                  + ' doit être au moins égal à 2 (au moins un fichier XML est requis).')
        # Check if the XML file exists.
        if not path.isfile(sys.argv[1]):
            usage('Le fichier XML attendu ' + sys.argv[1] + ' n\'existe pas.')
        # Prepare the tree of the XML file.
        xml_tree: ElemTree.ElementTree = ElemTree.parse(sys.argv[1])  # The XML tree.
        xml_tree_root: ElemTree.Element = xml_tree.getroot()  # Root of the XML tree.
        # Read the name of the game.
        self.game_name = xml_tree_root.find('game_name').text
        # Initialization of the minimum and maximum number of players of the game read from the XML file.
        self.init_min_max_players(xml_tree_root, n_args)
        # Initialization of the number of victory points a player have to reach to win the game read from the XML file.
        self.init_n_victory_pts_to_reach_for_players(xml_tree_root)
        # Initialization of the players colors of the game read from the XML file.
        self.init_player_colors(xml_tree_root)
        # Initialization of the resources (and related attributes) read from the XML file.
        self.init_resources(xml_tree_root)
        # Initialization of the buildings (and related attributes) read from the XML file.
        self.init_buildings(xml_tree_root)
        # Initialization of the actions (and related attributes) read from the XML file.
        self.init_actions(xml_tree_root)
        # End of the initialization of the elements of the game.
        self.display()
        pass

    def init_min_max_players(self, xml_tree_root: ElemTree.Element, n_args: int) -> None:
        """
        Initialization of the minimum and maximum number of players
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        :param n_args: Number of arguments of the command.
        """
        # Read the minimum number of players.
        self.n_min_players = int(xml_tree_root.find('n_min_players').text)
        # Check if the minimum number of players is positive and is not zero.
        if self.n_min_players < 1:
            usage('Le nombre minimum de joueurs ' + str(self.n_min_players) + ' doit être au moins égal à 1.')
        # Read the maximum number of players.
        self.n_max_players = int(xml_tree_root.find('n_max_players').text)
        # Check if the minimum number of players is smaller than or equals to the maximum number of players.
        if self.n_min_players > self.n_max_players:
            usage('Le nombre minimum de joueurs '
                  + str(self.n_min_players) + ' doit être au plus égal au nombre maximum de joueurs '
                  + str(self.n_max_players) + '.')
        # Check if the number of arguments according to the number of players.
        if not (self.n_min_players <= n_args - 2 <= self.n_max_players):
            usage('Le nombre d\'arguments ' + str(n_args) + ' doit être compris entre '
                  + str(2 + self.n_min_players) + ' et ' + str(2 + self.n_max_players) + '.')
        pass

    def init_n_victory_pts_to_reach_for_players(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the number of victory points a player have to reach to win
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the number of victory points a player have to reach to win.
        # Check if numbers of victory points a player have to reach to win are positive and nonzero.
        self.n_victory_pts_to_reach_for_players = dict()
        for n_vp_to_reach_for_players_tag in xml_tree_root.findall(
                'n_victory_pts_to_reach_for_players/n_victory_pts_to_reach_for_player'):
            n_vp_to_reach_for_players: int = int(n_vp_to_reach_for_players_tag.find(
                'n_victory_pts_to_reach').text)  # Number of VPs to reach for players.
            if n_vp_to_reach_for_players < 1:
                usage('Le nombre de points de victoire à atteindre pour gagner la partie '
                      + '(récupéré à partir du fichier XML) doit être au moins égal à 1.')
            self.n_victory_pts_to_reach_for_players[int(n_vp_to_reach_for_players_tag.find('n_players').text)] = \
                n_vp_to_reach_for_players
        # Check if all numbers of players are set for the number of victory points a player have to reach to win.
        if not (set(range(self.n_min_players, self.n_max_players + 1)) <=
                self.n_victory_pts_to_reach_for_players.keys()):
            usage('Il faut préciser le nombre de points de victoire à atteindre pour gagner la partie '
                  + '(récupéré à partir du fichier XML) pour tous les nombres de joueurs possibles (entre '
                  + str(self.n_min_players) + ' et ' + str(self.n_max_players) + ').')
        pass

    def init_player_colors(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the players colors
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the player colors. Check their names are unique.
        self.player_colors = set()
        player_color_names_xml_file: Set[str] = set()  # Names of the player colors.
        for player_color_name_tag in xml_tree_root.findall('player_colors/player_color'):
            player_color_name_xml_file: str = player_color_name_tag.text  # Name of the player color.
            if player_color_name_xml_file in player_color_names_xml_file:
                usage('Il y a des doublons, notamment ' + player_color_name_xml_file
                      + ', parmi les couleurs des joueurs (récupérées à partir du fichier XML).')
            else:
                player_color_names_xml_file.add(player_color_name_xml_file)
                self.player_colors.add(PlayerColor(player_color_name_xml_file))
        # Check if the number of player colors is equals to or greater than the maximal number of players.
        if len(self.player_colors) < self.n_max_players:
            usage('Le nombre de couleurs des joueurs ' + str(len(self.player_colors))
                  + ' doit être au moins égal au nombre maximum de joueurs ' + str(self.n_max_players) + '.')
        pass

    def init_resources(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the resources
        and “5 to 1“-swap resources during building purchase
        and the number of resources
            placed in the center of the table during the preparation,
            received by each player during the preparation,
            added in the center of the table during the 2nd phase of supplies
        and the number of material resources to draw from the bag
            to place in the center of the table during the preparation,
            for each player during the preparation,
            to place in the center of the table during the 2nd phase of supplies
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Prepare the resources.
        self.resources = dict()
        # Read the worker resource.
        self.resources[WorkerResource(
            xml_tree_root.find('resources/worker/name').text,
            int(xml_tree_root.find('resources/worker/number').text),
            xml_tree_root.find(
                'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_worker_removed').text ==
            'True')] = GameElementResource(0, 0, 0)
        # Read the coin resource.
        self.resources[CoinResource(
            xml_tree_root.find('resources/coin/name').text,
            int(xml_tree_root.find('resources/coin/number').text),
            xml_tree_root.find(
                'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_coin_removed').text ==
            'True')] = GameElementResource(0, 0, 0)
        # Read the material resources.
        # Get once a player has purchased a building, is the material removed from the game?
        # (otherwise is returned to the box)
        is_material_removed: bool = xml_tree_root.find(
            'course/phase_actions_and_purchasing/is_cost_after_purchasing_removed/is_material_removed').text == 'True'
        for material_tag in xml_tree_root.findall('resources/materials/material'):
            self.resources[MaterialResource(material_tag.find('name').text, int(material_tag.find('number').text),
                                            is_material_removed, material_tag.find('is_colored').text == 'True',
                                            material_tag.find('color').text)] = GameElementResource(0, 0, 0)
        # Check if the names of the resources read are unique.
        if len(set([resource.name for resource in self.resources])) < len(self.resources):
            usage('Il y a au moins un doublon parmi les noms des ressources (récupérées à partir du fichier XML).')
        # Check if the numbers of the resources read are all non negative.
        if [resource for resource in self.resources if resource.number < 0]:
            usage('Il y a au moins un nombre de ressources '
                  + '(récupérées à partir du fichier XML) qui est strictement négatif.')
        # Check if the material resource colors read are unique.
        if len(set([material.color for material in self.material_resources()])) < len(self.material_resources()):
            usage('Il y a au moins un doublon parmi les couleurs des matériaux (récupérées à partir du fichier XML).')
        # ~~~~~
        # Prepare the “5 to 1“-swap resources during building purchase.
        self.resources_swap = dict()
        # Read the number of coins instead of a worker when a player purchases a building.
        n_coins_for_1_worker: int = int(
            xml_tree_root.find('course/phase_actions_and_purchasing/swap_during_purchasing/n_coins_for_1_worker').text)
        # Check if the number of coins instead of a worker when a player purchases a building is positive and nonzero.
        if n_coins_for_1_worker < 1:
            usage('Le nombre de pièces de monnaie pour remplacer un ouvrier pendant l\'achat de bâtiments '
                  + str(n_coins_for_1_worker) + ' doit être au moins égal à 1.')
        # Set the number of coins instead of a worker when a player purchases a building.
        self.resources_swap[(self.coin_resource(), self.worker_resource())] = n_coins_for_1_worker
        # Read the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building.
        n_material_debris_for_1_colored_material: int = int(
            xml_tree_root.find('course/phase_actions_and_purchasing/swap_during_purchasing/n_coins_for_1_worker').text)
        # Check if the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building is positive and nonzero.
        if n_material_debris_for_1_colored_material < 1:
            usage('Le nombre de débris pour remplacer un autre matériau pendant l\'achat de bâtiments '
                  + str(n_material_debris_for_1_colored_material) + ' doit être au moins égal à 1.')
        # Set the number of gray (debris) materials instead of any one colored material
        # when a player purchases a building.
        colored_materials: Set[MaterialResource] = set([material for material in self.material_resources()
                                                        if material.is_colored])  # Material resources colored.
        for non_colored_material in set(
                [material for material in self.material_resources() if not material.is_colored]):
            for colored_material in colored_materials:
                self.resources_swap[(non_colored_material, colored_material)] = n_material_debris_for_1_colored_material
        # ~~~~~
        # Read the number of coins placed in the center of the table during the preparation.
        n_coins_preparation_table: int = int(xml_tree_root.find('preparation/center_table/n_coins').text)
        # Check if the number of coins placed in the center of the table during the preparation is nonnegative.
        if n_coins_preparation_table < 0:
            usage('Le nombre de pièces de monnaie à placer au centre de la table lors de la préparation '
                  + str(n_coins_preparation_table) + ' doit être positif ou nul.')
        # Set the number of coins placed in the center of the table during the preparation.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_preparation_table = n_coins_preparation_table
        self.resources[self.coin_resource()] = game_element_resource
        # Read the number of workers received by each player during the preparation.
        n_workers_preparation_player: int = int(xml_tree_root.find('preparation/player/n_workers').text)
        # Check if the number of workers received by each player during the preparation is nonnegative.
        if n_workers_preparation_player < 0:
            usage('Le nombre d\'ouvriers à donner à chaque joueur lors de la préparation '
                  + str(n_workers_preparation_player) + ' doit être positif ou nul.')
        # Set the number of workers received by each player during the preparation.
        game_element_resource: GameElementResource = self.resources[self.worker_resource()]
        game_element_resource.n_preparation_player = n_workers_preparation_player
        self.resources[self.worker_resource()] = game_element_resource
        # Read the number of coins received by each player during the preparation.
        n_coins_preparation_player: int = int(xml_tree_root.find('preparation/player/n_coins').text)
        # Check if the number of coins received by each player during the preparation is nonnegative.
        if n_coins_preparation_player < 0:
            usage('Le nombre de pièces de monnaie à donner à chaque joueur lors de la préparation '
                  + str(n_coins_preparation_player) + ' doit être positif ou nul.')
        # Set the number of coins received by each player during the preparation.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_preparation_player = n_coins_preparation_player
        self.resources[self.coin_resource()] = game_element_resource
        # Read the number of all materials received by each player during the preparation.
        for material_resource in self.material_resources():
            material_resource_name: str = material_resource.name  # Name of the material.
            # Read the number of materials received by each player during the preparation.
            n_materials_preparation_player: int = \
                int(xml_tree_root.find('preparation/player/n_material_' + material_resource_name).text)
            # Check if the number of materials received by each player during the preparation is nonnegative.
            if n_materials_preparation_player < 0:
                usage('Le nombre de matériaux ' + material_resource_name
                      + ' à donner à chaque joueur lors de la préparation '
                      + str(n_materials_preparation_player) + ' doit être positif ou nul.')
            # Set the number of materials received by each player during the preparation.
            game_element_resource: GameElementResource = self.resources[material_resource]
            game_element_resource.n_preparation_player = n_materials_preparation_player
            self.resources[material_resource] = game_element_resource
        # Read the number of coins added in the center of the table during the 2nd phase of supplies.
        n_coins_supplies: int = int(xml_tree_root.find('course/phase_supplies/n_coins').text)
        # Check if the number of coins added in the center of the table during the 2nd phase of supplies is nonnegative.
        if n_coins_supplies < 0:
            usage('Le nombre de pièces de monnaie à ajouter au centre de la table '
                  + 'lors de la phase d\'approvisionnements ' + str(n_coins_supplies) + ' doit être positif ou nul.')
        # Set the number of coins added in the center of the table during the 2nd phase of supplies.
        game_element_resource: GameElementResource = self.resources[self.coin_resource()]
        game_element_resource.n_supplies = n_coins_supplies
        self.resources[self.coin_resource()] = game_element_resource
        # ~~~~~
        # Read the number of material resources to draw from the bag to place in the center of the table
        # during the preparation.
        self.n_draw_materials_preparation_table = \
            int(xml_tree_root.find('preparation/center_table/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag to place in the center of the table
        # during the preparation is nonnegative.
        if self.n_draw_materials_preparation_table < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à placer au centre de la table '
                  + 'lors de la préparation ' + str(self.n_draw_materials_preparation_table)
                  + ' doit être positif ou nul.')
        # Read the number of material resources to draw from the bag for each player during the preparation.
        self.n_draw_materials_preparation_player = int(xml_tree_root.find('preparation/player/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag for each player
        # during the preparation is nonnegative.
        if self.n_draw_materials_preparation_player < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à donner à chaque joueur lors de la préparation'
                  + str(self.n_draw_materials_preparation_player) + ' doit être positif ou nul.')
        # Read the number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies.
        self.n_draw_materials_supplies = int(xml_tree_root.find('course/phase_supplies/n_draw_materials').text)
        # Check if the number of material resources to draw from the bag to place in the center of the table
        # during the 2nd phase of supplies is nonnegative.
        if self.n_draw_materials_supplies < 0:
            usage('Le nombre de matériaux à piocher dans le sac et à ajouter au centre de la table '
                  + 'lors de la phase d\'approvisionnements ' + str(self.n_draw_materials_supplies)
                  + ' doit être positif ou nul.')
        # ~~~~~
        # Check if the number of each material resource is enough to place in the table and
        # to give to the maximum number of players during the preparation.
        for resource, game_element_resource in sorted(self.resources.items()):
            if game_element_resource.n_preparation_table \
                    + (game_element_resource.n_preparation_player * self.n_max_players) > resource.number:
                usage('Le nombre de matériaux ' + resource.name
                      + ' doit être suffisant pour être placés au centre de la table '
                      + 'et donnés au nombre maximum de joueurs lors de la préparation.')
        # ~~~~~
        # Check if the total number of material resources is enough to place in the table and
        # to give to the maximum number of players during the preparation.
        if sum([game_element_resource.n_preparation_table
                for resource, game_element_resource in self.resources.items()
                if isinstance(resource, MaterialResource)]) \
                + self.n_draw_materials_preparation_table \
                + (sum([game_element_resource.n_preparation_player
                        for resource, game_element_resource in self.resources.items()
                        if isinstance(resource, MaterialResource)])
                   + self.n_draw_materials_preparation_player) * self.n_max_players \
                > sum([material.number for material in self.material_resources()]):
            usage('Le nombre total de matériaux doit être suffisant pour être placés au centre de la table et '
                  + 'donnés au nombre maximum de joueurs lors de la préparation.')
        pass

    def init_buildings(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the buildings
        and number of rows of buildings and number of buildings by row revealed face-up in the center of the table
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Prepare the buildings.
        self.buildings = set()
        exists_building_with_1or2or3_vp: bool = False  # Exists a building with n_victory_pts <= 3?
        # Read the buildings.
        # Check if their cost numbers are nonpositive.
        # Check if their numbers of victory points are positive and nonzero.
        for building_tag in xml_tree_root.findall('buildings/building'):
            building_costs: T_Resources_int = dict()  # Cost for each resource of the building.
            building_costs[self.worker_resource()] = int(building_tag.find('cost/n_workers').text)
            building_costs[self.coin_resource()] = int(building_tag.find('cost/n_coins').text)
            for material_resource in self.material_resources():
                material_resource_name: str = material_resource.name  # Name of the material.
                building_costs[material_resource] = \
                    int(building_tag.find('cost/n_material_' + material_resource_name).text)
            for n_building_cost in building_costs.values():
                if n_building_cost > 0:
                    usage('Le coût d\'une resource d\'un bâtiment (récupéré à partir du fichier XML) '
                          + 'doit être négatif ou nul.')
            n_victory_pts: int = \
                int(building_tag.find('n_victory_pts').text)  # Number of victory points of the building.
            if n_victory_pts < 1:
                usage('Le nombre de points de victoire d\'un bâtiment (récupéré à partir du fichier XML) '
                      + 'doit être au moins égal à 1.')
            if n_victory_pts <= 3:
                exists_building_with_1or2or3_vp = True
            self.buildings.add(
                Building(building_tag.find('is_architect_required').text == 'True', building_costs, n_victory_pts))
        # Check if exists a building.
        if not len(self.buildings):  # Check if self.buildings is equal to {} (and also is equal to None).
            usage('Aucun bâtiment n\'a été récupéré à partir du fichier XML.')
        # Check if exists a building with a number of victory points less than or equals to 3.
        if not exists_building_with_1or2or3_vp:
            usage('Aucun des bâtiments (récupérés à partir du fichier XML) n\'a au plus 3 points de victoire.')
        # Read the number of rows of buildings revealed face-up in the center of the table.
        self.n_rows_revealed_buildings = int(
            xml_tree_root.find('preparation/center_table/revealed_buildings/n_rows_revealed_buildings').text)
        # Check if the number of rows of buildings revealed face-up in the center of the table is positive and nonzero.
        if self.n_rows_revealed_buildings < 1:
            usage('Le nombre de lignes de bâtiments à acheter ' + str(self.n_rows_revealed_buildings)
                  + ' doit être au moins égal à 1.')
        # Read the number of buildings by row revealed face-up in the center of the table.
        self.n_revealed_buildings_by_row = int(
            xml_tree_root.find('preparation/center_table/revealed_buildings/n_revealed_buildings_by_row').text)
        # Check if the number of buildings by row revealed face-up in the center of the table
        # is greater than or equals to 2.
        if self.n_revealed_buildings_by_row < 2:
            usage('Le nombre de bâtiments par ligne à acheter ' + str(self.n_revealed_buildings_by_row)
                  + ' doit être au moins égal à 2 (car une ligne a deux extrémités).')
        # Check if there are enough buildings to reveal face-up in the center of the table.
        if self.n_rows_revealed_buildings * self.n_revealed_buildings_by_row > len(self.buildings):
            usage('Il n\'y a pas assez de bâtiments ' + str(len(self.buildings))
                  + ' qui peuvent être présentés pour être achetés ('
                  + str(self.n_rows_revealed_buildings) + ' lignes de '
                  + str(self.n_revealed_buildings_by_row) + ').')
        # Check if there are sufficient total number of victory points in all buildings to lead a player to win.
        if sum(building.n_victory_pts for building in self.buildings) <= \
                max([(self.n_victory_pts_to_reach_for_players[n_player] - 1) * n_player
                     for n_player in range(self.n_min_players, self.n_max_players + 1)]):
            usage('Il peut ne pas y avoir assez de points de victoire des bâtiments '
                  + 'pour permettre à un joueur de gagner.')
        pass

    def init_actions(self, xml_tree_root: ElemTree.Element) -> None:
        """
        Initialization of the actions
        and the number of actions, the number of new actions, the minimal number of actions
        of the game read from the XML file.
        :param xml_tree_root: Root of the XML tree.
        """
        # Read the actions. Check if their names are unique. Check if their values are between 0 and 9.
        self.actions = set()
        action_names_xml_file: Set[str] = set()  # Names of the actions read from the XML file.
        for action_tag in xml_tree_root.findall('actions/action'):
            action_name_xml_file: str = action_tag.find('name').text  # Name of the action read from the XML file.
            if action_name_xml_file in action_names_xml_file:
                usage('Il y a des doublons, notamment ' + action_name_xml_file
                      + ', parmi les actions (récupérées à partir du fichier XML).')
            else:
                action_names_xml_file.add(action_name_xml_file)
                action_value_xml_file: int = \
                    int(action_tag.find('value').text)  # Value of the action read from the XML file.
                if not (0 <= action_value_xml_file <= 9):
                    usage('La valeur d\'une action ' + str(action_value_xml_file)
                          + ' (récupérées à partir du fichier XML) doit être compris entre 0 et 9.')
                else:
                    action_names_xml_file.add(action_name_xml_file)
                    action_text_xml_file: str = \
                        action_tag.find('text').text  # Text of the action read from the XML file.
                    if not action_text_xml_file:
                        action_text_xml_file = ''
                    action_can_be_skipped_xml_file: bool = action_tag.find(
                        'can_be_skipped').text == 'True'  # Is the action read from the XML file can be skipped?
                    if action_name_xml_file == 'Siesta':
                        self.actions.add(
                            SiestaAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Refreshment':
                        self.actions.add(
                            RefreshmentAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                              action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Protection':
                        self.actions.add(
                            ProtectionAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                             action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Debris':
                        self.actions.add(
                            DebrisAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Conservation':
                        self.actions.add(
                            ConservationAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                               action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Tax Collector':
                        self.actions.add(
                            TaxCollectorAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                               action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Worker':
                        self.actions.add(
                            WorkerAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                         action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Architect':
                        self.actions.add(
                            ArchitectAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                            action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Pesos Thief':
                        self.actions.add(
                            PesosThiefAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                             action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Materials Thief':
                        self.actions.add(
                            MaterialsThiefAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                 action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Black Market':
                        self.actions.add(
                            BlackMarketAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                              action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Pesos':
                        self.actions.add(PesosAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                     action_can_be_skipped_xml_file))
                    elif action_name_xml_file == 'Mama':
                        self.actions.add(MamaAction(action_name_xml_file, action_value_xml_file, action_text_xml_file,
                                                    action_can_be_skipped_xml_file))
                    else:
                        usage('Le nom de l\'action ' + action_name_xml_file
                              + ' (récupéré à partir du fichier XML) est inconnu.')
        # Check if exists an action.
        if not len(self.actions):  # Check if self.actions is equal to {} (and also is equal to None).
            usage('Aucune action n\'a été récupérée à partir du fichier XML.')
        # Read the number of actions.
        self.n_actions = int(xml_tree_root.find('preparation/n_actions').text)
        # Check if the number of actions is between 1 and the total number of possible actions.
        if not (1 <= self.n_actions <= len(self.actions)):
            usage('Le nombre d\'action(s) (par manche) ' + str(self.n_actions)
                  + ' doit être compris entre 1 et le nombre total d\'actions possibles ' + str(len(self.actions))
                  + '.')
        # Read the number of new actions.
        self.n_new_actions = int(xml_tree_root.find('course/phase_new_action_card/n_new_actions').text)
        # Check if the number of new actions is between 1 and the number of actions.
        if not (1 <= self.n_new_actions <= self.n_actions):
            usage('Le nombre de nouvelle(s) action(s) (à chaque manche) ' + str(self.n_new_actions)
                  + ' doit être compris entre 1 et le nombre d\'action(s) (par manche) ' + str(self.n_actions)
                  + '.')
        # Read the minimal number of actions.
        self.n_min_actions_in_hand = int(xml_tree_root.find('course/phase_new_action_card/n_min_actions_in_hand').text)
        # Check if the minimal number of actions is between 1 and the total number of possible actions.
        if not (self.n_new_actions <= self.n_min_actions_in_hand <= len(self.actions)):
            usage('Le nombre d\'action(s) minimum (en main) ' + str(self.n_min_actions_in_hand)
                  + ' doit être compris entre le nombre de nouvelle(s) action(s) (à chaque manche) '
                  + str(self.n_new_actions) + ' et le nombre total d\'actions possibles ' + str(len(self.actions))
                  + '.')
        pass

    def init_frame_stock(self) -> None:
        """
        Initialization of the frame for the stock of the graphical user interface.
        """
        # Frame for the stock.
        frame_stock: LabelFrame = ttk.LabelFrame(master=self.game.gui.root, text='Réserve')  # Frame for the stock.
        frame_stock.grid(row=1, column=0)
        # Frame for the resources in the stock.
        self.frame_resources = ttk.LabelFrame(master=frame_stock, text='Ressources')
        self.frame_resources.grid(row=0, column=0, rowspan=4, columnspan=len(self.resources))
        self.create_widgets_frame_resources()

    def create_widgets_frame_resources(self) -> None:
        """
        [Re]Create all the widgets of the frame for the resources.
        """
        Resource.create_widgets_frame_resources(self.frame_resources,
                                                {resource: resource.number for resource in self.resources}, True)

    def display(self) -> None:
        """
        Display all the elements of the game read from the XML file.
        """
        printing(0, self.game_name
                 + ', ' + 'de ' + str(self.n_min_players) + ' à ' + str(self.n_max_players) + ' joueurs'
                 + ', ' + str(len(self.player_colors)) + ' couleurs de joueurs'
                 + ', ' + str(len(self.resources)) + ' ressources'
                 + ', ' + str(len(self.buildings)) + ' bâtiments'
                 + ', ' + str(len(self.actions)) + ' actions'
                 + '.')
        printing(1, 'Couleurs de joueurs :')
        for player_color in sorted(self.player_colors):
            printing(2, str(player_color))
        printing(1, 'Ressources :')
        for resource, game_element_resource in sorted(self.resources.items()):
            printing(2, resource.name)
            printing(3, str(resource))
            printing(3, str(game_element_resource))
        printing(1, 'Nombre de matériaux à piocher dans le sac : '
                 + str(self.n_draw_materials_preparation_table)
                 + ' à placer au centre de la table lors de la préparation, '
                 + str(self.n_draw_materials_preparation_player)
                 + ' à donner à chaque joueur lors de la préparation, '
                 + str(self.n_draw_materials_supplies)
                 + ' à ajouter au centre de la table lors de la phase d\'approvisionnements')
        printing(1, 'Échanges entre ressources :')
        for (resource_given, resource_obtained), number_for_1 in sorted(self.resources_swap.items()):
            printing(2, str(number_for_1) + ' ' + resource_given.name + ' for 1 ' + resource_obtained.name)
        printing(1, 'Actions :')
        for action in sorted(self.actions):
            printing(2, str(action))
        printing(1, str(self.n_actions) + ' action(s) (par manche), '
                 + str(self.n_new_actions) + ' nouvelle(s) action(s) (à chaque manche), '
                 + str(self.n_min_actions_in_hand) + ' action(s) minimum (en main)')
        printing(1, 'Bâtiments :')
        for building in sorted(self.buildings):
            printing(2, str(building))
        printing(1, 'Les bâtiments à acheter forment '
                 + str(self.n_rows_revealed_buildings) + ' lignes de ' + str(self.n_revealed_buildings_by_row)
                 + ' au centre de la table')
        printing(1, 'Points de victoire à atteindre pour gagner la partie selon le nombre de joueurs :')
        for n_players, n_victory_pts_to_reach in sorted(self.n_victory_pts_to_reach_for_players.items()):
            printing(2, str(n_victory_pts_to_reach) + ' points de victoire à ' + str(n_players) + ' joueurs')

    def worker_resource(self) -> WorkerResource:
        """
        Worker resource.
        :return: Worker resource.
        """
        workers_resources: List[WorkerResource] = [resource for resource in self.resources
                                                   if isinstance(resource, WorkerResource)]  # Workers as resources.
        return first(workers_resources) if workers_resources else None  # Remark: We consider exists 1! worker resource.

    def coin_resource(self) -> CoinResource:
        """
        Coin resource.
        :return: Coin resource.
        """
        coins_resources: List[CoinResource] = [resource for resource in self.resources
                                               if isinstance(resource, CoinResource)]  # Coins as resources.
        return first(coins_resources) if coins_resources else None  # Remark: We consider exists 1! coin resource.

    def material_resources(self) -> Set[MaterialResource]:
        """
        Material resources.
        :return: Material resources.
        """
        return set([resource for resource in self.resources.keys() if isinstance(resource, MaterialResource)])

    def material_resource(self, material_name: str) -> MaterialResource:
        """
        Material resource for which its name is equals to the name given in parameter.
        :param material_name: Name of the material resource.
        :return: Material resource for which its name is equals to the name given in parameter.
        """
        # Get materials such that their name is equals to the name given in parameter.
        material_resources_correct_name: List[MaterialResource] = [material for material in self.material_resources()
                                                                   if material.name == material_name]
        return first(material_resources_correct_name) if material_resources_correct_name else None

    def resource(self, resource_name: str) -> Resource:
        """
        Resource for which its name is equals to the name given in parameter.
        :param resource_name: Name of the resource.
        :return: Resource for which its name is equals to the name given in parameter.
        """
        # Get resources such that their name is equals to the name given in parameter.
        resources_correct_name: List[Resource] = [resource for resource in self.resources
                                                  if resource.name == resource_name]
        return first(resources_correct_name) if resources_correct_name else None

    def draw_materials(self, n_draw_materials: int) -> List[MaterialResource]:
        """
        Draw material resources ("without looking, draw material resources (cubes) from the bag").
        :param n_draw_materials: Number (maximum) of materials to draw (from the bag).
        :return: Material resources.
        """
        return n_random_elements_from_frequencies(n_draw_materials, {material_resource: material_resource.number
                                                                     for material_resource in self.material_resources()
                                                                     if material_resource.number > 0})

    def draw_buildings(self, n_draw_buildings: int, only_in_pile: bool) -> List[Building]:
        """
        Draw buildings. For example,
        "Reveal 12 building and place them face-up in two rows of 6 cards each in the center of the table.".
        :param n_draw_buildings: Number (maximum) of buildings to draw (from the bag).
        :param only_in_pile: Should we consider buildings only in the pile
                             (e.g. of building as a face-down draw pile at the table edge) that is
                             neither purchased by any player nor in the center of the table?
                             (otherwise, consider all buildings)
        :return: Buildings.
        """
        return n_random_elements_from_sequence(n_draw_buildings,
                                               self.buildings_in_pile() if only_in_pile else
                                               [building for building in self.buildings])

    def buildings_in_pile(self) -> List[Building]:
        """
        Buildings in the pile (e.g. of building as a face-down draw pile at the table edge) that is
        neither removed nor purchased by any player nor in the center of the table.
        :return: Buildings in the pile (e.g. of building as a face-down draw pile at the table edge).
        """
        return [building for building in self.buildings
                if not building.is_removed
                and building not in [building for player in self.game.players for building in player.buildings]
                and building not in [building for building_row in self.game.table.buildings
                                     for building in building_row]]

    def n_buildings_in_location(self, building_location: BuildingLocation) -> int:
        """
        Number of buildings in some location.
        :param building_location
        :return: Number of buildings in some location.
        """
        if building_location == BuildingLocation.TABLE:
            return len([None for building_row in self.game.table.buildings for _ in building_row])
        elif building_location == BuildingLocation.PURCHASED:
            return len([None for player in self.game.players for _ in player.buildings])
        elif building_location == BuildingLocation.PILE:
            return len(self.buildings_in_pile())
        elif building_location == BuildingLocation.REMOVED:
            return len(list(filter(lambda building: building.is_removed, self.buildings)))
        else:
            raise ValueError('Building location ' + str(building_location) + ' is unknown.')


class Game:
    """
    Game.
    """

    def __init__(self, the_game_element: GameElement):
        """
        Initialization (preparation) of the game.
        Remark: We rename "game_element" by "the_game_element" in order to avoid (weak) warning
                "shadowing names defined in outer scopes" during the code inspection.
        ~~~~~
        - Each player receives an identical set of 13 action cards and takes them in his hand.
        - Place the Pesos, workers, and the bag (containing all 80 building material cubes) as a stock
          at the table edge.
          Shuffle the 36 buildings.
          Reveal 12 building and place them face-up in two rows of 6 cards each in the center of the table.
          Note: there should be at least one building with a value of 1, 2, or 3 on either end of at least
                one of the rows.
          Place the remaining building as a face-down draw pile at the table edge.
        - Each player draws 1 building material cube from the bag and places it in front of himself.
          Additionally, each player receives 1 Peso, which he also places in front of himself.
        - Without looking, draw 3 cubes from the bag and place them below the 2 rows of face-up building cards
          in the center of the table.
          Also place 4 Pesos in the center of the table.
        - Each player chooses any 2 of his 13 action cards and places them face-down for the time being
          in front of himself.
          Once all players have done that, the 2 actions cards are revealed and arranged with the lower numbered card
          on the left.
        :param the_game_element: Elements of the game read from the XML file.
        """
        # Attributes (begin).
        # Graphical user interface. Remark: Like a public attribute only for friend class Gui.
        self.gui: Gui = None
        # Elements of the game read from the XML file.
        self.game_element: GameElement = the_game_element
        # Players.
        self.players: Set[Player] = None
        # Table.
        self.table: Table = None
        # Attributes used when the game is played.
        # Turn.
        self.turn: int = None  # Turn.
        self.widget_turn: LabelFrame = None  # Widget for the turn.
        # Is the action Conservation already played during the turn?
        # Remark: Like a public attribute only for friend class ConservationAction.
        self.is_conservation_played: bool = None
        # Is the game over?
        # Remark: Like a public attribute only for friend class ConservationAction.
        self.is_game_over: bool = None
        # Set the game for the game element: we have 1 game_element <---> 1 game.
        self.game_element.game = self
        # Attributes (end).
        # Initialization (preparation) of the table.
        self.init_table()
        # Initialization (preparation) of the (ordered) players.
        self.init_players()
        for player in sorted(self.players):
            player.choose_actions_preparation(self.game_element.n_actions)
        # End of the initialization of the game.
        # self.display()
        pass

    def init_table(self) -> None:
        """
        Initialization (preparation) of the table.
        """
        # Prepare the buildings.
        # Check if exists at least one building with number of victory points <= 3 on
        # either end of at least one of the rows.
        exists_building_3_or_less_vp: bool = False  # Exists building with VPs <= 3 at the end of some row?
        buildings: List[List[Building]] = None  # Initial buildings of the table.
        while not exists_building_3_or_less_vp:
            buildings = []
            # Draw all buildings.
            n_draw_buildings: int = self.game_element.n_rows_revealed_buildings * \
                                    self.game_element.n_revealed_buildings_by_row  # Number of buildings to draw.
            draw_buildings = self.game_element.draw_buildings(n_draw_buildings=n_draw_buildings, only_in_pile=False)
            # Split all buildings (in several rows of same length).
            for _ in range(self.game_element.n_rows_revealed_buildings):
                row_buildings: List[Building] = \
                    draw_buildings[:self.game_element.n_revealed_buildings_by_row]  # One row of buildings.
                if row_buildings[0].n_victory_pts <= 3 or row_buildings[-1].n_victory_pts <= 3:
                    exists_building_3_or_less_vp = True
                buildings.append(row_buildings)
                draw_buildings = draw_buildings[self.game_element.n_revealed_buildings_by_row:]
            if draw_buildings:
                raise ValueError('The draw buildings ' + str(draw_buildings)
                                 + ' during the initialization of the table is not [].')
        # Prepare the resources.
        resources: T_Resources_int = \
            {resource: 0 for resource in self.game_element.resources.keys()}  # Initial resources of the table.
        # Initialize the table.
        self.table = Table(buildings, resources)
        # Update resources from stock to the table during preparation.
        for resource, game_element_resource in self.game_element.resources.items():
            resource.move(n_resources_to_move=game_element_resource.n_preparation_table,
                          from_stock=True, to_table=self.table)
        # Draw material resources from stock to the table during preparation.
        draw_materials: List[MaterialResource] = \
            self.game_element.draw_materials(self.game_element.n_draw_materials_preparation_table)  # Draw materials.
        for material in draw_materials:
            material.move(n_resources_to_move=1, from_stock=True, to_table=self.table)
        pass

    def init_players(self) -> None:
        """
        Initialization (preparation) of the players.
        """
        # Prepare the players.
        self.players = set()
        # Prepare the elements the player has initially.
        resources: T_Resources_int = \
            {resource: 0 for resource in self.game_element.resources.keys()}  # Initial resources the player has.
        buildings: Set[Building] = set()  # Initial buildings the player has purchased.
        actions: Dict[Action, ActionLocation] = \
            {action: ActionLocation.HAND
             for action in self.game_element.actions}  # Initial deck of actions the player has.
        # Check the players: colors are unique, ai names exist, 1! human.
        player_color_names: Dict[str, PlayerColor] = {player_color.name: player_color for player_color in
                                                      self.game_element.player_colors}  # Player colors by their names.
        n_human_players: int = 0  # Number of human players.
        for arg in sys.argv[2:]:
            # Remark: The argument of the command is either <color> for a human or <color=ai_name> for an AI.
            arg_split: List[str] = arg.split('=')  # Argument is split.
            arg_split_len: int = len(arg_split)  # Length of the argument once split.
            arg_player_color_name: str = arg_split[0]  # Player color name obtained from the argument.
            arg_upper_ai_name: str = (arg_split[1].upper() if arg_split_len == 2 else
                                      None)  # Uppercase AI name obtained from the argument.
            # Check the argument of the command.
            if not (1 <= arg_split_len <= 2):
                usage('L\'argument ' + arg + ' de la commande ne respecte pas le format.')
            if arg_player_color_name not in player_color_names.keys():
                usage('La couleur de joueur ' + arg_player_color_name + ' de l\'argument ' + arg
                      + ' de la commande n\'existe pas ou a déjà été attribuée à un autre joueur.')
            if arg_split_len == 2 and arg_upper_ai_name not in AIPlayer.upper_ai_names():
                usage('Le nom ' + arg_upper_ai_name + ' pour une intelligence artificielle de l\'argument ' + arg
                      + ' de la commande n\'existe pas.')
            # Initialization of a player.
            player: Player = None  # Player.
            if arg_split_len == 1:
                n_human_players += 1
                player = HumanPlayer(player_color_names[arg_player_color_name],
                                     resources.copy(), buildings.copy(), actions.copy())
            elif arg_upper_ai_name == BasicAIPlayer.ai_name.upper():
                player = BasicAIPlayer(player_color_names[arg_player_color_name],
                                       resources.copy(), buildings.copy(), actions.copy())
            elif arg_upper_ai_name == AdvancedAIPlayer.ai_name.upper():
                player = AdvancedAIPlayer(player_color_names[arg_player_color_name],
                                          resources.copy(), buildings.copy(), actions.copy())
            else:
                usage('Le nom ' + arg_upper_ai_name + ' pour une intelligence artificielle de l\'argument ' + arg
                      + ' de la commande est inconnu.')
            # Add the player.
            self.players.add(player)
            # Update resources from stock to the player during preparation.
            for resource, game_element_resource in self.game_element.resources.items():
                resource.move(n_resources_to_move=game_element_resource.n_preparation_player,
                              from_stock=True, to_player=player)
            # Draw material resources from stock to the player during preparation.
            draw_materials: List[MaterialResource] = self.game_element.draw_materials(
                self.game_element.n_draw_materials_preparation_player)  # Draw materials.
            for material in draw_materials:
                material.move(n_resources_to_move=1, from_stock=True, to_player=player)
            # Delete the player color.
            del player_color_names[arg_player_color_name]
        if n_human_players > 1:
            usage('Le nombre de joueurs humains ' + str(n_human_players) + ' doit être d\'au plus 1.')
        pass

    def init_gui(self) -> None:
        """
        Initialization of all frames of the graphical user interface.
        """
        self.table.init_frame_table(self)
        self.game_element.init_frame_stock()
        self.init_frame_game()
        self.init_frame_players()

    def init_frame_game(self) -> None:
        """
        Initialization of the frame on some data of the game of the graphical user interface.
        """
        # Frame on some data of the game.
        frame_game: LabelFrame = ttk.LabelFrame(master=self.gui.root, text='Jeu')  # Frame on some data of the Game.
        frame_game.grid(row=2, column=0, rowspan=self.game_element.n_max_players)
        # Frame for the turn.
        self.widget_turn = ttk.LabelFrame(master=frame_game, text='Manche')
        self.widget_turn.grid(row=0, column=0)
        self.create_widget_turn()
        # Frame for (ordered) players.
        n_players: int = len(self.players)  # Number of players.
        widget_players: LabelFrame = ttk.LabelFrame(master=frame_game,
                                                    text=str(n_players) + ' joueurs')  # Widget for the players.
        widget_players.grid(row=0, column=1, rowspan=n_players)
        for i_player, player in enumerate(sorted(self.players)):
            ttk.Label(master=widget_players, text=player.name()).grid(row=i_player, column=0)
        # Frame for the number of victory points to reach for a player to win.
        widget_n_victory_pts_to_reach: LabelFrame = \
            ttk.LabelFrame(master=frame_game, text='PV à atteindre')  # Widget for the number of VPs to reach.
        widget_n_victory_pts_to_reach.grid(row=0, column=2)
        ttk.Label(master=widget_n_victory_pts_to_reach,
                  text=n2str_txt_label_widget(self.game_element.n_victory_pts_to_reach_for_players[n_players])
                  ).grid(row=0, column=0)

    def init_frame_players(self) -> None:
        """
        Initialization of the frame for the players of the graphical user interface.
        """
        # Frame for the players.
        frame_players: LabelFrame = ttk.LabelFrame(master=self.gui.root, text='Joueurs')  # Frame for the players.
        frame_players.grid(row=0, column=1, rowspan=len(self.players))
        # Frame for all (ordered) players.
        for i_player, player in enumerate(sorted(self.players)):
            player.init_frame_player(frame_players, i_player)

    def create_widget_turn(self) -> None:
        """
        Create a widget for the turn.
        """
        if self.turn:
            ttk.Label(master=self.widget_turn, text=n2str_txt_label_widget(self.turn)).grid(row=0, column=0)

    def display(self, playing_order_players: List[Player]) -> None:
        """
        Display the game (for one turn).
        :param playing_order_players: Playing order among all players.
        """
        if self.turn is not None and not self.is_game_over:
            printing(0, LINE_SEPARATOR)
            printing(0, 'Manche : ' + str(self.turn))
        self.display_stock(1)
        self.display_table(1)
        self.display_players(0, playing_order_players)

    def display_stock(self, n_indent_to_add: int) -> None:
        """
        Display the elements in the stock.
        :param n_indent_to_add: Number of indentations to add.
        """
        printing(0 + n_indent_to_add,
                 'Réserve : ' + ', '.join([str(resource.number) + ' ' + resource.name
                                           for resource in sorted(self.game_element.resources.keys())]))

    def display_table(self, n_indent_to_add: int) -> None:
        """
        Display the elements of the table.
        :param n_indent_to_add: Number of indentations to add.
        """
        printing(0 + n_indent_to_add, 'Au centre de la table :')
        printing(1 + n_indent_to_add, 'Bâtiments :')
        for i_row, building_row in enumerate(self.table.buildings):
            printing(2 + n_indent_to_add,
                     'Ligne n° ' + str(i_row + 1) + ' (de ' + str(len(building_row)) + ' bâtiments) :')
            for building in building_row:
                printing(3 + n_indent_to_add, str(building))
        printing(2 + n_indent_to_add, 'Bâtiments achetables :')
        for building in self.table.buildings_extremities_rows():
            printing(3 + n_indent_to_add, str(building))
        printing(1 + n_indent_to_add,
                 'Ressources : ' + ', '.join([str(number) + ' ' + resource.name
                                              for resource, number in sorted(self.table.resources.items())]))

    def display_players(self, n_indent_to_add: int, playing_order_players: List[Player]) -> None:
        """
        Display the elements of the players.
        :param n_indent_to_add: Number of indentations to add.
        :param playing_order_players: Playing order among all players.
        """
        printing(1 + n_indent_to_add, str(len(self.players)) + ' joueurs (dans l\'ordre de leurs tours de jeu) :')
        for player in playing_order_players:
            self.display_player(n_indent_to_add, player)

    @classmethod
    def display_player(cls, n_indent_to_add: int, player: Player) -> None:
        """
        Display the elements of the player.
        :param n_indent_to_add: Number of indentations to add.
        :param player: Player to display.
        """
        printing(2 + n_indent_to_add, str(player))

    def play(self) -> None:
        """
        Play the game (course of the game).
        ~~~~~
        Each round consists of the following 3 consecutive phases:
        First, all players, separately and in turn, carry out 1st phase
        (“carry out two actions and purchase buildings”).
        In 2nd phase (“supplies”), 3 new Pesos and 3 new building materials are placed from the stock
        in the center of the table.
        In the concluding 3rd phase (“new action card”), all players place exactly 1 new action card
        in front of themselves.
        """
        # End the initialization (preparation) of the (ordered) players by revealing the actions.
        messagebox_showinfo('Fin de la préparation (actions cachées)',
                            'Durant cette phase de préparation,\n'
                            + 'chaque joueur a pris les premières actions de sa main\n'
                            + 'et les a placées faces cachées.\n')
        for player in sorted(self.players):
            player.reveal_actions()
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Fin de la préparation (actions révélées)',
                            'Durant cette phase de préparation,\nles premières actions des joueurs sont révélées.\n')
        # Start of the game.
        self.is_game_over = False
        self.turn = 0
        self.create_widget_turn()
        while not self.is_game_over:
            # New turn.
            self.turn = self.turn + 1
            self.create_widget_turn()
            messagebox_showinfo('Nouvelle manche', 'Manche ' + str(self.turn) + '.')
            self.is_conservation_played = False
            playing_order_players: List[Player] = self.playing_order_players()  # Playing order among all players.
            self.display(playing_order_players)
            # Players carries out actions and can purchase buildings.
            self.play_carry_out_actions_purchase_buildings(playing_order_players)
            # Supplies and players take new actions.
            if not self.is_game_over:
                self.play_supplies()
                self.play_new_actions(playing_order_players)
        # End of the game.
        self.end_of_game()
        pass

    def playing_order_players(self) -> List[Player]:
        """
        Playing order among all players.
        :return: Playing order among all players.
        """
        playing_order_players: List[Player] = []  # Playing order among all players.
        playing_order_details_players: Dict[Player, List[int]] = \
            {player: player.playing_order_details() for player in self.players}  # Playing order details of all players.
        while playing_order_details_players:
            min_playing_order_details: List[int] = \
                min(playing_order_details_players.values())  # Minimum of playing order details.
            players_min_playing_order_details: List[Player] = \
                [player for player, playing_order_details in playing_order_details_players.items() if
                 playing_order_details == min_playing_order_details]  # Players having minimum of playing order details.
            random.shuffle(players_min_playing_order_details)  # Instead of "If they tie, the younger player begins.".
            playing_order_players.extend(players_min_playing_order_details)
            for player_min_playing_order_details in players_min_playing_order_details:
                del playing_order_details_players[player_min_playing_order_details]
        return playing_order_players

    def play_carry_out_actions_purchase_buildings(self, playing_order_players: List[Player]):
        """
        Play the 1st phase: (the players can) carry out two actions and purchase buildings.
        Remark: Can set the game attribute indicating the game is over.
        :param playing_order_players: Playing order among all players.
        ~~~~~
        - The player whose laid-out cards show the lowest number begins and carries out the actions of his 2 cards,
          in any order.
          The player with the second lowest number takes his turn and carries out his two actions in any order.
          Now, the player with the third lowest two-digit number goes, and so on, until each player has taken their turn
          and carried out his 2 actions.
        - The actions are explained in detail on the cards! The 2 action cards remain lying face-up.
          Note: An action cannot be skipped- unless the respective card expressly allows it.
        - Playing order in case of identical numbers:
          In case several players show the same number, the player among them owning the fewest victory points begins.
          If their number of victory points is also the same, the player owning the fewest colored building materials
          in total begins.
          If there still is a tie, the one owning the fewest Pesos begins.
          Then the fewest workers.
          Then the least gray building materials (debris).
          If they tie in all respects, the younger player begins.
        - Purchasing buildings: On a player’s turn, after he has carried out both of his actions, he can buy any number
          of the face-up building cards.
          In order to do so, he must fulfill/hand over what is shown on that particular building card.
          * Building materials and gray debris must be handed over and returned to the box (removed from the game).
          * Pesos and workers are returned to the table edge.
          * If the architect is required, the player must have him laid-out in front of himself now,
            i.e. as one of his currently laid-out action cards.
            The architect is not handed over after the purchase of a building – he remains where he is!
        - Very important: In principle, buildings can only be purchased if they lay on the far left or the far right
          of the two rows of cards – this makes exactly 4 available cards at all times.
          During the course of the game, the cards are essentially purchased “from the outside to the inside”.
          A player may choose in which order he purchases which card(s) in which row.
          For example, provided he has sufficient resources, he can first buy the upper left building card,
          then the lower right one and immediately another lower right one.
          Note: Only the current player in phase 1 may purchase buildings – no one else.
                In the 2nd and 3rd phases no buildings at all can be purchased.
        - “5 to 1“-swap during building purchase
          If a player purchases a building, he may choose to pay 5 Pesos instead of a worker (back to the stock).
          Also, 5 gray building material cubes (debris) can be paid instead of any one colored cube
          of building material (removed from the game to the box).
          Note: It is not possible to swap workers for Pesos or colored building materials for gray building materials!
        - If in the course of the game only 2 cards remain in a row, immediately draw 4 new buildings
          from the face-down draw pile at the table edge and place them face-up between the 2 old cards.
        further course of the game: All of the following rounds are played just as described.
        """
        for i_playing_order_players, player in enumerate(playing_order_players):
            if not self.is_game_over:
                self.play_carry_out_actions(player, i_playing_order_players,
                                            playing_order_players[:i_playing_order_players],
                                            playing_order_players[i_playing_order_players + 1:])
                if not self.is_game_over:
                    self.play_purchase_buildings(player)
        pass

    def play_carry_out_actions(self, player: Player, i_playing_order_players: int,
                               before_playing_order_players: List[Player],
                               after_playing_order_players: List[Player]) -> None:
        """
        [1st subphase of 1st phase] The player have to carry out revealed actions.
        Remark: Action Conservation can set the game attribute indicating the game is over.
        Remark: Action Conservation can set the game attribute indicating the action Conservation was already played
                during the turn.
        :param player: The player whose turn it is.
        :param i_playing_order_players: Order of the player during this turn.
        :param before_playing_order_players: Players before according to playing order among all players.
        :param after_playing_order_players: Players after according to playing order among all players.
        """
        printing(0, 'Tour de jeu du joueur ' + player.name() + ' (n° ' + str(i_playing_order_players + 1)
                 + ' dans l\'ordre du tour de jeu ' + str(self.turn) + ').')
        printing(1, str(player))
        remaining_revealed_actions: Set[Action] = \
            player.get_actions(ActionLocation.REVEALED)  # Remaining revealed actions of the player.
        while remaining_revealed_actions and not self.is_game_over:
            action_to_carry_out: Action = first(list(remaining_revealed_actions)) if len(
                remaining_revealed_actions) == 1 else player.choose_action_to_carry_out(
                remaining_revealed_actions)  # Action to carry out.
            printing(1, 'Action effectuée : ' + action_to_carry_out.name + '.')
            action_to_carry_out.play(player=player, the_game=self,
                                     before_playing_order_players=before_playing_order_players,
                                     after_playing_order_players=after_playing_order_players)
            if not self.is_game_over:
                printing(2, str(player))
            remaining_revealed_actions.remove(action_to_carry_out)
        pass

    def play_purchase_buildings(self, player: Player):
        """
        [2nd subphase of 1st phase] The player can purchase buildings.
        Remark: Can set the game attribute indicating the game is over.
        :param player: The player whose turn it is.
        """
        is_play_purchase_buildings_ended: bool = False  # Is this subphase the player can purchase buildings ended?
        while not is_play_purchase_buildings_ended:
            # Get all buildings with their coordinates at the extremities of all rows of the table.
            buildings_extremities_rows_and_coord: Dict[Building, Tuple[int, int]] = \
                self.table.buildings_extremities_rows_and_coord()
            # Set all buildings the player can purchase with some resources.
            buildings_purchasable_with_resources: Dict[Building, List[T_Resources_int]] = \
                dict()  # Buildings the player can purchase with some resources and with architect if required.
            for building in buildings_extremities_rows_and_coord.keys():
                # If the building requires the architect, the player must have play it during this turn.
                if (not building.is_architect_required) or player.has_architect():
                    # Get all payable resources for all equivalent resources to purchase a building.
                    all_payable_equiv_building_resources: List[
                        T_Resources_int] = player.all_payable_equiv_building_resources(
                        building.all_equiv_building_resources(self.game_element.resources_swap))
                    if all_payable_equiv_building_resources:
                        buildings_purchasable_with_resources[building] = all_payable_equiv_building_resources
            # The payer purchases or not a building at the extremities of all rows of the table.
            if not buildings_purchasable_with_resources:
                #  The player cannot purchase any building.
                printing(1, 'Le joueur ' + player.name() + ' ne peut pas acheter de bâtiment.')
                is_play_purchase_buildings_ended = True
            else:
                choose_building_to_purchase: Union[None, Tuple[Building, T_Resources_int]] = \
                    player.choose_building_to_purchase(
                        buildings_purchasable_with_resources)  # The player chooses (or not) a building to purchase.
                if choose_building_to_purchase is None:
                    # The player does not want to purchase any purchasable building.
                    printing(1, 'Le joueur ' + player.name() + ' ne veut pas acheter de bâtiment parmi le(s) '
                             + str(len(buildings_purchasable_with_resources.keys())) + ' qu\'il pourrait acheter.')
                    is_play_purchase_buildings_ended = True
                else:
                    # The player purchases a purchasable building.
                    printing(1, 'Le joueur ' + player.name() + ' achète l\'un des '
                             + str(len(buildings_purchasable_with_resources.keys())) + ' bâtiment(s).')
                    building_to_purchase, cost_resources_to_purchase_building = \
                        choose_building_to_purchase  # Building to purchase and its cost resources.
                    if building_to_purchase is None:
                        raise ValueError('The player does not chose a correct building to purchase.')
                    if cost_resources_to_purchase_building is None:
                        raise ValueError('The player does not chose correct cost resources to purchase a building.')
                    self.player_purchase_building_resources(player,
                                                            buildings_extremities_rows_and_coord[building_to_purchase],
                                                            building_to_purchase, cost_resources_to_purchase_building)
                    # Is the game over?
                    self.check_is_game_over(player)
                    if self.is_game_over:
                        is_play_purchase_buildings_ended = True
                    else:
                        pass  # The game is not over and the player can perhaps purchase another building.
        pass

    def player_purchase_building_resources(self, player: Player, building_coord: Tuple[int, int], building: Building,
                                           cost_resources: T_Resources_int):
        """
        The player purchases a building with some resources.
        We may have to update the table.
        :param player: The player whose turn it is purchases a building.
        :param building_coord: Coordinates of the building in the table.
        :param building: The building the player wants to purchase.
        :param cost_resources: The (cost) resources the player has to pay for the building.
                               Remark: All numbers of resources are <= 0.
        """
        # Display the old situation.
        printing(1, 'Achat d\'un bâtiment :')
        printing(2, 'Le bâtiment ' + str(building) + ' du centre de la table va être acheté par le joueur '
                 + player.name() + '.')
        self.display_player(1, player)
        self.display_table(3)
        # The player pays resources for the building.
        for resource, number in cost_resources.items():
            resource.move(n_resources_to_move=-number, from_player=player, to_stock=True)
        player.create_widgets_frame_resources()
        self.game_element.create_widgets_frame_resources()
        # The player obtains the building which is deleted from the table.
        player.buildings.add(building)
        player.create_widget_n_tot_victory_pts()
        player.create_widget_n_buildings()
        # The building at some extremity of some row of the table is deleted.
        self.delete_building(building_coord)
        # Display the new situation.
        printing(2, 'Le bâtiment ' + str(building) + ' du centre de la table vient d\'être acheté par le joueur '
                 + player.name() + '.')
        self.display_player(1, player)
        self.display_table(3)

    def delete_building(self, building_coord: Tuple[int, int]) -> None:
        """
        Delete the building in the (j_row)th entry from the (i_row)th row at the extremities of all rows of the table.
        If there is not enough buildings in the updated row, draw new buildings and place them in the updated row.
        :param building_coord: Coordinates of the building in the table.
        """
        # Delete the building in the (j_row)th entry from the (i_row)th row at the extremities of all rows of the table.
        (i_row, j_row) = building_coord  # Coordinates of the building in the table.
        del self.table.buildings[i_row][j_row]  # Delete (j_row)th entry from (i_row)th row.
        self.table.create_widgets_frame_buildings_table(self.game_element)
        # If there is not enough buildings in the updated row, draw new buildings and place them in the updated row.
        n_buildings_row_updated: int = len(self.table.buildings[i_row])  # New number of buildings in the updated row.
        if n_buildings_row_updated > 2:
            pass  # Nothing to do (there is enough buildings in the updated row)!
        else:
            draw_buildings: List[Building] = \
                game_element.draw_buildings(
                    n_draw_buildings=self.game_element.n_revealed_buildings_by_row - n_buildings_row_updated,
                    only_in_pile=True)  # Draw buildings from the face-down draw pile at the table edge.
            if draw_buildings:
                if n_buildings_row_updated == 0:
                    self.table.buildings[i_row] = draw_buildings
                elif n_buildings_row_updated == 1:
                    self.table.buildings[i_row] = draw_buildings \
                                                  + ([self.table.buildings[i_row][0]] if j_row == 0 else
                                                     [self.table.buildings[i_row][0]]) \
                                                  + draw_buildings
                else:  # n_buildings_row_updated == 2
                    self.table.buildings[i_row] = [self.table.buildings[i_row][0]] + draw_buildings \
                                                  + [self.table.buildings[i_row][1]]
                printing(2, 'De nouveaux bâtiments ont été placés au centre de la table.')
                self.table.create_widgets_frame_buildings_table(self.game_element)
                self.table.create_widget_n_buildings_in_pile(self.game_element)
            else:
                printing(2, 'Aucun nouveau bâtiment n\'a pu être placé au centre de la table.')
        pass

    def play_supplies(self) -> None:
        """
        Play the 2nd phase: supplies.
        ~~~~~
        - Draw 3 cubes from the bag and place them in the center of the table.
          Additionally, place 3 Pesos from the stock in the center of the table.
          If there are still Pesos and/or building materials left from 1st phase (and/or earlier turns)
          in the center of the table, they remain where they are.
          Pesos and building materials may accumulate in the center of the table over the course of several rounds.
        """
        # Update resources from stock to the table during supplies.
        for resource, game_element_resource in self.game_element.resources.items():
            resource.move(n_resources_to_move=min(game_element_resource.n_supplies, resource.number),
                          from_stock=True, to_table=self.table)
        # Draw material resources from stock to the table during supplies.
        draw_materials: List[MaterialResource] = \
            game_element.draw_materials(self.game_element.n_draw_materials_supplies)  # Draw materials during supplies.
        for material in draw_materials:
            material.move(n_resources_to_move=1, from_stock=True, to_table=self.table)
        # Update resources of the GUI.
        self.game_element.create_widgets_frame_resources()
        self.table.create_widgets_frame_resources()
        pass

    def play_new_actions(self, playing_order_players: List[Player]) -> None:
        """
        Play the 3rd phase: new action card.
        :param playing_order_players: Playing order among all players.
        ~~~~~
        - Separately and in turn, each player takes exactly 1 of the cards from his hand and places it
          face-down on top of any of the two action cards laid-out in front of him.
          Note: This happens in exactly the same order as the actions were carried out in 1st phase
                (according to the numbers formed)!
        - Once every player has laid down 1 of his cards, they are revealed.
          The old card underneath is placed face-down next to each player (on his own discard pile).
          Now, each player again arranges his 2 face-up cards by the shown numbers in rising order from left to right,
          in order to form the smallest possible number.
          For example, a 2 and an 8 must always be arranged as 28 – never as 82! A 6 and a 0 always become 06, never 60!
        - Very important: If a player now, i.e. after revealing the face-down action card, holds no more than
                          2 action cards in his hand, he may now take all of his discarded action cards back to his hand
                          – he once again has 11 hand cards at his disposal.
        """
        # Choose (the same number of) actions from revealed to discarded and from his hand to face-down.
        for player in playing_order_players:
            player.choose_new_actions(self.game_element.n_new_actions)
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Nouvelles actions choisies',
                            'Chaque joueur a pris des actions de sa main\net les a placées faces cachées\n'
                            + 'sur des actions précédemment révélées\n(et qui ont déjà été défaussées).\n')
        # Set (all) actions from face-down to revealed.
        for player in self.players:
            player.reveal_actions()
        for player in self.players:
            player.create_widgets_frame_actions()
        messagebox_showinfo('Nouvelles actions révélées', 'Les actions placées faces cachées ont été révélées.')
        # In particular case, each player can take all discarded actions back to his hand.
        for player in self.players:
            player.take_actions_back(self.game_element.n_min_actions_in_hand)
        pass

    def check_is_game_over(self, player: Player = None):
        """
        Check if the game is over (and set if it the case).
        The game ends when either not exists building or not exists payable building (considering all resources,
        but without considering if the architect is required or not because players can have action Architect
        in their hand by refreshing or when they obtain all cards from discarded pile)
        or the player reaches the number of victory points.
        ~~~~~
        If, in a two-player game, one player has 25 victory points or more laying in front of him after 1st phase,
        the game ends immediately with him the winner – the round will not be finished!
        In a three-player game, 20 victory points must be gained like this,
        and 15 victory points in a four-player game.
        :param player: Player to verify if he wins the game.
        """
        if self.is_game_over:
            printing(0, 'La partie est déjà terminée.')
            pass  # Nothing to redo!
        elif player is not None and player.n_tot_victory_pts() >= \
                self.game_element.n_victory_pts_to_reach_for_players[len(self.players)]:
            # Game over if the player reaches the number of victory points.
            printing(0, 'La partie se termine par le gagnant ' + player.name()
                     + ' qui avec ' + str(player.n_tot_victory_pts())
                     + ' points de victoire a atteint ou dépassé les '
                     + str(self.game_element.n_victory_pts_to_reach_for_players[len(self.players)])
                     + ' requis pour une partie à ' + str(len(self.players)) + ' joueurs.')
            self.is_game_over = True
        elif self.game_element.buildings_in_pile():
            # It is possible that a building in the nonempty face-down draw pile (at the table edge)
            # first will be placed in the center of the table and next will be purchased by a player.
            pass  # The game is not over!
        elif self.game_element.n_buildings_in_location(BuildingLocation.TABLE) == 0:
            # Game over if the face-down draw pile (at the table edge) of buildings and
            # all rows in the center of the table are both empty.
            printing(0, 'La partie se termine car il n\'y a plus de bâtiment '
                     + 'ni dans la pile ni au centre de la table ('
                     + str(self.game_element.n_buildings_in_location(BuildingLocation.PURCHASED)) + ' achetés' + ', '
                     + str(self.game_element.n_buildings_in_location(BuildingLocation.REMOVED)) + ' supprimés' + ')'
                     + '.')
            self.is_game_over = True
        elif list(filter(lambda building_row: len(building_row) > 2, self.table.buildings)):
            # The draw pile of buildings is empty, the center of the table is not empty and some buildings
            # inside (e.g. not at the extremities of) some rows first will become at the extremities and
            # next will be purchased by a player.
            pass  # The game is not over!
        else:
            # The draw pile of buildings is empty.
            # The center of the table is not empty but each row contains 0 or 1 or 2 buildings.
            # Game over if not exists a payable building among all buildings at the extremities of all rows
            # in the center of the table with sufficient available resources everywhere
            # (owned by all players, on the table, and in the stock) to pay for it.
            # We ignore the architect because this action can be already available or can be reintroduce later.
            # Get all equivalent (by applying “5 to 1“-swap resources) available
            # (owned by all players, on the table, and in the stock) resources.
            all_equiv_available_resources: List[T_Resources_int] = \
                Resource.all_equiv_resources({resource: -(sum([player.resources[resource] for player in self.players])
                                                          + self.table.resources[resource]
                                                          + resource.number)
                                              for resource in self.game_element.resources.keys()},
                                             self.game_element.resources_swap)
            is_game_over_in_this_case: bool = True  # Is the game over (in this case)?
            for building in self.table.buildings_extremities_rows():
                if is_game_over_in_this_case:
                    for one_equiv_available_resources in all_equiv_available_resources:
                        if is_game_over_in_this_case:
                            for resource in self.game_element.resources.keys():
                                one_equiv_available_resources[resource] = -one_equiv_available_resources[resource]
                            if building.is_payable(one_equiv_available_resources):
                                is_game_over_in_this_case = False
            if is_game_over_in_this_case:
                printing(0, 'La partie se termine par manque de ressources '
                         + '(des joueurs, au centre de la table, de la réserve) '
                         + 'pour pouvoir acheter ne serait-ce que l\'un quelconque des '
                         + str(len(self.table.buildings_extremities_rows()))
                         + ' bâtiments aux extrémités des lignes au centre de la table.')
                self.is_game_over = True
            else:
                pass  # The game is not over!
        pass

    def end_of_game(self) -> None:
        """
        Display the end of the game (winners e.g. players by decreasing total number of victory points).
        """
        one_line_end_of_game: str = str(len(self.players)) + ' joueurs. ' \
                                    + str(self.turn) + ' manches.'  # End of the game on one line to print.
        printing(0, LINE_SEPARATOR)
        printing(0, 'Fin de la partie :')
        self.display(list(self.players))
        players_vp: Dict[Player, int] = {player: player.n_tot_victory_pts()
                                         for player in self.players}  # Total number of victory points of all players.
        while players_vp:
            max_vp: int = max(players_vp.values())  # Maximum of victory points.
            printing(1, str(max_vp) + ' PV :')
            one_line_end_of_game = one_line_end_of_game + ' ' + str(max_vp) + ' PV pour'
            players_max_vp: List[Player] = \
                [player for player, playing_order_details in sorted(players_vp.items())
                 if playing_order_details == max_vp]  # Players having maximum of victory points.
            for player_max_VPs in players_max_vp:
                printing(2, str(player_max_VPs))
                one_line_end_of_game = one_line_end_of_game + ' ' + player_max_VPs.name()
                del players_vp[player_max_VPs]
            one_line_end_of_game = one_line_end_of_game + (' ;' if players_vp else '.')
        print(one_line_end_of_game)  # Print, in verbose mode or not, this one line end of game.
        messagebox.showinfo('Fin de la partie', one_line_end_of_game.replace('. ', '.\n\n').replace(' ; ', '\n'))
        pass


class Gui:
    """
    Graphical user interface of the game.
    """

    ROOT_WIDTH = 1310  # Width of the root window.
    ROOT_HEIGHT = 710  # Height of the root window.

    def __init__(self, the_game: Game):
        """
        Initialization of the graphical user interface.
        :param the_game: Game.
        """
        self.game = the_game  # Game.
        self.game.gui = self  # Graphical user interface. Remark: we have 1 GUI <---> 1 game.
        self.root = Tk()  # Root window of the GUI.
        self.root.title(self.game.game_element.game_name)
        self.root.resizable(width=False, height=False)
        self.root.minsize(Gui.ROOT_WIDTH, Gui.ROOT_HEIGHT)
        self.root.focus_set()
        self.game.init_gui()
        self.game.play()  # The GUI launches the game.
        self.root.mainloop()


if __name__ == "__main__":
    printing(0, 'Start at ' + datetime.datetime.now().isoformat(sep=' ', timespec='seconds') + '.')
    game_element: GameElement = GameElement()
    game: Game = Game(game_element)
    Gui(game)
    printing(0, 'End at ' + datetime.datetime.now().isoformat(sep=' ', timespec='seconds') + '.')
